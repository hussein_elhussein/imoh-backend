require('dotenv').config({
    path: __dirname + '/.env'
});
const SOCKET_PORT = process.env.SOCKET_PORT;
const APP_DEBUG = process.env.APP_DEBUG;
const REDIS = {
    "host": process.env.REDIS_HOST,
    "port": process.env.REDIS_PORT,
    "password": process.env.REDIS_PASSWORD,
    "family": 4
};
const server_url = process.env.APP_URL;

const axios = require('axios');
const app = require('http').Server();
var retries = 0;
try {
    app.listen(SOCKET_PORT, ()=>{
        logData(new Date,true);
        logData('Server is running on port '+ SOCKET_PORT + '', true);
    });
} catch (err) {
    logData(new Date,true);
    logData('Server failed to run on port '+ SOCKET_PORT + '', true);
    throw new Error(err);
    process.exit(1);
}
const io = require('socket.io')(app,{
    handlePreflightRequest: (req, res)=>{
        let headers = {
            'Access-Control-Allow-Headers': 'Content-Type, Authorization',
            'Access-Control-Allow-Origin': req.headers.origin,
            'Access-Control-Allow-Credentials': true
        };
        res.writeHead(200, headers);
        res.end();
    }
});
try {
    const ioredis = require('ioredis');
    const redis = new ioredis(REDIS);
    logData(new Date);
    logData('Connect to Redis server on port ' + REDIS.port);
    // subscribe to the redis chat channel events:
    redis.psubscribe(['chat-channel','private-App.*'], (error, count) =>{
        logData(new Date);
        logData('Server is listening for notifications messages');
    });
    //get the data from the event:
    listenToRedisEvents(redis);
    // middleware, authenticate and save the socket id
    io.use((socket, next) => {
        authenticate(socket,() => {
            return next();
        }, (err) => {
            return next(new Error(err));
        })
    });
    // socket events
    io.on('connection', socket => {
        logData('new connection:');
        logData(socket);
        listenToSocketEvents(socket);
        socket.on('disconnect', ()=>{
            setDisconnected(socket);
        });
    });


} catch (err) {
    logData(new Date, true);
    logData('Cannot connect to Redis server on SOCKET_PORT ' + REDIS.SOCKET_PORT, true);
    throw new Error(err);
    process.exit(1);
}
function listenToRedisEvents(redis){
    redis.on('pmessage', (channel_pattern, channel, event_data)=>{
        let data = JSON.parse(event_data);
        logData('new pm message');
        //Checks if it's a notification instance:
        if(channel_pattern === 'private-App.*'){
            notify(data);
        }else if(data.event.match('ConversationCreated')){
            logData(data);
            openConversation(data.data);
        }else if(data.event.match('SocketConnected')){
            joinConversations(data.data);
        }else{
            logData("unknown event");
            logData(data);
        }
    });
}
function notify(_data){
    var data =  _data.data;
    var notifiable = _data.data.notifiable;
    logData("notification type: " + _data.data.type);
    if(_data.data.type.match('UpdateCreated')){
        data.type = "update_created"
    }
    if(_data.data.type.match('UpdateUpdated')){
        data.type = "update_updated"
    }
    if(_data.data.type.match('ReminderCreated')){
        data.type = "reminder_created"
    }
    if(_data.data.type.match('MessageCreated')){
        data.type = "message_created"
    }
    if(_data.data.type.match('ReminderNote')){
        data.type = "reminder"
    }
    if(_data.data.type.match('UpdateCode')){
        data.type = "update_code"
    }
    if(_data.data.type.match('ConfirmationCreated')){
        data.type = "confirmation_created";
    }
    delete data.notifiable;
    io.to(notifiable).emit('notification:new',data);
}
function openConversation(data){
    let ns = io.of("/");
    //join all receivers to the conversation
    if(data.receivers){
        let found_receivers = [];
        let non_found_sockets = [];
        for(let receiver of data.receivers){
            let receiver_socket = ns.connected[receiver.socket_id];
            if(receiver_socket){
                found_receivers.push(receiver.socket_id);
                receiver_socket.join(data.socket_id);
                receiver_socket.emit('conversation:opened',data);
                receiver_socket.emit('conversation:joined',data);
                logData('conversation:opened emitted');
                logData('conversation:joined emitted');
            }else{
                logData('a receiver socket not found:');
                logData(receiver);
                non_found_sockets.push(receiver.socket_id);
            }
        }
        logData('connected sockets:');
        logData(ns.connected);
        //if none of the receivers sockets found, tell the conversation opener this:
        if(non_found_sockets.length === found_receivers){
            io.to(data.socket_id).emit('conversation:opened',false);
            logData('none of the receivers found');
        }else{
            logData('all receivers joined');
        }
    }else{
        io.to(data.socket_id).emit('conversation:opened',false);
    }
}
function joinConversations(data){
    for(let conversation of data.conversations){
        let socket = getSocket(conversation.client_id);
        if(socket){
            socket.join(conversation.conversation_id);
            socket.emit('conversation:joined', conversation);
            logData('socket joined the conversation: ' + conversation.conversation_id);
        }
    }
}
function listenToSocketEvents(socket){
    socket.on('im:new', (data) => {
        logData('im:new emitted');
        logData(data);
        socket.to(data.conversation_socket_id).emit('im:new', data);
    });
    socket.on('conversation:entered', (data) => {
        logData("conversation:entered emitted");
        socket.to(data.socket_id).emit('conversation:entered', data);
        conversationEntered(socket, data);
    });
    socket.on('conversation:left', (data) => {
        logData("conversation:left emitted");
        socket.to(data.socket_id).emit('conversation:left', data);
        conversationLeft(socket, data);
    })
}
function conversationEntered(socket,data){
    const auth = socket.handshake.headers['authorization'];
    const options = {
        url: server_url + '/api/conversation/entered/' + data.id,
        method: 'POST',
        timeout: 10000,
        headers: {'Authorization': auth},
        data: {

        }
    };
    if(auth){
        axios(options).then(res => {
            retries = 0;
            logData('viewers updated');
        }).catch(err => {
            if(retries < 3){
                retries += 1;
                conversationEntered(socket,data);
            }else{
                logData("error while trying to update viewers", true);
            }
        });
    }else{
        logData('no auth header');
    }
}
function conversationLeft(socket,data){
    const auth = socket.handshake.headers['authorization'];
    const options = {
        url: server_url + '/api/conversation/left/' + data.id,
        method: 'POST',
        timeout: 10000,
        headers: {'Authorization': auth},
        data: {

        }
    };
    if(auth){
        axios(options).then(res => {
            retries = 0;
            logData('viewers updated');
        }).catch(err => {
            if(retries < 3){
                retries += 1;
                conversationLeft(socket,data);
            }else{
                logData("error while trying to update viewers", true);
            }
        });
    }else{
        logData('no auth header');
    }
}
function setDisconnected(socket){
    const auth = socket.handshake.headers['authorization'];
    const options = {
        url: server_url + '/api/profile/disconnected',
        method: 'POST',
        timeout: 10000,
        headers: {'Authorization': auth},
        data: {
            socket_id: socket.id
        }
    };
    if(auth){
        axios(options).then(res => {
            retries = 0;
            logData('user is set as disconnected');
        }).catch(err => {
            if(retries < 3){
                retries += 1;
                setDisconnected(socket);
            }else{
                logData("error while trying to update connection status", true);
            }
        });
    }else{
        logData('no auth header');
    }
}
function authenticate(socket,callback,error){
    const auth = socket.handshake.headers['authorization'];
    const err = {
        message:'Unauthorized',
        code:401,
        status:401
    };
    const options = {
        url: server_url + '/api/profile/socket',
        method: 'POST',
        timeout: 10000,
        headers: {'Authorization': auth},
        data: {
            socket_id: socket.id
        }
    };
    if(auth){
        axios(options).then(res => {
            callback();
        }).catch(err => {
            error(err);
        });
    }else{
        console.log('no auth header');
        error(err);
    }
}
function logData(data, err = false){
    if(APP_DEBUG){
        console.log('');
        console.log(data);
    }else if(err){
        console.log('');
        console.log(data);
    }
}
function getSocket(id){
    let ns = io.of("/");
    return ns.connected[id];
}
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fields', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name');
          $table->enum('type',[
            'checkbox',
            'color',
            'date',
            'start_date',
            'end_date',
            'file',
            'image',
            'number',
            'password',
            'radio_btn',
            'rich_text_box',
            'code_editor',
            'markdown_editor',
            'select_dropdown',
            'text',
            'text_area',
            'timestamp',
            'hidden',
            'coordinates',
          ])->default('text');
          $table->boolean('default_body')->default(false);
          $table->boolean('default_title')->default(false);
          $table->string('label');
          $table->boolean('required')->default(false);
          $table->integer('order')->default(1);
          $table->boolean('browse')->default(true);
          $table->boolean('read')->default(true);
          $table->boolean('edit')->default(true);
          $table->boolean('add')->default(true);
          $table->boolean('delete')->default(true);
          $table->json('options')->nullable();
          $table->text('notes')->nullable();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('fields');
    }
}

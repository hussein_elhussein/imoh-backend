<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            // Relations:
            $table->increments('id');
            $table->tinyInteger('primary')->default(true);
            $table->integer('membership_id')->unsigned()->nullable();
            $table->foreign('membership_id')->references('id')->on('memberships')->onDelete('cascade');
            $table->integer('content_type_id')->unsigned();
            $table->foreign('content_type_id')->references('id')->on('content_types')->onDelete('cascade');
            $table->integer('location_id')->unsigned()->nullable();
            $table->foreign('location_id')->references('id')->on('locations')->onDelete('cascade');
            $table->integer('country_id')->unsigned()->nullable();
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');


            // Additional info:
            $table->string('name');
            $table->string('avatar')->nullable();
            $table->decimal('loc_lat',10,8)->nullable();
            $table->decimal('loc_lng',11,8)->nullable();
            $table->longText('socket_id')->nullable();
            // 1: active, 2: pending, 3: inactive, 0: blocked:
            $table->smallInteger('status')->default(1)->index();
            $table->tinyInteger('online')->default(0);
            $table->string('last_active')->nullable();
            $table->string('device_token')->nullable();
            $table->string('profile_id');
            $table->unique('profile_id');
            $table->string('qr_code');
            $table->unique('qr_code');
            // 1: english
            // 2: arabic
            $table->integer('locale')->default(1)->nullable();
            $table->string('timezone')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('profiles');
    }
}

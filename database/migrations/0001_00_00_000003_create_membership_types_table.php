<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembershipTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('membership_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->decimal('price')->default(0);
            $table->boolean('is_default')->nullable()->default(false);
            $table->boolean('is_free')->nullable()->default(false);
            $table->integer('validity_duration')->default(12);
            $table->string('validity_unit',1)->default("m");
            $table->index('validity_duration');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('membership_types');
    }
}

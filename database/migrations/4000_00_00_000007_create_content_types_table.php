<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type')->unique();
            $table->integer('order')->default(1);
            $table->integer('parent_id')->unsigned()->nullable();
            $table->tinyInteger('menu')->default(0);
            $table->tinyInteger('tab')->default(0);
            $table->string('icon')->nullable();
            $table->tinyInteger('strict_mode')->nullable();
            $table->tinyInteger('show_map')->nullable();
            $table->tinyInteger('has_updates')->nullable();
            // how many content of this type a user can have:
            $table->integer('profile_limit')->nullable()->default(0);
            // if it's moderated, content of this type won't be published unless accepted by a moderator
            $table->tinyInteger('moderated')->nullable();
            $table->tinyInteger('profile_tab')->nullable();
            $table->integer('membership_type_id')->unsigned()->nullable();
            $table->foreign('membership_type_id')->references('id')->on('membership_types')->onDelete('set null');
            $table->integer('role_id')->unsigned()->nullable();
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_types');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profile_id')->unsigned();
            $table->foreign('profile_id')->references('id')->on('profiles')->onDelete('cascade');
            $table->integer('content_type_id')->unsigned();
            $table->foreign('content_type_id')->references('id')->on('content_types')->onDelete('cascade');
            $table->integer('location_id')->unsigned()->nullable();
            $table->foreign('location_id')->references('id')->on('locations')->onDelete('cascade');
            $table->integer('parent_id')->unsigned()->nullable();
            $table->string('title');
            $table->longText('body')->nullable();
            $table->text('excerpt')->nullable();
            $table->integer('quantity')->default(1);
            // 1: published,
            // 2: pending,
            // 3: blocked,
            // 4: closed,
            // 5: deactivated (for events):
            // 10: resolved (for services):
            $table->smallInteger('status')->default(1)->index();

            $table->tinyInteger('featured')->default(0)->index();
            $table->integer('updates_limit')->default(0)->index();
            $table->decimal('loc_lat',10,8);
            $table->decimal('loc_lng',11,8);
            $table->string('qr_code')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contents');
    }
}

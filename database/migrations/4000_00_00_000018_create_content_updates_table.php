<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentUpdatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_updates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profile_id')->unsigned();
            $table->foreign('profile_id')->references('id')->on('profiles')->onDelete('cascade');
            $table->integer('content_id')->unsigned();
            $table->foreign('content_id')->references('id')->on('contents')->onDelete('cascade');
            $table->integer('office_id')->unsigned()->nullable();
            $table->foreign('office_id')->references('id')->on('contents')->onDelete('cascade');
            $table->integer('previous_id')->unsigned()->nullable();
            $table->foreign('previous_id')->references('id')->on('content_updates')->onDelete('cascade');
            $table->integer('parent_id')->unsigned()->nullable();
            $table->foreign('parent_id')->references('id')->on('content_updates')->onDelete('cascade');
            $table->tinyInteger('actions_allowed')->nullable();
            $table->tinyInteger('primary')->nullable();
            $table->string('qr_code')->nullable();
            $table->tinyInteger('direct')->nullable();
            // 1: waiting acceptance or rejecting,
            // 2: accepted,
            // 3: canceled,
            // 4: rejected,
            // 5: edited,
            // 6: sent to org/office,
            // 7: received by org/office,
            // 8: delivered,
            // 9: received by author,
            // 10: resolved:
            // 11: when the schedule pass without status update:
            // OFFICE:
            // 12: waiting for office acceptance:
            // 13: office accepted:
            // 14: office rejected:
            // 15: office requested appointment:
            // 16: needy accepted the appointment with the office:
            // 17: needy rejected the appointment with the office:
            $table->smallInteger('status')->default(1);
            $table->string('details')->nullable();
            $table->tinyInteger('scheduled')->nullable();
            //unix
            $table->bigInteger('schedule')->nullable();
            $table->decimal('lat',10,8)->nullable();
            $table->decimal('lng',11,8)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_updates');
    }
}

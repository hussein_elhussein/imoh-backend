<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('confirmation_id')->unsigned();
            $table->foreign('confirmation_id')->references('id')->on('confirmations')->onDelete('cascade');
            $table->integer('from_id')->unsigned();
            $table->foreign('from_id')->references('id')->on('profiles')->onDelete('cascade');
            $table->integer('to_id')->unsigned();
            $table->foreign('to_id')->references('id')->on('profiles')->onDelete('cascade');
            $table->integer('receiver_id')->unsigned();
            $table->foreign('receiver_id')->references('id')->on('profiles')->onDelete('cascade');

            // 1: received
            // 2: delivered
            $table->smallInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}

<?php

use Faker\Generator as Faker;

$factory->define(\App\Taggable::class, function (Faker $faker) {
    $tags = \App\Tag::all()->pluck('id')->toArray();
    $posts = \App\Post::all()->pluck('id')->toArray();

    return [
        'tag_id' => $faker->randomElement($tags),
        'taggable_id' => $faker->randomElement($posts),
        'taggable_type' => "App\Post",
    ];
});
$factory->state(\App\Taggable::class,'news', function (Faker $faker) {
    $tags = \App\Tag::all()->pluck('id')->toArray();
    $news = \App\News::all()->pluck('id')->toArray();
    return [
        'tag_id' => $faker->randomElement($tags),
        'taggable_id' => $faker->randomElement($news),
        'taggable_type' => "App\News",
    ];
});
$factory->state(\App\Taggable::class,'event', function (Faker $faker) {
    $tags = \App\Tag::all()->pluck('id')->toArray();
    $events = \App\Event::all()->pluck('id')->toArray();
    return [
        'tag_id' => $faker->randomElement($tags),
        'taggable_id' => $faker->randomElement($events),
        'taggable_type' => "App\Event",
    ];
});
$factory->state(\App\Taggable::class,'service', function (Faker $faker) {
    $tags = \App\Tag::all()->pluck('id')->toArray();
    $services = \App\Service::all()->pluck('id')->toArray();
    return [
        'tag_id' => $faker->randomElement($tags),
        'taggable_id' => $faker->randomElement($services),
        'taggable_type' => "App\Service",
    ];
});

<?php

use Faker\Generator as Faker;

$factory->define(\App\Action::class, function (Faker $faker) {
    return [
        'author_id' => $faker->numberBetween(1,10),
        'actionable_id' => $faker->numberBetween(1,10),
        'actionable_type' => "App\User",
        'service_id' => $faker->numberBetween(1,50),
    ];
});
$factory->state(\App\Action::class,'organization', function (Faker $faker) {
    $orgs = \App\Organization::all()->pluck('author_id')->toArray();
    return [
        'author_id' => $faker->randomElement($orgs),
        'actionable_id' => $faker->numberBetween(1,10),
        'actionable_type' => "App\Organization",
        'service_id' => $faker->numberBetween(1,50),
    ];
});

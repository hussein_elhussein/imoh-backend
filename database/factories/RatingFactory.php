<?php

use Faker\Generator as Faker;

$factory->define(\App\Rating::class, function (Faker $faker) {
    $users = \App\User::all()->pluck('id')->toArray();
    return [
        'user_id' => $faker->randomElement($users),
        'rateable_id' => $faker->randomElement($users),
        'rateable_type' => "App\User",
        'rating' => $faker->numberBetween(1,5),
        'comment' => $faker->sentence(4),
    ];
});
$factory->state(\App\Rating::class,'service', function (Faker $faker) {
    $users = \App\User::all()->pluck('id')->toArray();
    $services = \App\Service::all()->pluck('id')->toArray();
    return [
        'user_id' => $faker->randomElement($users),
        'rateable_id' => $faker->randomElement($services),
        'rateable_type' => "App\Service",
        'rating' => $faker->numberBetween(1,5),
        'comment' => $faker->sentence(4),
    ];
});
$factory->state(\App\Rating::class,'response', function (Faker $faker) {
    $users = \App\User::all()->pluck('id')->toArray();
    $responses = \App\Action::all()->pluck('id')->toArray();
    return [
        'user_id' => $faker->randomElement($users),
        'rateable_id' => $faker->randomElement($responses),
        'rateable_type' => "App\Response",
        'rating' => $faker->numberBetween(1,5),
        'comment' => $faker->sentence(4),
    ];
});
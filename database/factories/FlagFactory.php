<?php

use Faker\Generator as Faker;

$factory->define(\App\Flag::class, function (Faker $faker) {
    $users = \App\User::all()->pluck('id')->toArray();
    $posts = \App\Post::all()->pluck('id')->toArray();
    return [
        'author_id' => $faker->randomElement($users),
        'flaggable_id' => $faker->randomElement($posts),
        'flaggable_type' => "App\Post",
        'comment' => $faker->sentence(4),
    ];
});
$factory->state(\App\Flag::class,'comment', function (Faker $faker) {
    $users = \App\User::all()->pluck('id')->toArray();
    $comments = \App\Comment::all()->pluck('id')->toArray();
    return [
        'author_id' => $faker->randomElement($users),
        'flaggable_id' => $faker->randomElement($comments),
        'flaggable_type' => "App\Comment",
        'comment' => $faker->sentence(4),
    ];
});
$factory->state(\App\Flag::class,'user', function (Faker $faker) {
    $users = \App\User::all()->pluck('id')->toArray();
    return [
        'author_id' => $faker->randomElement($users),
        'flaggable_id' => $faker->randomElement($users),
        'flaggable_type' => "App\User",
        'comment' => $faker->sentence(4),
    ];
});
$factory->state(\App\Flag::class,'service', function (Faker $faker) {
    $users = \App\User::all()->pluck('id')->toArray();
    $services = \App\Service::all()->pluck('id')->toArray();
    return [
        'author_id' => $faker->randomElement($users),
        'flaggable_id' => $faker->randomElement($services),
        'flaggable_type' => "App\Service",
        'comment' => $faker->sentence(4),
    ];
});
$factory->state(\App\Flag::class,'organization', function (Faker $faker) {
    $users = \App\User::all()->pluck('id')->toArray();
    $organizations = \App\Service::all()->pluck('id')->toArray();
    return [
        'author_id' => $faker->randomElement($users),
        'flaggable_id' => $faker->randomElement($organizations),
        'flaggable_type' => "App\Organization",
        'comment' => $faker->sentence(4),
    ];
});
$factory->state(\App\Flag::class,'rating', function (Faker $faker) {
    $users = \App\User::all()->pluck('id')->toArray();
    $ratings = \App\Rating::all()->pluck('id')->toArray();
    return [
        'author_id' => $faker->randomElement($users),
        'flaggable_id' => $faker->randomElement($ratings),
        'flaggable_type' => "App\Rating",
        'comment' => $faker->sentence(4),
    ];
});

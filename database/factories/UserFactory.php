<?php

use Faker\Generator as Faker;

$factory->define(\App\User::class, function (Faker $faker) {
    return [
        'role_id' => 1,
        'email' => $faker->unique()->email,
        'username' => $faker->unique()->userName,
        'password' => '$2y$10$6Iu.QUjwgz/oWAc6n1NSOeGPVldFMEdxZ6SD0xIro1EqxEY.q3q36',
        'name' => $faker->unique()->name,
        'owner' => 0,
        'verified' => 1,
        'token' => NULL,
        'deleted_at' => NULL,
        'remember_token' => NULL,
        'settings' => NULL,
    ];
});

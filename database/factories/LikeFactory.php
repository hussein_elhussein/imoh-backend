<?php

use Faker\Generator as Faker;

$factory->define(\App\Like::class, function (Faker $faker) {
    $users = \App\User::all()->pluck('id')->toArray();
    $posts = \App\Post::all()->pluck('id')->toArray();
    return [
        'author_id' => $faker->randomElement($users),
        'likeable_id' => $faker->randomElement($posts),
        'likeable_type' => "App\Post",
        'like' => $faker->boolean(80),
    ];
});
$factory->state(\App\Like::class,'comment', function (Faker $faker) {
    $users = \App\User::all()->pluck('id')->toArray();
    $comments = \App\Comment::all()->pluck('id')->toArray();
    return [
        'author_id' => $faker->randomElement($users),
        'likeable_id' => $faker->randomElement($comments),
        'likeable_type' => "App/Comment",
        'like' => $faker->boolean(80),
    ];
});

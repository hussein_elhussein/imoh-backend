<?php

use Faker\Generator as Faker;

$factory->define(\App\Membership::class, function (Faker $faker) {
    $types = \App\MembershipType::all()->pluck('id')->toArray();
    return [
        'membership_type_id' => $faker->randomElement($types),
        'start_date' =>  \Illuminate\Support\Carbon::now('UTC'),
        'end_date' =>  \Illuminate\Support\Carbon::now('UTC')->addYear()->toDateTimeString(),
    ];
});

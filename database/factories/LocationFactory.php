<?php

use Faker\Generator as Faker;

$factory->define(\App\Location::class, function (Faker $faker) {
    $countries = \App\Country::all()->pluck('id')->toArray();
    return [
        'country_id' => $faker->randomElement($countries),
        'administrative_area' => $faker->sentence(3),
        'sub_administrative_area' => $faker->sentence(3),
        'postal_code' => $faker->postcode,
    ];
});

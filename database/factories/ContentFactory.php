<?php

use Faker\Generator as Faker;

$factory->define(\App\Content::class, function (Faker $faker) {
    $profiles = \App\Profile::all()->pluck('id')->toArray();
    $type = \App\ContentType::whereType('Required')->first();
    return [
        'profile_id' => $faker->randomElement($profiles),
        'content_type_id' => $type->id,
        'location_id' => 11,
        'parent_id' => NULL,
        'title' => $faker->sentence(3),
        'body' => $faker->paragraph(10),
        'excerpt' => $faker->paragraph(5),
        'status' => $faker->randomElement([1,2,3]),
        'featured' => (int) $faker->boolean(40),
        'loc_lat' => '33.91561950',
        'loc_lng' => '35.60225570'
    ];
});

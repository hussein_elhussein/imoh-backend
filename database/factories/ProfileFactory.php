<?php

use Faker\Generator as Faker;

$factory->define(\App\Profile::class, function (Faker $faker) {
    $memberships = \App\Membership::all()->pluck('id')->toArray();
    return [
        'primary' => 1,
        'membership_id' => $faker->randomElement($memberships),
        'content_type_id' => $faker->boolean(50)? 1: 2,
        'location_id' => 11,
        'country_id' => 121,
        'name' => $faker->name,
        'avatar' => NULL,
        'loc_lat' => '33.91561950',
        'loc_lng' => '35.60225570',
        'profile_id' => $faker->unique()->uuid,
        'qr_code' => $faker->unique()->uuid,
        'socket_id' => NULL,
        'status' => 1,
        'online' => 0,
        'last_active' => \Illuminate\Support\Carbon::now()->getTimestamp(),
    ];
});
$factory->state(\App\Profile::class,'user', function (Faker $faker) {
    $memberships = \App\Membership::all()->pluck('id')->toArray();
    return [
        'primary' => 1,
        'membership_id' => $faker->randomElement($memberships),
        'content_type_id' => 1,
        'location_id' => 11,
        'country_id' => 121,
        'name' => $faker->name,
        'avatar' => NULL,
        'loc_lat' => '33.91561950',
        'loc_lng' => '35.60225570',
        'profile_id' => $faker->unique()->uuid,
        'qr_code' => $faker->unique()->uuid,
        'socket_id' => NULL,
        'status' => 1,
        'online' => 0,
        'last_active' => \Illuminate\Support\Carbon::now()->getTimestamp(),
    ];
});
$factory->state(\App\Profile::class,'org', function (Faker $faker) {
    $memberships = \App\Membership::all()->pluck('id')->toArray();
    return [
        'primary' => 1,
        'membership_id' => $faker->randomElement($memberships),
        'content_type_id' => 2,
        'location_id' => 11,
        'country_id' => 121,
        'name' => $faker->company,
        'avatar' => NULL,
        'loc_lat' => '33.91561950',
        'loc_lng' => '35.60225570',
        'profile_id' => $faker->unique()->uuid,
        'qr_code' => $faker->unique()->uuid,
        'socket_id' => NULL,
        'status' => 1,
        'online' => 0,
        'last_active' => \Illuminate\Support\Carbon::now()->getTimestamp(),
    ];
});

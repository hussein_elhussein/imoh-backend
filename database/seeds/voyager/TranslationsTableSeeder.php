<?php

use Illuminate\Database\Seeder;

class TranslationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('translations')->delete();
        
        \DB::table('translations')->insert(array (
            0 => 
            array (
                'id' => 1,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 1,
                'locale' => 'ar',
                'value' => 'أروبا',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 2,
                'locale' => 'ar',
                'value' => 'أفغانستان',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 3,
                'locale' => 'ar',
                'value' => 'أنغولا',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 4,
                'locale' => 'ar',
                'value' => 'أنغويلا',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 5,
                'locale' => 'ar',
                'value' => 'جزر آلاند',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 6,
                'locale' => 'ar',
                'value' => 'ألبانيا',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 7,
                'locale' => 'ar',
                'value' => 'أندورا',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 8,
                'locale' => 'ar',
                'value' => 'الإمارات العربية المتحدة',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 9,
                'locale' => 'ar',
                'value' => 'الأرجنتين',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 10,
                'locale' => 'ar',
                'value' => 'أرمينيا',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 11,
                'locale' => 'ar',
                'value' => 'ساموا الأمريكية',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 12,
                'locale' => 'ar',
                'value' => 'أنتيغوا وبربودا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 13,
                'locale' => 'ar',
                'value' => 'أستراليا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 14,
                'locale' => 'ar',
                'value' => 'النمسا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 15,
                'locale' => 'ar',
                'value' => 'أذربيجان',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 16,
                'locale' => 'ar',
                'value' => 'بوروندي',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 17,
                'locale' => 'ar',
                'value' => 'بلجيكا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 18,
                'locale' => 'ar',
                'value' => 'بنين',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 19,
                'locale' => 'ar',
                'value' => 'بوركينا فاسو',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 20,
                'locale' => 'ar',
                'value' => 'بنغلاديش',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 21,
                'locale' => 'ar',
                'value' => 'بلغاريا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 22,
                'locale' => 'ar',
                'value' => 'البحرين',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 23,
                'locale' => 'ar',
                'value' => 'البهاما',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 24,
                'locale' => 'ar',
                'value' => 'البوسنة والهرسك',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 25,
                'locale' => 'ar',
                'value' => 'سان بارتليمي',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 26,
                'locale' => 'ar',
                'value' => 'بيلاروس',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 27,
                'locale' => 'ar',
                'value' => 'بليز',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 28,
                'locale' => 'ar',
                'value' => 'برمودا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'id' => 29,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 29,
                'locale' => 'ar',
                'value' => 'بوليفيا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'id' => 30,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 30,
                'locale' => 'ar',
                'value' => 'البرازيل',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            30 => 
            array (
                'id' => 31,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 31,
                'locale' => 'ar',
                'value' => 'بربادوس',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'id' => 32,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 32,
                'locale' => 'ar',
                'value' => 'بروناي',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'id' => 33,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 33,
                'locale' => 'ar',
                'value' => 'بوتان',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'id' => 34,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 34,
                'locale' => 'ar',
                'value' => 'بتسوانا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'id' => 35,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 35,
                'locale' => 'ar',
                'value' => 'جمهورية أفريقيا الوسطى',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            35 => 
            array (
                'id' => 36,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 36,
                'locale' => 'ar',
                'value' => 'كندا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            36 => 
            array (
                'id' => 37,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 37,
                'locale' => 'ar',
            'value' => 'جزر كوكوس (كيلينغ)',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            37 => 
            array (
                'id' => 38,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 38,
                'locale' => 'ar',
                'value' => 'سويسرا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            38 => 
            array (
                'id' => 39,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 39,
                'locale' => 'ar',
                'value' => 'تشيلي',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            39 => 
            array (
                'id' => 40,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 40,
                'locale' => 'ar',
                'value' => 'الصين',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            40 => 
            array (
                'id' => 41,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 41,
                'locale' => 'ar',
                'value' => 'ساحل العاج',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            41 => 
            array (
                'id' => 42,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 42,
                'locale' => 'ar',
                'value' => 'الكاميرون',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            42 => 
            array (
                'id' => 43,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 43,
                'locale' => 'ar',
                'value' => 'الكونغو - كينشاسا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            43 => 
            array (
                'id' => 44,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 44,
                'locale' => 'ar',
                'value' => 'الكونغو - برازافيل',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            44 => 
            array (
                'id' => 45,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 45,
                'locale' => 'ar',
                'value' => 'جزر كوك',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            45 => 
            array (
                'id' => 46,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 46,
                'locale' => 'ar',
                'value' => 'كولومبيا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            46 => 
            array (
                'id' => 47,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 47,
                'locale' => 'ar',
                'value' => 'جزر القمر',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            47 => 
            array (
                'id' => 48,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 48,
                'locale' => 'ar',
                'value' => 'الرأس الأخضر',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            48 => 
            array (
                'id' => 49,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 49,
                'locale' => 'ar',
                'value' => 'كوستاريكا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            49 => 
            array (
                'id' => 50,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 50,
                'locale' => 'ar',
                'value' => 'كوبا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            50 => 
            array (
                'id' => 51,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 51,
                'locale' => 'ar',
                'value' => 'كوراساو',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            51 => 
            array (
                'id' => 52,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 52,
                'locale' => 'ar',
                'value' => 'جزيرة الكريسماس',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            52 => 
            array (
                'id' => 53,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 53,
                'locale' => 'ar',
                'value' => 'جزر كايمان',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            53 => 
            array (
                'id' => 54,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 54,
                'locale' => 'ar',
                'value' => 'قبرص',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            54 => 
            array (
                'id' => 55,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 55,
                'locale' => 'ar',
                'value' => 'التشيك',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            55 => 
            array (
                'id' => 56,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 56,
                'locale' => 'ar',
                'value' => 'ألمانيا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            56 => 
            array (
                'id' => 57,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 57,
                'locale' => 'ar',
                'value' => 'جيبوتي',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            57 => 
            array (
                'id' => 58,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 58,
                'locale' => 'ar',
                'value' => 'دومينيكا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            58 => 
            array (
                'id' => 59,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 59,
                'locale' => 'ar',
                'value' => 'الدانمرك',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            59 => 
            array (
                'id' => 60,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 60,
                'locale' => 'ar',
                'value' => 'جمهورية الدومينيكان',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            60 => 
            array (
                'id' => 61,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 61,
                'locale' => 'ar',
                'value' => 'الجزائر',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            61 => 
            array (
                'id' => 62,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 62,
                'locale' => 'ar',
                'value' => 'الإكوادور',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            62 => 
            array (
                'id' => 63,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 63,
                'locale' => 'ar',
                'value' => 'مصر',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            63 => 
            array (
                'id' => 64,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 64,
                'locale' => 'ar',
                'value' => 'إريتريا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            64 => 
            array (
                'id' => 65,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 65,
                'locale' => 'ar',
                'value' => 'الصحراء الغربية',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            65 => 
            array (
                'id' => 66,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 66,
                'locale' => 'ar',
                'value' => 'إسبانيا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            66 => 
            array (
                'id' => 67,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 67,
                'locale' => 'ar',
                'value' => 'إستونيا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            67 => 
            array (
                'id' => 68,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 68,
                'locale' => 'ar',
                'value' => 'إثيوبيا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            68 => 
            array (
                'id' => 69,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 69,
                'locale' => 'ar',
                'value' => 'فنلندا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            69 => 
            array (
                'id' => 70,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 70,
                'locale' => 'ar',
                'value' => 'فيجي',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            70 => 
            array (
                'id' => 71,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 71,
                'locale' => 'ar',
                'value' => 'جزر فوكلاند',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            71 => 
            array (
                'id' => 72,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 72,
                'locale' => 'ar',
                'value' => 'فرنسا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            72 => 
            array (
                'id' => 73,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 73,
                'locale' => 'ar',
                'value' => 'جزر فارو',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            73 => 
            array (
                'id' => 74,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 74,
                'locale' => 'ar',
                'value' => 'ميكرونيزيا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            74 => 
            array (
                'id' => 75,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 75,
                'locale' => 'ar',
                'value' => 'الغابون',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            75 => 
            array (
                'id' => 76,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 76,
                'locale' => 'ar',
                'value' => 'المملكة المتحدة',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            76 => 
            array (
                'id' => 77,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 77,
                'locale' => 'ar',
                'value' => 'جورجيا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            77 => 
            array (
                'id' => 78,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 78,
                'locale' => 'ar',
                'value' => 'غيرنزي',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            78 => 
            array (
                'id' => 79,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 79,
                'locale' => 'ar',
                'value' => 'غانا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            79 => 
            array (
                'id' => 80,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 80,
                'locale' => 'ar',
                'value' => 'جبل طارق',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            80 => 
            array (
                'id' => 81,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 81,
                'locale' => 'ar',
                'value' => 'غينيا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            81 => 
            array (
                'id' => 82,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 82,
                'locale' => 'ar',
                'value' => 'غوادلوب',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            82 => 
            array (
                'id' => 83,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 83,
                'locale' => 'ar',
                'value' => 'غامبيا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            83 => 
            array (
                'id' => 84,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 84,
                'locale' => 'ar',
                'value' => 'غينيا بيساو',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            84 => 
            array (
                'id' => 85,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 85,
                'locale' => 'ar',
                'value' => 'غينيا الاستوائية',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            85 => 
            array (
                'id' => 86,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 86,
                'locale' => 'ar',
                'value' => 'اليونان',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            86 => 
            array (
                'id' => 87,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 87,
                'locale' => 'ar',
                'value' => 'غرينادا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            87 => 
            array (
                'id' => 88,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 88,
                'locale' => 'ar',
                'value' => 'غرينلاند',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            88 => 
            array (
                'id' => 89,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 89,
                'locale' => 'ar',
                'value' => 'غواتيمالا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            89 => 
            array (
                'id' => 90,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 90,
                'locale' => 'ar',
                'value' => 'غويانا الفرنسية',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            90 => 
            array (
                'id' => 91,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 91,
                'locale' => 'ar',
                'value' => 'غوام',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            91 => 
            array (
                'id' => 92,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 92,
                'locale' => 'ar',
                'value' => 'غيانا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            92 => 
            array (
                'id' => 93,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 93,
                'locale' => 'ar',
                'value' => 'هونغ كونغ الصينية',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            93 => 
            array (
                'id' => 94,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 94,
                'locale' => 'ar',
                'value' => 'هندوراس',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            94 => 
            array (
                'id' => 95,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 95,
                'locale' => 'ar',
                'value' => 'كرواتيا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            95 => 
            array (
                'id' => 96,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 96,
                'locale' => 'ar',
                'value' => 'هايتي',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            96 => 
            array (
                'id' => 97,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 97,
                'locale' => 'ar',
                'value' => 'هنغاريا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            97 => 
            array (
                'id' => 98,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 98,
                'locale' => 'ar',
                'value' => 'إندونيسيا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            98 => 
            array (
                'id' => 99,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 99,
                'locale' => 'ar',
                'value' => 'جزيرة مان',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            99 => 
            array (
                'id' => 100,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 100,
                'locale' => 'ar',
                'value' => 'الهند',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            100 => 
            array (
                'id' => 101,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 101,
                'locale' => 'ar',
                'value' => 'الإقليم البريطاني في المحيط الهندي',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            101 => 
            array (
                'id' => 102,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 102,
                'locale' => 'ar',
                'value' => 'أيرلندا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            102 => 
            array (
                'id' => 103,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 103,
                'locale' => 'ar',
                'value' => 'إيران',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            103 => 
            array (
                'id' => 104,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 104,
                'locale' => 'ar',
                'value' => 'العراق',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            104 => 
            array (
                'id' => 105,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 105,
                'locale' => 'ar',
                'value' => 'أيسلندا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            105 => 
            array (
                'id' => 106,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 106,
                'locale' => 'ar',
                'value' => 'إيطاليا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            106 => 
            array (
                'id' => 107,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 107,
                'locale' => 'ar',
                'value' => 'جامايكا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            107 => 
            array (
                'id' => 108,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 108,
                'locale' => 'ar',
                'value' => 'جيرسي',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            108 => 
            array (
                'id' => 109,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 109,
                'locale' => 'ar',
                'value' => 'الأردن',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            109 => 
            array (
                'id' => 110,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 110,
                'locale' => 'ar',
                'value' => 'اليابان',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            110 => 
            array (
                'id' => 111,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 111,
                'locale' => 'ar',
                'value' => 'كازاخستان',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            111 => 
            array (
                'id' => 112,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 112,
                'locale' => 'ar',
                'value' => 'كينيا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            112 => 
            array (
                'id' => 113,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 113,
                'locale' => 'ar',
                'value' => 'قيرغيزستان',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            113 => 
            array (
                'id' => 114,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 114,
                'locale' => 'ar',
                'value' => 'كمبوديا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            114 => 
            array (
                'id' => 115,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 115,
                'locale' => 'ar',
                'value' => 'كيريباتي',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            115 => 
            array (
                'id' => 116,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 116,
                'locale' => 'ar',
                'value' => 'سانت كيتس ونيفيس',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            116 => 
            array (
                'id' => 117,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 117,
                'locale' => 'ar',
                'value' => 'كوريا الجنوبية',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            117 => 
            array (
                'id' => 118,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 118,
                'locale' => 'ar',
                'value' => 'كوسوفو',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            118 => 
            array (
                'id' => 119,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 119,
                'locale' => 'ar',
                'value' => 'الكويت',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            119 => 
            array (
                'id' => 120,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 120,
                'locale' => 'ar',
                'value' => 'لاوس',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            120 => 
            array (
                'id' => 121,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 121,
                'locale' => 'ar',
                'value' => 'لبنان',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            121 => 
            array (
                'id' => 122,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 122,
                'locale' => 'ar',
                'value' => 'ليبيريا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            122 => 
            array (
                'id' => 123,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 123,
                'locale' => 'ar',
                'value' => 'ليبيا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            123 => 
            array (
                'id' => 124,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 124,
                'locale' => 'ar',
                'value' => 'سانت لوسيا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            124 => 
            array (
                'id' => 125,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 125,
                'locale' => 'ar',
                'value' => 'ليختنشتاين',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            125 => 
            array (
                'id' => 126,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 126,
                'locale' => 'ar',
                'value' => 'سريلانكا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            126 => 
            array (
                'id' => 127,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 127,
                'locale' => 'ar',
                'value' => 'ليسوتو',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            127 => 
            array (
                'id' => 128,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 128,
                'locale' => 'ar',
                'value' => 'ليتوانيا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            128 => 
            array (
                'id' => 129,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 129,
                'locale' => 'ar',
                'value' => 'لوكسمبورغ',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            129 => 
            array (
                'id' => 130,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 130,
                'locale' => 'ar',
                'value' => 'لاتفيا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            130 => 
            array (
                'id' => 131,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 131,
                'locale' => 'ar',
            'value' => 'مكاو الصينية (منطقة إدارية خاصة)',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            131 => 
            array (
                'id' => 132,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 132,
                'locale' => 'ar',
                'value' => 'سانت مارتن',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            132 => 
            array (
                'id' => 133,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 133,
                'locale' => 'ar',
                'value' => 'المغرب',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            133 => 
            array (
                'id' => 134,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 134,
                'locale' => 'ar',
                'value' => 'موناكو',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            134 => 
            array (
                'id' => 135,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 135,
                'locale' => 'ar',
                'value' => 'مولدوفا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            135 => 
            array (
                'id' => 136,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 136,
                'locale' => 'ar',
                'value' => 'مدغشقر',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            136 => 
            array (
                'id' => 137,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 137,
                'locale' => 'ar',
                'value' => 'جزر المالديف',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            137 => 
            array (
                'id' => 138,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 138,
                'locale' => 'ar',
                'value' => 'المكسيك',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            138 => 
            array (
                'id' => 139,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 139,
                'locale' => 'ar',
                'value' => 'جزر مارشال',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            139 => 
            array (
                'id' => 140,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 140,
                'locale' => 'ar',
                'value' => 'مقدونيا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            140 => 
            array (
                'id' => 141,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 141,
                'locale' => 'ar',
                'value' => 'مالي',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            141 => 
            array (
                'id' => 142,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 142,
                'locale' => 'ar',
                'value' => 'مالطا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            142 => 
            array (
                'id' => 143,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 143,
                'locale' => 'ar',
            'value' => 'ميانمار (بورما)',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            143 => 
            array (
                'id' => 144,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 144,
                'locale' => 'ar',
                'value' => 'الجبل الأسود',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            144 => 
            array (
                'id' => 145,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 145,
                'locale' => 'ar',
                'value' => 'منغوليا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            145 => 
            array (
                'id' => 146,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 146,
                'locale' => 'ar',
                'value' => 'جزر ماريانا الشمالية',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            146 => 
            array (
                'id' => 147,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 147,
                'locale' => 'ar',
                'value' => 'موزمبيق',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            147 => 
            array (
                'id' => 148,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 148,
                'locale' => 'ar',
                'value' => 'موريتانيا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            148 => 
            array (
                'id' => 149,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 149,
                'locale' => 'ar',
                'value' => 'مونتسرات',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            149 => 
            array (
                'id' => 150,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 150,
                'locale' => 'ar',
                'value' => 'جزر المارتينيك',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            150 => 
            array (
                'id' => 151,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 151,
                'locale' => 'ar',
                'value' => 'موريشيوس',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            151 => 
            array (
                'id' => 152,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 152,
                'locale' => 'ar',
                'value' => 'ملاوي',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            152 => 
            array (
                'id' => 153,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 153,
                'locale' => 'ar',
                'value' => 'ماليزيا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            153 => 
            array (
                'id' => 154,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 154,
                'locale' => 'ar',
                'value' => 'مايوت',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            154 => 
            array (
                'id' => 155,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 155,
                'locale' => 'ar',
                'value' => 'ناميبيا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            155 => 
            array (
                'id' => 156,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 156,
                'locale' => 'ar',
                'value' => 'كاليدونيا الجديدة',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            156 => 
            array (
                'id' => 157,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 157,
                'locale' => 'ar',
                'value' => 'النيجر',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            157 => 
            array (
                'id' => 158,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 158,
                'locale' => 'ar',
                'value' => 'جزيرة نورفولك',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            158 => 
            array (
                'id' => 159,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 159,
                'locale' => 'ar',
                'value' => 'نيجيريا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            159 => 
            array (
                'id' => 160,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 160,
                'locale' => 'ar',
                'value' => 'نيكاراغوا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            160 => 
            array (
                'id' => 161,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 161,
                'locale' => 'ar',
                'value' => 'نيوي',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            161 => 
            array (
                'id' => 162,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 162,
                'locale' => 'ar',
                'value' => 'هولندا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            162 => 
            array (
                'id' => 163,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 163,
                'locale' => 'ar',
                'value' => 'النرويج',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            163 => 
            array (
                'id' => 164,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 164,
                'locale' => 'ar',
                'value' => 'نيبال',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            164 => 
            array (
                'id' => 165,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 165,
                'locale' => 'ar',
                'value' => 'ناورو',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            165 => 
            array (
                'id' => 166,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 166,
                'locale' => 'ar',
                'value' => 'نيوزيلندا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            166 => 
            array (
                'id' => 167,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 167,
                'locale' => 'ar',
                'value' => 'عُمان',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            167 => 
            array (
                'id' => 168,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 168,
                'locale' => 'ar',
                'value' => 'باكستان',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            168 => 
            array (
                'id' => 169,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 169,
                'locale' => 'ar',
                'value' => 'بنما',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            169 => 
            array (
                'id' => 170,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 170,
                'locale' => 'ar',
                'value' => 'جزر بيتكيرن',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            170 => 
            array (
                'id' => 171,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 171,
                'locale' => 'ar',
                'value' => 'بيرو',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            171 => 
            array (
                'id' => 172,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 172,
                'locale' => 'ar',
                'value' => 'الفلبين',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            172 => 
            array (
                'id' => 173,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 173,
                'locale' => 'ar',
                'value' => 'بالاو',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            173 => 
            array (
                'id' => 174,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 174,
                'locale' => 'ar',
                'value' => 'بابوا غينيا الجديدة',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            174 => 
            array (
                'id' => 175,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 175,
                'locale' => 'ar',
                'value' => 'بولندا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            175 => 
            array (
                'id' => 176,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 176,
                'locale' => 'ar',
                'value' => 'بورتوريكو',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            176 => 
            array (
                'id' => 177,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 177,
                'locale' => 'ar',
                'value' => 'كوريا الشمالية',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            177 => 
            array (
                'id' => 178,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 178,
                'locale' => 'ar',
                'value' => 'البرتغال',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            178 => 
            array (
                'id' => 179,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 179,
                'locale' => 'ar',
                'value' => 'باراغواي',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            179 => 
            array (
                'id' => 180,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 180,
                'locale' => 'ar',
                'value' => 'فلسطين',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            180 => 
            array (
                'id' => 181,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 181,
                'locale' => 'ar',
                'value' => 'بولينيزيا الفرنسية',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            181 => 
            array (
                'id' => 182,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 182,
                'locale' => 'ar',
                'value' => 'قطر',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            182 => 
            array (
                'id' => 183,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 183,
                'locale' => 'ar',
                'value' => 'روينيون',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            183 => 
            array (
                'id' => 184,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 184,
                'locale' => 'ar',
                'value' => 'رومانيا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            184 => 
            array (
                'id' => 185,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 185,
                'locale' => 'ar',
                'value' => 'روسيا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            185 => 
            array (
                'id' => 186,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 186,
                'locale' => 'ar',
                'value' => 'رواندا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            186 => 
            array (
                'id' => 187,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 187,
                'locale' => 'ar',
                'value' => 'المملكة العربية السعودية',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            187 => 
            array (
                'id' => 188,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 188,
                'locale' => 'ar',
                'value' => 'السودان',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            188 => 
            array (
                'id' => 189,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 189,
                'locale' => 'ar',
                'value' => 'السنغال',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            189 => 
            array (
                'id' => 190,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 190,
                'locale' => 'ar',
                'value' => 'سنغافورة',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            190 => 
            array (
                'id' => 191,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 191,
                'locale' => 'ar',
                'value' => 'جورجيا الجنوبية وجزر ساندويتش الجنوبية',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            191 => 
            array (
                'id' => 192,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 192,
                'locale' => 'ar',
                'value' => 'سفالبارد وجان مايان',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            192 => 
            array (
                'id' => 193,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 193,
                'locale' => 'ar',
                'value' => 'جزر سليمان',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            193 => 
            array (
                'id' => 194,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 194,
                'locale' => 'ar',
                'value' => 'سيراليون',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            194 => 
            array (
                'id' => 195,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 195,
                'locale' => 'ar',
                'value' => 'السلفادور',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            195 => 
            array (
                'id' => 196,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 196,
                'locale' => 'ar',
                'value' => 'سان مارينو',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            196 => 
            array (
                'id' => 197,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 197,
                'locale' => 'ar',
                'value' => 'الصومال',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            197 => 
            array (
                'id' => 198,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 198,
                'locale' => 'ar',
                'value' => 'سانت بيير وميكولون',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            198 => 
            array (
                'id' => 199,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 199,
                'locale' => 'ar',
                'value' => 'صربيا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            199 => 
            array (
                'id' => 200,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 200,
                'locale' => 'ar',
                'value' => 'جنوب السودان',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            200 => 
            array (
                'id' => 201,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 201,
                'locale' => 'ar',
                'value' => 'ساو تومي وبرينسيبي',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            201 => 
            array (
                'id' => 202,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 202,
                'locale' => 'ar',
                'value' => 'سورينام',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            202 => 
            array (
                'id' => 203,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 203,
                'locale' => 'ar',
                'value' => 'سلوفاكيا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            203 => 
            array (
                'id' => 204,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 204,
                'locale' => 'ar',
                'value' => 'سلوفينيا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            204 => 
            array (
                'id' => 205,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 205,
                'locale' => 'ar',
                'value' => 'السويد',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            205 => 
            array (
                'id' => 206,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 206,
                'locale' => 'ar',
                'value' => 'سوازيلاند',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            206 => 
            array (
                'id' => 207,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 207,
                'locale' => 'ar',
                'value' => 'سينت مارتن',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            207 => 
            array (
                'id' => 208,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 208,
                'locale' => 'ar',
                'value' => 'سيشل',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            208 => 
            array (
                'id' => 209,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 209,
                'locale' => 'ar',
                'value' => 'سوريا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            209 => 
            array (
                'id' => 210,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 210,
                'locale' => 'ar',
                'value' => 'جزر توركس وكايكوس',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            210 => 
            array (
                'id' => 211,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 211,
                'locale' => 'ar',
                'value' => 'تشاد',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            211 => 
            array (
                'id' => 212,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 212,
                'locale' => 'ar',
                'value' => 'توغو',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            212 => 
            array (
                'id' => 213,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 213,
                'locale' => 'ar',
                'value' => 'تايلاند',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            213 => 
            array (
                'id' => 214,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 214,
                'locale' => 'ar',
                'value' => 'طاجيكستان',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            214 => 
            array (
                'id' => 215,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 215,
                'locale' => 'ar',
                'value' => 'توكيلو',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            215 => 
            array (
                'id' => 216,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 216,
                'locale' => 'ar',
                'value' => 'تركمانستان',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            216 => 
            array (
                'id' => 217,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 217,
                'locale' => 'ar',
                'value' => 'تيمور- ليشتي',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            217 => 
            array (
                'id' => 218,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 218,
                'locale' => 'ar',
                'value' => 'تونغا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            218 => 
            array (
                'id' => 219,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 219,
                'locale' => 'ar',
                'value' => 'ترينيداد وتوباغو',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            219 => 
            array (
                'id' => 220,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 220,
                'locale' => 'ar',
                'value' => 'تونس',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            220 => 
            array (
                'id' => 221,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 221,
                'locale' => 'ar',
                'value' => 'تركيا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            221 => 
            array (
                'id' => 222,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 222,
                'locale' => 'ar',
                'value' => 'توفالو',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            222 => 
            array (
                'id' => 223,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 223,
                'locale' => 'ar',
                'value' => 'تايوان',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            223 => 
            array (
                'id' => 224,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 224,
                'locale' => 'ar',
                'value' => 'تنزانيا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            224 => 
            array (
                'id' => 225,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 225,
                'locale' => 'ar',
                'value' => 'أوغندا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            225 => 
            array (
                'id' => 226,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 226,
                'locale' => 'ar',
                'value' => 'أوكرانيا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            226 => 
            array (
                'id' => 227,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 227,
                'locale' => 'ar',
                'value' => 'أورغواي',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            227 => 
            array (
                'id' => 228,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 228,
                'locale' => 'ar',
                'value' => 'الولايات المتحدة',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            228 => 
            array (
                'id' => 229,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 229,
                'locale' => 'ar',
                'value' => 'أوزبكستان',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            229 => 
            array (
                'id' => 230,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 230,
                'locale' => 'ar',
                'value' => 'الفاتيكان',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            230 => 
            array (
                'id' => 231,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 231,
                'locale' => 'ar',
                'value' => 'سانت فنسنت وجزر غرينادين',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            231 => 
            array (
                'id' => 232,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 232,
                'locale' => 'ar',
                'value' => 'فنزويلا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            232 => 
            array (
                'id' => 233,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 233,
                'locale' => 'ar',
                'value' => 'جزر فيرجن البريطانية',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            233 => 
            array (
                'id' => 234,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 234,
                'locale' => 'ar',
                'value' => 'جزر فيرجن التابعة للولايات المتحدة',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            234 => 
            array (
                'id' => 235,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 235,
                'locale' => 'ar',
                'value' => 'فيتنام',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            235 => 
            array (
                'id' => 236,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 236,
                'locale' => 'ar',
                'value' => 'فانواتو',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            236 => 
            array (
                'id' => 237,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 237,
                'locale' => 'ar',
                'value' => 'جزر والس وفوتونا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            237 => 
            array (
                'id' => 238,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 238,
                'locale' => 'ar',
                'value' => 'ساموا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            238 => 
            array (
                'id' => 239,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 239,
                'locale' => 'ar',
                'value' => 'اليمن',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            239 => 
            array (
                'id' => 240,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 240,
                'locale' => 'ar',
                'value' => 'جنوب أفريقيا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            240 => 
            array (
                'id' => 241,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 241,
                'locale' => 'ar',
                'value' => 'زامبيا',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            241 => 
            array (
                'id' => 242,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 242,
                'locale' => 'ar',
                'value' => 'زيمبابوي',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            242 => 
            array (
                'id' => 243,
                'table_name' => 'countries',
                'column_name' => 'name',
                'foreign_key' => 243,
                'locale' => 'ar',
                'value' => 'سانت هيلانة',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            243 => 
            array (
                'id' => 244,
                'table_name' => 'comments',
                'column_name' => 'content',
                'foreign_key' => 1,
                'locale' => 'ar',
                'value' => 'تعليق',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            244 => 
            array (
                'id' => 245,
                'table_name' => 'comments',
                'column_name' => 'created_at',
                'foreign_key' => 1,
                'locale' => 'ar',
                'value' => 'تاريخ الإنشاء',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            245 => 
            array (
                'id' => 246,
                'table_name' => 'comments',
                'column_name' => 'updated_at',
                'foreign_key' => 1,
                'locale' => 'ar',
                'value' => 'تاريخ التعديل',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            246 => 
            array (
                'id' => 247,
                'table_name' => 'messages',
                'column_name' => 'sender_id',
                'foreign_key' => 2,
                'locale' => 'ar',
                'value' => 'المرسل',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            247 => 
            array (
                'id' => 248,
                'table_name' => 'messages',
                'column_name' => 'attachment_path',
                'foreign_key' => 2,
                'locale' => 'ar',
                'value' => 'رابط الرفق',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            248 => 
            array (
                'id' => 249,
                'table_name' => 'messages',
                'column_name' => 'content',
                'foreign_key' => 2,
                'locale' => 'ar',
                'value' => 'محتوى',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            249 => 
            array (
                'id' => 250,
                'table_name' => 'messages',
                'column_name' => 'conversation_id',
                'foreign_key' => 2,
                'locale' => 'ar',
                'value' => 'المحادثة',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            250 => 
            array (
                'id' => 251,
                'table_name' => 'messages',
                'column_name' => 'created_at',
                'foreign_key' => 2,
                'locale' => 'ar',
                'value' => 'تاريخ الإنشاء',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            251 => 
            array (
                'id' => 252,
                'table_name' => 'messages',
                'column_name' => 'updated_at',
                'foreign_key' => 2,
                'locale' => 'ar',
                'value' => 'تاريخ التعديل',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            252 => 
            array (
                'id' => 253,
                'table_name' => 'public_messages',
                'column_name' => 'sender_id',
                'foreign_key' => 3,
                'locale' => 'ar',
                'value' => 'المرسل',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            253 => 
            array (
                'id' => 254,
                'table_name' => 'public_messages',
                'column_name' => 'content',
                'foreign_key' => 3,
                'locale' => 'ar',
                'value' => 'محتوى',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            254 => 
            array (
                'id' => 255,
                'table_name' => 'public_messages',
                'column_name' => 'created_at',
                'foreign_key' => 3,
                'locale' => 'ar',
                'value' => 'تاريخ الإنشاء',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            255 => 
            array (
                'id' => 256,
                'table_name' => 'public_messages',
                'column_name' => 'updated_at',
                'foreign_key' => 3,
                'locale' => 'ar',
                'value' => 'تاريخ التعديل',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            256 => 
            array (
                'id' => 257,
                'table_name' => 'posts',
                'column_name' => 'author_id',
                'foreign_key' => 4,
                'locale' => 'ar',
                'value' => 'المحرر',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            257 => 
            array (
                'id' => 258,
                'table_name' => 'posts',
                'column_name' => 'staff_post',
                'foreign_key' => 4,
                'locale' => 'ar',
                'value' => 'منشور إداري',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            258 => 
            array (
                'id' => 259,
                'table_name' => 'posts',
                'column_name' => 'category_id',
                'foreign_key' => 4,
                'locale' => 'ar',
                'value' => 'التصنيف',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            259 => 
            array (
                'id' => 260,
                'table_name' => 'posts',
                'column_name' => 'title',
                'foreign_key' => 4,
                'locale' => 'ar',
                'value' => 'العنوان',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            260 => 
            array (
                'id' => 261,
                'table_name' => 'posts',
                'column_name' => 'seo_title',
                'foreign_key' => 4,
                'locale' => 'ar',
                'value' => 'العنوان لمحركات البحث',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            261 => 
            array (
                'id' => 262,
                'table_name' => 'posts',
                'column_name' => 'body',
                'foreign_key' => 4,
                'locale' => 'ar',
                'value' => 'المحتوى',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            262 => 
            array (
                'id' => 263,
                'table_name' => 'posts',
                'column_name' => 'image',
                'foreign_key' => 4,
                'locale' => 'ar',
                'value' => 'الصورة',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            263 => 
            array (
                'id' => 264,
                'table_name' => 'posts',
                'column_name' => 'slug',
                'foreign_key' => 4,
                'locale' => 'ar',
                'value' => 'الرابط الفرعي',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            264 => 
            array (
                'id' => 265,
                'table_name' => 'posts',
                'column_name' => 'meta_description',
                'foreign_key' => 4,
                'locale' => 'ar',
                'value' => 'الوصف لمحركات البحث',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            265 => 
            array (
                'id' => 266,
                'table_name' => 'posts',
                'column_name' => 'meta_keywords',
                'foreign_key' => 4,
                'locale' => 'ar',
                'value' => 'الكلمات الدلالية',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            266 => 
            array (
                'id' => 267,
                'table_name' => 'posts',
                'column_name' => 'status',
                'foreign_key' => 4,
                'locale' => 'ar',
                'value' => 'الحالة',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            267 => 
            array (
                'id' => 268,
                'table_name' => 'posts',
                'column_name' => 'featured',
                'foreign_key' => 4,
                'locale' => 'ar',
                'value' => 'مثبت',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            268 => 
            array (
                'id' => 269,
                'table_name' => 'posts',
                'column_name' => 'created_at',
                'foreign_key' => 4,
                'locale' => 'ar',
                'value' => 'تاريخ الإنشاء',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            269 => 
            array (
                'id' => 270,
                'table_name' => 'posts',
                'column_name' => 'updated_at',
                'foreign_key' => 4,
                'locale' => 'ar',
                'value' => 'تاريخ التعديل',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            270 => 
            array (
                'id' => 271,
                'table_name' => 'pages',
                'column_name' => 'author_id',
                'foreign_key' => 5,
                'locale' => 'ar',
                'value' => 'المحرر',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            271 => 
            array (
                'id' => 272,
                'table_name' => 'pages',
                'column_name' => 'title',
                'foreign_key' => 5,
                'locale' => 'ar',
                'value' => 'العنوان',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            272 => 
            array (
                'id' => 273,
                'table_name' => 'pages',
                'column_name' => 'body',
                'foreign_key' => 5,
                'locale' => 'ar',
                'value' => 'المحتوى',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            273 => 
            array (
                'id' => 274,
                'table_name' => 'pages',
                'column_name' => 'image',
                'foreign_key' => 5,
                'locale' => 'ar',
                'value' => 'الصورة',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            274 => 
            array (
                'id' => 275,
                'table_name' => 'pages',
                'column_name' => 'slug',
                'foreign_key' => 5,
                'locale' => 'ar',
                'value' => 'الرابط الفرعي',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            275 => 
            array (
                'id' => 276,
                'table_name' => 'pages',
                'column_name' => 'meta_description',
                'foreign_key' => 5,
                'locale' => 'ar',
                'value' => 'الوصف لمحركات البحث',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            276 => 
            array (
                'id' => 277,
                'table_name' => 'pages',
                'column_name' => 'meta_keywords',
                'foreign_key' => 5,
                'locale' => 'ar',
                'value' => 'الكلمات الدلالية',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            277 => 
            array (
                'id' => 278,
                'table_name' => 'pages',
                'column_name' => 'status',
                'foreign_key' => 5,
                'locale' => 'ar',
                'value' => 'الحالة',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            278 => 
            array (
                'id' => 279,
                'table_name' => 'pages',
                'column_name' => 'created_at',
                'foreign_key' => 5,
                'locale' => 'ar',
                'value' => 'تاريخ الإنشاء',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            279 => 
            array (
                'id' => 280,
                'table_name' => 'pages',
                'column_name' => 'updated_at',
                'foreign_key' => 5,
                'locale' => 'ar',
                'value' => 'تاريخ التعديل',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            281 => 
            array (
                'id' => 282,
                'table_name' => 'users',
                'column_name' => 'name',
                'foreign_key' => 6,
                'locale' => 'ar',
                'value' => 'الإسم',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            282 => 
            array (
                'id' => 283,
                'table_name' => 'users',
                'column_name' => 'email',
                'foreign_key' => 6,
                'locale' => 'ar',
                'value' => 'عنوان البريد',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            283 => 
            array (
                'id' => 284,
                'table_name' => 'users',
                'column_name' => 'username',
                'foreign_key' => 6,
                'locale' => 'ar',
                'value' => 'إسم المستخدم',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            284 => 
            array (
                'id' => 285,
                'table_name' => 'users',
                'column_name' => 'password',
                'foreign_key' => 6,
                'locale' => 'ar',
                'value' => 'كلمة السر',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            285 => 
            array (
                'id' => 286,
                'table_name' => 'users',
                'column_name' => 'profile_id',
                'foreign_key' => 6,
                'locale' => 'ar',
                'value' => 'الرقم التعريفي',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            286 => 
            array (
                'id' => 287,
                'table_name' => 'users',
                'column_name' => 'gender',
                'foreign_key' => 6,
                'locale' => 'ar',
                'value' => 'الجنس',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            287 => 
            array (
                'id' => 288,
                'table_name' => 'users',
                'column_name' => 'avatar',
                'foreign_key' => 6,
                'locale' => 'ar',
                'value' => 'الصورة الشخصية',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            288 => 
            array (
                'id' => 289,
                'table_name' => 'users',
                'column_name' => 'birthdate',
                'foreign_key' => 6,
                'locale' => 'ar',
                'value' => 'تاريخ الولادة',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            289 => 
            array (
                'id' => 290,
                'table_name' => 'users',
                'column_name' => 'country_id',
                'foreign_key' => 6,
                'locale' => 'ar',
                'value' => 'البلد',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            290 => 
            array (
                'id' => 291,
                'table_name' => 'users',
                'column_name' => 'owner',
                'foreign_key' => 6,
                'locale' => 'ar',
                'value' => 'عضو مالك',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            291 => 
            array (
                'id' => 292,
                'table_name' => 'users',
                'column_name' => 'verified',
                'foreign_key' => 6,
                'locale' => 'ar',
                'value' => 'تم التأكيد',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            292 => 
            array (
                'id' => 293,
                'table_name' => 'users',
                'column_name' => 'token',
                'foreign_key' => 6,
                'locale' => 'ar',
                'value' => 'مفتاح مؤقت',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            293 => 
            array (
                'id' => 294,
                'table_name' => 'users',
                'column_name' => 'was_online',
                'foreign_key' => 6,
                'locale' => 'ar',
                'value' => 'آخر ظهور',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            294 => 
            array (
                'id' => 295,
                'table_name' => 'users',
                'column_name' => 'remember_token',
                'foreign_key' => 6,
                'locale' => 'ar',
                'value' => 'تذكير كلمة المرور',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            295 => 
            array (
                'id' => 296,
                'table_name' => 'users',
                'column_name' => 'deleted_at',
                'foreign_key' => 6,
                'locale' => 'ar',
                'value' => 'تاريخ الحدف',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            296 => 
            array (
                'id' => 297,
                'table_name' => 'users',
                'column_name' => 'created_at',
                'foreign_key' => 6,
                'locale' => 'ar',
                'value' => 'تاريخ الإنشاء',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            297 => 
            array (
                'id' => 298,
                'table_name' => 'users',
                'column_name' => 'updated_at',
                'foreign_key' => 6,
                'locale' => 'ar',
                'value' => 'تاريخ التعديل',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            298 => 
            array (
                'id' => 299,
                'table_name' => 'categories',
                'column_name' => 'order',
                'foreign_key' => 7,
                'locale' => 'ar',
                'value' => 'ترتيب',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            299 => 
            array (
                'id' => 300,
                'table_name' => 'categories',
                'column_name' => 'name',
                'foreign_key' => 7,
                'locale' => 'ar',
                'value' => 'الأسم',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            300 => 
            array (
                'id' => 301,
                'table_name' => 'categories',
                'column_name' => 'created_at',
                'foreign_key' => 7,
                'locale' => 'ar',
                'value' => 'تاريخ الإنشاء',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            301 => 
            array (
                'id' => 302,
                'table_name' => 'categories',
                'column_name' => 'updated_at',
                'foreign_key' => 7,
                'locale' => 'ar',
                'value' => 'تاريخ التعديل',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            302 => 
            array (
                'id' => 303,
                'table_name' => 'roles',
                'column_name' => 'name',
                'foreign_key' => 9,
                'locale' => 'ar',
                'value' => 'العنوان',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            303 => 
            array (
                'id' => 304,
                'table_name' => 'roles',
                'column_name' => 'display_name',
                'foreign_key' => 9,
                'locale' => 'ar',
                'value' => 'العنوان المعروض',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            304 => 
            array (
                'id' => 305,
                'table_name' => 'roles',
                'column_name' => 'created_at',
                'foreign_key' => 9,
                'locale' => 'ar',
                'value' => 'تاريخ الإنشاء',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            305 => 
            array (
                'id' => 306,
                'table_name' => 'roles',
                'column_name' => 'updated_at',
                'foreign_key' => 9,
                'locale' => 'ar',
                'value' => 'تاريخ التعديل',
                
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            306 => 
            array (
                'id' => 307,
                'table_name' => 'data_types',
                'column_name' => 'display_name_singular',
                'foreign_key' => 15,
                'locale' => 'ar',
                'value' => 'Service Category',
                
                'created_at' => '2018-01-03 19:12:06',
                'updated_at' => '2018-01-03 19:12:06',
            ),
            307 => 
            array (
                'id' => 308,
                'table_name' => 'data_types',
                'column_name' => 'display_name_plural',
                'foreign_key' => 15,
                'locale' => 'ar',
                'value' => 'Service Categories',
                
                'created_at' => '2018-01-03 19:12:07',
                'updated_at' => '2018-01-03 19:12:07',
            ),
            308 => 
            array (
                'id' => 309,
                'table_name' => 'data_types',
                'column_name' => 'display_name_singular',
                'foreign_key' => 16,
                'locale' => 'ar',
                'value' => 'Service Sub Category',
                
                'created_at' => '2018-01-03 19:15:17',
                'updated_at' => '2018-01-03 19:15:17',
            ),
            309 => 
            array (
                'id' => 310,
                'table_name' => 'data_types',
                'column_name' => 'display_name_plural',
                'foreign_key' => 16,
                'locale' => 'ar',
                'value' => 'Service Sub Categories',
                
                'created_at' => '2018-01-03 19:15:17',
                'updated_at' => '2018-01-03 19:15:17',
            ),
            310 => 
            array (
                'id' => 311,
                'table_name' => 'data_types',
                'column_name' => 'display_name_singular',
                'foreign_key' => 17,
                'locale' => 'ar',
                'value' => 'Service Type',
                
                'created_at' => '2018-01-03 19:18:15',
                'updated_at' => '2018-01-03 19:18:15',
            ),
            311 => 
            array (
                'id' => 312,
                'table_name' => 'data_types',
                'column_name' => 'display_name_plural',
                'foreign_key' => 17,
                'locale' => 'ar',
                'value' => 'Service Types',
                
                'created_at' => '2018-01-03 19:18:15',
                'updated_at' => '2018-01-03 19:18:15',
            ),
            312 => 
            array (
                'id' => 313,
                'table_name' => 'data_types',
                'column_name' => 'display_name_singular',
                'foreign_key' => 18,
                'locale' => 'ar',
                'value' => 'Service',
                
                'created_at' => '2018-01-03 19:23:12',
                'updated_at' => '2018-01-03 19:23:12',
            ),
            313 => 
            array (
                'id' => 314,
                'table_name' => 'data_types',
                'column_name' => 'display_name_plural',
                'foreign_key' => 18,
                'locale' => 'ar',
                'value' => 'Services',
                
                'created_at' => '2018-01-03 19:23:12',
                'updated_at' => '2018-01-03 19:23:12',
            ),
            314 => 
            array (
                'id' => 315,
                'table_name' => 'data_types',
                'column_name' => 'display_name_singular',
                'foreign_key' => 19,
                'locale' => 'ar',
                'value' => 'Field',
                
                'created_at' => '2018-01-03 19:44:13',
                'updated_at' => '2018-01-03 19:44:13',
            ),
            315 => 
            array (
                'id' => 316,
                'table_name' => 'data_types',
                'column_name' => 'display_name_plural',
                'foreign_key' => 19,
                'locale' => 'ar',
                'value' => 'Fields',
                
                'created_at' => '2018-01-03 19:44:13',
                'updated_at' => '2018-01-03 19:44:13',
            ),
            316 => 
            array (
                'id' => 317,
                'table_name' => 'data_types',
                'column_name' => 'display_name_singular',
                'foreign_key' => 20,
                'locale' => 'ar',
                'value' => 'Fields Group',
                
                'created_at' => '2018-01-03 19:47:32',
                'updated_at' => '2018-01-03 19:47:32',
            ),
            317 => 
            array (
                'id' => 318,
                'table_name' => 'data_types',
                'column_name' => 'display_name_plural',
                'foreign_key' => 20,
                'locale' => 'ar',
                'value' => 'Fields Groups',
                
                'created_at' => '2018-01-03 19:47:32',
                'updated_at' => '2018-01-03 19:47:32',
            ),
            318 => 
            array (
                'id' => 319,
                'table_name' => 'menu_items',
                'column_name' => 'title',
                'foreign_key' => 30,
                'locale' => 'ar',
                'value' => '',
                
                'created_at' => '2018-01-03 19:48:57',
                'updated_at' => '2018-01-03 19:48:57',
            ),
            319 => 
            array (
                'id' => 320,
                'table_name' => 'menu_items',
                'column_name' => 'title',
                'foreign_key' => 29,
                'locale' => 'ar',
                'value' => 'Fields Groups',
                
                'created_at' => '2018-01-03 19:53:20',
                'updated_at' => '2018-01-03 19:53:20',
            ),
            320 => 
            array (
                'id' => 323,
                'table_name' => 'menu_items',
                'column_name' => 'title',
                'foreign_key' => 33,
                'locale' => 'ar',
                'value' => 'Service Types',
                
                'created_at' => '2018-03-03 16:44:13',
                'updated_at' => '2018-03-03 16:44:13',
            ),
            321 => 
            array (
                'id' => 324,
                'table_name' => 'menu_items',
                'column_name' => 'title',
                'foreign_key' => 34,
                'locale' => 'ar',
                'value' => 'Services',
                
                'created_at' => '2018-03-03 16:44:24',
                'updated_at' => '2018-03-03 16:44:24',
            ),
            322 => 
            array (
                'id' => 325,
                'table_name' => 'data_types',
                'column_name' => 'display_name_singular',
                'foreign_key' => 22,
                'locale' => 'ar',
                'value' => 'Service Sub Category',
                
                'created_at' => '2018-03-03 16:46:37',
                'updated_at' => '2018-03-03 16:46:37',
            ),
            323 => 
            array (
                'id' => 326,
                'table_name' => 'data_types',
                'column_name' => 'display_name_plural',
                'foreign_key' => 22,
                'locale' => 'ar',
                'value' => 'Service Sub Categories',
                
                'created_at' => '2018-03-03 16:46:37',
                'updated_at' => '2018-03-03 16:46:37',
            ),
            324 => 
            array (
                'id' => 327,
                'table_name' => 'data_types',
                'column_name' => 'display_name_singular',
                'foreign_key' => 23,
                'locale' => 'ar',
                'value' => 'Service Type',
                
                'created_at' => '2018-03-03 18:09:38',
                'updated_at' => '2018-03-03 18:09:38',
            ),
            325 => 
            array (
                'id' => 328,
                'table_name' => 'data_types',
                'column_name' => 'display_name_plural',
                'foreign_key' => 23,
                'locale' => 'ar',
                'value' => 'Service Types',
                
                'created_at' => '2018-03-03 18:09:38',
                'updated_at' => '2018-03-03 18:09:38',
            ),
            326 => 
            array (
                'id' => 329,
                'table_name' => 'data_types',
                'column_name' => 'display_name_singular',
                'foreign_key' => 7,
                'locale' => 'ar',
                'value' => 'Category',
                
                'created_at' => '2018-03-03 19:40:36',
                'updated_at' => '2018-03-03 19:40:36',
            ),
            327 => 
            array (
                'id' => 330,
                'table_name' => 'data_types',
                'column_name' => 'display_name_plural',
                'foreign_key' => 7,
                'locale' => 'ar',
                'value' => 'Categories',
                
                'created_at' => '2018-03-03 19:40:36',
                'updated_at' => '2018-03-03 19:40:36',
            ),
            328 => 
            array (
                'id' => 331,
                'table_name' => 'categories',
                'column_name' => 'slug',
                'foreign_key' => 1,
                'locale' => 'ar',
                'value' => '',
                
                'created_at' => '2018-03-03 19:41:00',
                'updated_at' => '2018-03-03 19:41:00',
            ),
            329 => 
            array (
                'id' => 332,
                'table_name' => 'categories',
                'column_name' => 'name',
                'foreign_key' => 1,
                'locale' => 'ar',
                'value' => '',
                
                'created_at' => '2018-03-03 19:41:00',
                'updated_at' => '2018-03-03 19:41:00',
            ),
            330 => 
            array (
                'id' => 333,
                'table_name' => 'data_types',
                'column_name' => 'display_name_singular',
                'foreign_key' => 6,
                'locale' => 'ar',
                'value' => 'User',
                
                'created_at' => '2018-03-03 22:11:38',
                'updated_at' => '2018-03-03 22:11:38',
            ),
            331 => 
            array (
                'id' => 334,
                'table_name' => 'data_types',
                'column_name' => 'display_name_plural',
                'foreign_key' => 6,
                'locale' => 'ar',
                'value' => 'Users',
                
                'created_at' => '2018-03-03 22:11:38',
                'updated_at' => '2018-03-03 22:11:38',
            ),
            332 => 
            array (
                'id' => 335,
                'table_name' => 'data_types',
                'column_name' => 'display_name_singular',
                'foreign_key' => 25,
                'locale' => 'ar',
                'value' => 'Section',
                
                'created_at' => '2018-03-04 20:08:07',
                'updated_at' => '2018-03-04 20:08:07',
            ),
            333 => 
            array (
                'id' => 336,
                'table_name' => 'data_types',
                'column_name' => 'display_name_plural',
                'foreign_key' => 25,
                'locale' => 'ar',
                'value' => 'Sections',
                
                'created_at' => '2018-03-04 20:08:08',
                'updated_at' => '2018-03-04 20:08:08',
            ),
            334 => 
            array (
                'id' => 337,
                'table_name' => 'menu_items',
                'column_name' => 'title',
                'foreign_key' => 36,
                'locale' => 'ar',
                'value' => 'Forms',
                
                'created_at' => '2018-03-04 20:13:36',
                'updated_at' => '2018-03-04 20:13:36',
            ),
            335 => 
            array (
                'id' => 338,
                'table_name' => 'menu_items',
                'column_name' => 'title',
                'foreign_key' => 38,
                'locale' => 'ar',
                'value' => 'Offices',
                
                'created_at' => '2018-03-09 22:54:30',
                'updated_at' => '2018-03-09 22:54:30',
            ),
            336 => 
            array (
                'id' => 339,
                'table_name' => 'menu_items',
                'column_name' => 'title',
                'foreign_key' => 37,
                'locale' => 'ar',
                'value' => 'Organizations',
                
                'created_at' => '2018-03-09 23:05:21',
                'updated_at' => '2018-03-09 23:05:21',
            ),
            337 => 
            array (
                'id' => 340,
                'table_name' => 'data_types',
                'column_name' => 'display_name_singular',
                'foreign_key' => 28,
                'locale' => 'ar',
                'value' => 'Office',
                
                'created_at' => '2018-03-09 23:07:33',
                'updated_at' => '2018-03-09 23:07:33',
            ),
            338 => 
            array (
                'id' => 341,
                'table_name' => 'data_types',
                'column_name' => 'display_name_plural',
                'foreign_key' => 28,
                'locale' => 'ar',
                'value' => 'Offices',
                
                'created_at' => '2018-03-09 23:07:33',
                'updated_at' => '2018-03-09 23:07:33',
            ),
        ));
        
        
    }
}
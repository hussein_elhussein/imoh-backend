<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('settings')->delete();

        \DB::table('settings')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'key' => 'site.title',
                    'display_name' => 'Site Title',
                    'value' => 'IMOH',
                    'details' => '',
                    'type' => 'text',
                    'order' => 1,
                    'group' => 'Site',
                ),
            1 =>
                array (
                    'id' => 2,
                    'key' => 'site.description',
                    'display_name' => 'Site Description',
                    'value' => 'Boutique Create Agency',
                    'details' => '',
                    'type' => 'text',
                    'order' => 2,
                    'group' => 'Site',
                ),
            2 =>
                array (
                    'id' => 3,
                    'key' => 'site.logo',
                    'display_name' => 'Site Logo',
                    'value' => 'settings/December2017/a8HSwIsmzLCgIFJQr5SD.png',
                    'details' => '',
                    'type' => 'image',
                    'order' => 3,
                    'group' => 'Site',
                ),
            3 =>
                array (
                    'id' => 4,
                    'key' => 'site.google_analytics_tracking_id',
                    'display_name' => 'Google Analytics Tracking ID',
                    'value' => NULL,
                    'details' => '',
                    'type' => 'text',
                    'order' => 4,
                    'group' => 'Site',
                ),
            4 =>
                array (
                    'id' => 5,
                    'key' => 'admin.bg_image',
                    'display_name' => 'Admin Background Image',
                    'value' => 'settings/background.jpg',
                    'details' => '',
                    'type' => 'image',
                    'order' => 5,
                    'group' => 'Admin',
                ),
            5 =>
                array (
                    'id' => 6,
                    'key' => 'admin.title',
                    'display_name' => 'Admin Title',
                    'value' => 'IMOH',
                    'details' => '',
                    'type' => 'text',
                    'order' => 1,
                    'group' => 'Admin',
                ),
            6 =>
                array (
                    'id' => 7,
                    'key' => 'admin.description',
                    'display_name' => 'Admin Description',
                    'value' => 'Boutique Create Agency',
                    'details' => '',
                    'type' => 'text',
                    'order' => 2,
                    'group' => 'Admin',
                ),
            7 =>
                array (
                    'id' => 8,
                    'key' => 'admin.loader',
                    'display_name' => 'Admin Loader',
                    'value' => '',
                    'details' => '',
                    'type' => 'image',
                    'order' => 3,
                    'group' => 'Admin',
                ),
            8 =>
                array (
                    'id' => 9,
                    'key' => 'admin.icon_image',
                    'display_name' => 'Admin Icon Image',
                    'value' => 'settings/December2017/cOhdRInTy6ewpInYlhMv.png',
                    'details' => '',
                    'type' => 'image',
                    'order' => 4,
                    'group' => 'Admin',
                ),
            9 =>
                array (
                    'id' => 10,
                    'key' => 'admin.google_analytics_client_id',
                    'display_name' => 'Google Analytics Client ID (used for admin dashboard)',
                    'value' => NULL,
                    'details' => '',
                    'type' => 'text',
                    'order' => 1,
                    'group' => 'Admin',
                ),
            10 =>
                array (
                    'id' => 11,
                    'key' => 'mail.smtp_host',
                    'display_name' => 'SMTP Host',
                    'value' => 'smtp.mailtrap.io',
                    'details' => NULL,
                    'type' => 'text',
                    'order' => 6,
                    'group' => 'Mail',
                ),
            11 =>
                array (
                    'id' => 12,
                    'key' => 'mail.smtp_port',
                    'display_name' => 'SMTP Port',
                    'value' => '2525',
                    'details' => NULL,
                    'type' => 'text',
                    'order' => 7,
                    'group' => 'Mail',
                ),
            12 =>
                array (
                    'id' => 13,
                    'key' => 'mail.imap_host',
                    'display_name' => 'IMAP Host',
                    'value' => 'a2plcpnl0905.prod.iad2.secureserver.net',
                    'details' => NULL,
                    'type' => 'text',
                    'order' => 8,
                    'group' => 'Mail',
                ),
            13 =>
                array (
                    'id' => 14,
                    'key' => 'mail.imap_port',
                    'display_name' => 'IMAP Port',
                    'value' => '993',
                    'details' => NULL,
                    'type' => 'text',
                    'order' => 9,
                    'group' => 'Mail',
                ),
            14 =>
                array (
                    'id' => 15,
                    'key' => 'mail.pop3_port',
                    'display_name' => 'POP3 Port',
                    'value' => '9950',
                    'details' => NULL,
                    'type' => 'text',
                    'order' => 11,
                    'group' => 'Mail',
                ),
            15 =>
                array (
                    'id' => 19,
                    'key' => 'mail.encryption',
                    'display_name' => 'Encryption',
                    'value' => 'SSL',
                    'details' => '{
"default" : "none",
"options" : {
"none": "None",
"SSL": "SSL",
"TLS": "TLS"
}
}',
                    'type' => 'select_dropdown',
                    'order' => 12,
                    'group' => 'Mail',
                ),
            16 =>
                array (
                    'id' => 22,
                    'key' => 'mail.username',
                    'display_name' => 'Username',
                    'value' => 'cpimoh',
                    'details' => NULL,
                    'type' => 'text',
                    'order' => 13,
                    'group' => 'Mail',
                ),
            17 =>
                array (
                    'id' => 23,
                    'key' => 'mail.password',
                    'display_name' => 'password',
                    'value' => 'IMOHorg1009010200',
                    'details' => NULL,
                    'type' => 'text',
                    'order' => 16,
                    'group' => 'Mail',
                ),
            18 =>
                array (
                    'id' => 25,
                    'key' => 'mail.pop3_host',
                    'display_name' => 'POP3 Host',
                    'value' => 'smtp.mailtrap.io',
                    'details' => NULL,
                    'type' => 'text',
                    'order' => 10,
                    'group' => 'Mail',
                ),
            19 =>
                array (
                    'id' => 26,
                    'key' => 'mail.from_address',
                    'display_name' => 'From Address',
                    'value' => 'function.op@gmail.com',
                    'details' => NULL,
                    'type' => 'text',
                    'order' => 17,
                    'group' => 'Mail',
                ),
            20 =>
                array (
                    'id' => 27,
                    'key' => 'mail.from_name',
                    'display_name' => 'From Name',
                    'value' => 'Hussein',
                    'details' => NULL,
                    'type' => 'text',
                    'order' => 18,
                    'group' => 'Mail',
                ),
            21 =>
                array (
                    'id' => 28,
                    'key' => 'mail.sending_interval',
                    'display_name' => 'Sending Interval (in seconds)',
                    'value' => '20',
                    'details' => NULL,
                    'type' => 'text',
                    'order' => 19,
                    'group' => 'Mail',
                ),
            22 =>
                array (
                    'id' => 30,
                    'key' => 'content-types.service_type_id',
                    'display_name' => 'Service type id',
                    'value' => '3',
                    'details' => NULL,
                    'type' => 'text',
                    'order' => 20,
                    'group' => 'Content Types',
                ),
            23 =>
                array (
                    'id' => 31,
                    'key' => 'content-types.offered_service_type_id',
                    'display_name' => 'Offered service type id',
                    'value' => '5',
                    'details' => NULL,
                    'type' => 'text',
                    'order' => 21,
                    'group' => 'Content Types',
                ),
            24 =>
                array (
                    'id' => 32,
                    'key' => 'content-types.required_service_type_id',
                    'display_name' => 'Required service type id',
                    'value' => '4',
                    'details' => NULL,
                    'type' => 'text',
                    'order' => 22,
                    'group' => 'Content Types',
                ),
            25 =>
                array (
                    'id' => 33,
                    'key' => 'content-types.profile_type_id',
                    'display_name' => 'Profile type id',
                    'value' => '1',
                    'details' => NULL,
                    'type' => 'text',
                    'order' => 23,
                    'group' => 'Content Types',
                ),
            26 =>
                array (
                    'id' => 34,
                    'key' => 'content-types.organization_type_id',
                    'display_name' => 'Organization type id',
                    'value' => '2',
                    'details' => NULL,
                    'type' => 'text',
                    'order' => 24,
                    'group' => 'Content Types',
                ),
            27 =>
                array (
                    'id' => 35,
                    'key' => 'content-types.office_type_id',
                    'display_name' => 'Office type id',
                    'value' => '6',
                    'details' => NULL,
                    'type' => 'text',
                    'order' => 25,
                    'group' => 'Content Types',
                ),
        ));
        
        
    }
}
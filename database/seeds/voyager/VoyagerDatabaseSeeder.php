<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Traits\Seedable;

class VoyagerDatabaseSeeder extends Seeder
{
    use Seedable;

    protected $seedersPath = __DIR__ . 'VoyagerDatabaseSeeder.php/';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            DataTypesTableSeeder::class,
            MenusTableSeeder::class,
            SettingsTableSeeder::class,
            PermissionsTableSeeder::class,
            PermissionRoleTableSeeder::class,
            MenuItemsTableSeeder::class,
            TranslationsTableSeeder::class,
            DataRowsTableSeeder::class,

        ]);
    }
}

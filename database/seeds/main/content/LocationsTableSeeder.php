<?php

use Illuminate\Database\Seeder;

class LocationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        factory(App\Location::class,10)->create();
        \DB::table('locations')->insert([
            'id' => 11,
            'country_id' => 121,
            'administrative_area' => 'Mount Lebanon Governorate',
            'sub_administrative_area' => 'Matn',
            'postal_code' => NULL,
            'deleted_at' => NULL,
        ]);
        
        
    }
}
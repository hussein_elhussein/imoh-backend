<?php

use Illuminate\Database\Seeder;

class ContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            CountriesTableSeeder::class,
            LocationsTableSeeder::class,
            MembershipsTableSeeder::class,
            RolesTableSeeder::class,
            UsersTableSeeder::class,
            UserRolesTableSeeder::class,
            CommentsTableSeeder::class,
            FlagsTableSeeder::class,
            LikesTableSeeder::class,
            RatingsTableSeeder::class,
            TagsTableSeeder::class,
            ContentsTableSeeder::class,
        ]);
    }
}

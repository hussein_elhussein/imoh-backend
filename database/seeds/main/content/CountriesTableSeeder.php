<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('countries')->delete();
        
        \DB::table('countries')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Aruba',
                'calling_code' => '297',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Afghanistan',
                'calling_code' => '93',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Angola',
                'calling_code' => '244',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Anguilla',
                'calling_code' => '1264',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Åland Islands',
                'calling_code' => '358',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Albania',
                'calling_code' => '355',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Andorra',
                'calling_code' => '376',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'United Arab Emirates',
                'calling_code' => '971',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Argentina',
                'calling_code' => '54',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Armenia',
                'calling_code' => '374',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'American Samoa',
                'calling_code' => '1684',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'Antigua and Barbuda',
                'calling_code' => '1268',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'Australia',
                'calling_code' => '61',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'Austria',
                'calling_code' => '43',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'Azerbaijan',
                'calling_code' => '994',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'Burundi',
                'calling_code' => '257',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'Belgium',
                'calling_code' => '32',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'name' => 'Benin',
                'calling_code' => '229',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'name' => 'Burkina Faso',
                'calling_code' => '226',
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'name' => 'Bangladesh',
                'calling_code' => '880',
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'name' => 'Bulgaria',
                'calling_code' => '359',
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'name' => 'Bahrain',
                'calling_code' => '973',
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'name' => 'Bahamas',
                'calling_code' => '1242',
                'deleted_at' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'name' => 'Bosnia and Herzegovina',
                'calling_code' => '387',
                'deleted_at' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'name' => 'Saint Barthélemy',
                'calling_code' => '590',
                'deleted_at' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'name' => 'Belarus',
                'calling_code' => '375',
                'deleted_at' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'name' => 'Belize',
                'calling_code' => '501',
                'deleted_at' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'name' => 'Bermuda',
                'calling_code' => '1441',
                'deleted_at' => NULL,
            ),
            28 => 
            array (
                'id' => 29,
                'name' => 'Bolivia',
                'calling_code' => '591',
                'deleted_at' => NULL,
            ),
            29 => 
            array (
                'id' => 30,
                'name' => 'Brazil',
                'calling_code' => '55',
                'deleted_at' => NULL,
            ),
            30 => 
            array (
                'id' => 31,
                'name' => 'Barbados',
                'calling_code' => '1246',
                'deleted_at' => NULL,
            ),
            31 => 
            array (
                'id' => 32,
                'name' => 'Brunei',
                'calling_code' => '673',
                'deleted_at' => NULL,
            ),
            32 => 
            array (
                'id' => 33,
                'name' => 'Bhutan',
                'calling_code' => '975',
                'deleted_at' => NULL,
            ),
            33 => 
            array (
                'id' => 34,
                'name' => 'Botswana',
                'calling_code' => '267',
                'deleted_at' => NULL,
            ),
            34 => 
            array (
                'id' => 35,
                'name' => 'Central African Republic',
                'calling_code' => '236',
                'deleted_at' => NULL,
            ),
            35 => 
            array (
                'id' => 36,
                'name' => 'Canada',
                'calling_code' => '1',
                'deleted_at' => NULL,
            ),
            36 => 
            array (
                'id' => 37,
            'name' => 'Cocos (Keeling) Islands',
                'calling_code' => '61',
                'deleted_at' => NULL,
            ),
            37 => 
            array (
                'id' => 38,
                'name' => 'Switzerland',
                'calling_code' => '41',
                'deleted_at' => NULL,
            ),
            38 => 
            array (
                'id' => 39,
                'name' => 'Chile',
                'calling_code' => '56',
                'deleted_at' => NULL,
            ),
            39 => 
            array (
                'id' => 40,
                'name' => 'China',
                'calling_code' => '86',
                'deleted_at' => NULL,
            ),
            40 => 
            array (
                'id' => 41,
                'name' => 'Ivory Coast',
                'calling_code' => '225',
                'deleted_at' => NULL,
            ),
            41 => 
            array (
                'id' => 42,
                'name' => 'Cameroon',
                'calling_code' => '237',
                'deleted_at' => NULL,
            ),
            42 => 
            array (
                'id' => 43,
                'name' => 'DR Congo',
                'calling_code' => '243',
                'deleted_at' => NULL,
            ),
            43 => 
            array (
                'id' => 44,
                'name' => 'Republic of the Congo',
                'calling_code' => '242',
                'deleted_at' => NULL,
            ),
            44 => 
            array (
                'id' => 45,
                'name' => 'Cook Islands',
                'calling_code' => '682',
                'deleted_at' => NULL,
            ),
            45 => 
            array (
                'id' => 46,
                'name' => 'Colombia',
                'calling_code' => '57',
                'deleted_at' => NULL,
            ),
            46 => 
            array (
                'id' => 47,
                'name' => 'Comoros',
                'calling_code' => '269',
                'deleted_at' => NULL,
            ),
            47 => 
            array (
                'id' => 48,
                'name' => 'Cape Verde',
                'calling_code' => '238',
                'deleted_at' => NULL,
            ),
            48 => 
            array (
                'id' => 49,
                'name' => 'Costa Rica',
                'calling_code' => '506',
                'deleted_at' => NULL,
            ),
            49 => 
            array (
                'id' => 50,
                'name' => 'Cuba',
                'calling_code' => '53',
                'deleted_at' => NULL,
            ),
            50 => 
            array (
                'id' => 51,
                'name' => 'Curaçao',
                'calling_code' => '5999',
                'deleted_at' => NULL,
            ),
            51 => 
            array (
                'id' => 52,
                'name' => 'Christmas Island',
                'calling_code' => '61',
                'deleted_at' => NULL,
            ),
            52 => 
            array (
                'id' => 53,
                'name' => 'Cayman Islands',
                'calling_code' => '1345',
                'deleted_at' => NULL,
            ),
            53 => 
            array (
                'id' => 54,
                'name' => 'Cyprus',
                'calling_code' => '357',
                'deleted_at' => NULL,
            ),
            54 => 
            array (
                'id' => 55,
                'name' => 'Czech Republic',
                'calling_code' => '420',
                'deleted_at' => NULL,
            ),
            55 => 
            array (
                'id' => 56,
                'name' => 'Germany',
                'calling_code' => '49',
                'deleted_at' => NULL,
            ),
            56 => 
            array (
                'id' => 57,
                'name' => 'Djibouti',
                'calling_code' => '253',
                'deleted_at' => NULL,
            ),
            57 => 
            array (
                'id' => 58,
                'name' => 'Dominica',
                'calling_code' => '1767',
                'deleted_at' => NULL,
            ),
            58 => 
            array (
                'id' => 59,
                'name' => 'Denmark',
                'calling_code' => '45',
                'deleted_at' => NULL,
            ),
            59 => 
            array (
                'id' => 60,
                'name' => 'Dominican Republic',
                'calling_code' => '1809',
                'deleted_at' => NULL,
            ),
            60 => 
            array (
                'id' => 61,
                'name' => 'Algeria',
                'calling_code' => '213',
                'deleted_at' => NULL,
            ),
            61 => 
            array (
                'id' => 62,
                'name' => 'Ecuador',
                'calling_code' => '593',
                'deleted_at' => NULL,
            ),
            62 => 
            array (
                'id' => 63,
                'name' => 'Egypt',
                'calling_code' => '20',
                'deleted_at' => NULL,
            ),
            63 => 
            array (
                'id' => 64,
                'name' => 'Eritrea',
                'calling_code' => '291',
                'deleted_at' => NULL,
            ),
            64 => 
            array (
                'id' => 65,
                'name' => 'Western Sahara',
                'calling_code' => '212',
                'deleted_at' => NULL,
            ),
            65 => 
            array (
                'id' => 66,
                'name' => 'Spain',
                'calling_code' => '34',
                'deleted_at' => NULL,
            ),
            66 => 
            array (
                'id' => 67,
                'name' => 'Estonia',
                'calling_code' => '372',
                'deleted_at' => NULL,
            ),
            67 => 
            array (
                'id' => 68,
                'name' => 'Ethiopia',
                'calling_code' => '251',
                'deleted_at' => NULL,
            ),
            68 => 
            array (
                'id' => 69,
                'name' => 'Finland',
                'calling_code' => '358',
                'deleted_at' => NULL,
            ),
            69 => 
            array (
                'id' => 70,
                'name' => 'Fiji',
                'calling_code' => '679',
                'deleted_at' => NULL,
            ),
            70 => 
            array (
                'id' => 71,
                'name' => 'Falkland Islands',
                'calling_code' => '500',
                'deleted_at' => NULL,
            ),
            71 => 
            array (
                'id' => 72,
                'name' => 'France',
                'calling_code' => '33',
                'deleted_at' => NULL,
            ),
            72 => 
            array (
                'id' => 73,
                'name' => 'Faroe Islands',
                'calling_code' => '298',
                'deleted_at' => NULL,
            ),
            73 => 
            array (
                'id' => 74,
                'name' => 'Micronesia',
                'calling_code' => '691',
                'deleted_at' => NULL,
            ),
            74 => 
            array (
                'id' => 75,
                'name' => 'Gabon',
                'calling_code' => '241',
                'deleted_at' => NULL,
            ),
            75 => 
            array (
                'id' => 76,
                'name' => 'United Kingdom',
                'calling_code' => '44',
                'deleted_at' => NULL,
            ),
            76 => 
            array (
                'id' => 77,
                'name' => 'Georgia',
                'calling_code' => '995',
                'deleted_at' => NULL,
            ),
            77 => 
            array (
                'id' => 78,
                'name' => 'Guernsey',
                'calling_code' => '44',
                'deleted_at' => NULL,
            ),
            78 => 
            array (
                'id' => 79,
                'name' => 'Ghana',
                'calling_code' => '233',
                'deleted_at' => NULL,
            ),
            79 => 
            array (
                'id' => 80,
                'name' => 'Gibraltar',
                'calling_code' => '350',
                'deleted_at' => NULL,
            ),
            80 => 
            array (
                'id' => 81,
                'name' => 'Guinea',
                'calling_code' => '224',
                'deleted_at' => NULL,
            ),
            81 => 
            array (
                'id' => 82,
                'name' => 'Guadeloupe',
                'calling_code' => '590',
                'deleted_at' => NULL,
            ),
            82 => 
            array (
                'id' => 83,
                'name' => 'Gambia',
                'calling_code' => '220',
                'deleted_at' => NULL,
            ),
            83 => 
            array (
                'id' => 84,
                'name' => 'Guinea-Bissau',
                'calling_code' => '245',
                'deleted_at' => NULL,
            ),
            84 => 
            array (
                'id' => 85,
                'name' => 'Equatorial Guinea',
                'calling_code' => '240',
                'deleted_at' => NULL,
            ),
            85 => 
            array (
                'id' => 86,
                'name' => 'Greece',
                'calling_code' => '30',
                'deleted_at' => NULL,
            ),
            86 => 
            array (
                'id' => 87,
                'name' => 'Grenada',
                'calling_code' => '1473',
                'deleted_at' => NULL,
            ),
            87 => 
            array (
                'id' => 88,
                'name' => 'Greenland',
                'calling_code' => '299',
                'deleted_at' => NULL,
            ),
            88 => 
            array (
                'id' => 89,
                'name' => 'Guatemala',
                'calling_code' => '502',
                'deleted_at' => NULL,
            ),
            89 => 
            array (
                'id' => 90,
                'name' => 'French Guiana',
                'calling_code' => '594',
                'deleted_at' => NULL,
            ),
            90 => 
            array (
                'id' => 91,
                'name' => 'Guam',
                'calling_code' => '1671',
                'deleted_at' => NULL,
            ),
            91 => 
            array (
                'id' => 92,
                'name' => 'Guyana',
                'calling_code' => '592',
                'deleted_at' => NULL,
            ),
            92 => 
            array (
                'id' => 93,
                'name' => 'Hong Kong',
                'calling_code' => '852',
                'deleted_at' => NULL,
            ),
            93 => 
            array (
                'id' => 94,
                'name' => 'Honduras',
                'calling_code' => '504',
                'deleted_at' => NULL,
            ),
            94 => 
            array (
                'id' => 95,
                'name' => 'Croatia',
                'calling_code' => '385',
                'deleted_at' => NULL,
            ),
            95 => 
            array (
                'id' => 96,
                'name' => 'Haiti',
                'calling_code' => '509',
                'deleted_at' => NULL,
            ),
            96 => 
            array (
                'id' => 97,
                'name' => 'Hungary',
                'calling_code' => '36',
                'deleted_at' => NULL,
            ),
            97 => 
            array (
                'id' => 98,
                'name' => 'Indonesia',
                'calling_code' => '62',
                'deleted_at' => NULL,
            ),
            98 => 
            array (
                'id' => 99,
                'name' => 'Isle of Man',
                'calling_code' => '44',
                'deleted_at' => NULL,
            ),
            99 => 
            array (
                'id' => 100,
                'name' => 'India',
                'calling_code' => '91',
                'deleted_at' => NULL,
            ),
            100 => 
            array (
                'id' => 101,
                'name' => 'British Indian Ocean Territory',
                'calling_code' => '246',
                'deleted_at' => NULL,
            ),
            101 => 
            array (
                'id' => 102,
                'name' => 'Ireland',
                'calling_code' => '353',
                'deleted_at' => NULL,
            ),
            102 => 
            array (
                'id' => 103,
                'name' => 'Iran',
                'calling_code' => '98',
                'deleted_at' => NULL,
            ),
            103 => 
            array (
                'id' => 104,
                'name' => 'Iraq',
                'calling_code' => '964',
                'deleted_at' => NULL,
            ),
            104 => 
            array (
                'id' => 105,
                'name' => 'Iceland',
                'calling_code' => '354',
                'deleted_at' => NULL,
            ),
            105 => 
            array (
                'id' => 106,
                'name' => 'Italy',
                'calling_code' => '39',
                'deleted_at' => NULL,
            ),
            106 => 
            array (
                'id' => 107,
                'name' => 'Jamaica',
                'calling_code' => '1876',
                'deleted_at' => NULL,
            ),
            107 => 
            array (
                'id' => 108,
                'name' => 'Jersey',
                'calling_code' => '44',
                'deleted_at' => NULL,
            ),
            108 => 
            array (
                'id' => 109,
                'name' => 'Jordan',
                'calling_code' => '962',
                'deleted_at' => NULL,
            ),
            109 => 
            array (
                'id' => 110,
                'name' => 'Japan',
                'calling_code' => '81',
                'deleted_at' => NULL,
            ),
            110 => 
            array (
                'id' => 111,
                'name' => 'Kazakhstan',
                'calling_code' => '76',
                'deleted_at' => NULL,
            ),
            111 => 
            array (
                'id' => 112,
                'name' => 'Kenya',
                'calling_code' => '254',
                'deleted_at' => NULL,
            ),
            112 => 
            array (
                'id' => 113,
                'name' => 'Kyrgyzstan',
                'calling_code' => '996',
                'deleted_at' => NULL,
            ),
            113 => 
            array (
                'id' => 114,
                'name' => 'Cambodia',
                'calling_code' => '855',
                'deleted_at' => NULL,
            ),
            114 => 
            array (
                'id' => 115,
                'name' => 'Kiribati',
                'calling_code' => '686',
                'deleted_at' => NULL,
            ),
            115 => 
            array (
                'id' => 116,
                'name' => 'Saint Kitts and Nevis',
                'calling_code' => '1869',
                'deleted_at' => NULL,
            ),
            116 => 
            array (
                'id' => 117,
                'name' => 'South Korea',
                'calling_code' => '82',
                'deleted_at' => NULL,
            ),
            117 => 
            array (
                'id' => 118,
                'name' => 'Kosovo',
                'calling_code' => '383',
                'deleted_at' => NULL,
            ),
            118 => 
            array (
                'id' => 119,
                'name' => 'Kuwait',
                'calling_code' => '965',
                'deleted_at' => NULL,
            ),
            119 => 
            array (
                'id' => 120,
                'name' => 'Laos',
                'calling_code' => '856',
                'deleted_at' => NULL,
            ),
            120 => 
            array (
                'id' => 121,
                'name' => 'Lebanon',
                'calling_code' => '961',
                'deleted_at' => NULL,
            ),
            121 => 
            array (
                'id' => 122,
                'name' => 'Liberia',
                'calling_code' => '231',
                'deleted_at' => NULL,
            ),
            122 => 
            array (
                'id' => 123,
                'name' => 'Libya',
                'calling_code' => '218',
                'deleted_at' => NULL,
            ),
            123 => 
            array (
                'id' => 124,
                'name' => 'Saint Lucia',
                'calling_code' => '1758',
                'deleted_at' => NULL,
            ),
            124 => 
            array (
                'id' => 125,
                'name' => 'Liechtenstein',
                'calling_code' => '423',
                'deleted_at' => NULL,
            ),
            125 => 
            array (
                'id' => 126,
                'name' => 'Sri Lanka',
                'calling_code' => '94',
                'deleted_at' => NULL,
            ),
            126 => 
            array (
                'id' => 127,
                'name' => 'Lesotho',
                'calling_code' => '266',
                'deleted_at' => NULL,
            ),
            127 => 
            array (
                'id' => 128,
                'name' => 'Lithuania',
                'calling_code' => '370',
                'deleted_at' => NULL,
            ),
            128 => 
            array (
                'id' => 129,
                'name' => 'Luxembourg',
                'calling_code' => '352',
                'deleted_at' => NULL,
            ),
            129 => 
            array (
                'id' => 130,
                'name' => 'Latvia',
                'calling_code' => '371',
                'deleted_at' => NULL,
            ),
            130 => 
            array (
                'id' => 131,
                'name' => 'Macau',
                'calling_code' => '853',
                'deleted_at' => NULL,
            ),
            131 => 
            array (
                'id' => 132,
                'name' => 'Saint Martin',
                'calling_code' => '590',
                'deleted_at' => NULL,
            ),
            132 => 
            array (
                'id' => 133,
                'name' => 'Morocco',
                'calling_code' => '212',
                'deleted_at' => NULL,
            ),
            133 => 
            array (
                'id' => 134,
                'name' => 'Monaco',
                'calling_code' => '377',
                'deleted_at' => NULL,
            ),
            134 => 
            array (
                'id' => 135,
                'name' => 'Moldova',
                'calling_code' => '373',
                'deleted_at' => NULL,
            ),
            135 => 
            array (
                'id' => 136,
                'name' => 'Madagascar',
                'calling_code' => '261',
                'deleted_at' => NULL,
            ),
            136 => 
            array (
                'id' => 137,
                'name' => 'Maldives',
                'calling_code' => '960',
                'deleted_at' => NULL,
            ),
            137 => 
            array (
                'id' => 138,
                'name' => 'Mexico',
                'calling_code' => '52',
                'deleted_at' => NULL,
            ),
            138 => 
            array (
                'id' => 139,
                'name' => 'Marshall Islands',
                'calling_code' => '692',
                'deleted_at' => NULL,
            ),
            139 => 
            array (
                'id' => 140,
                'name' => 'Macedonia',
                'calling_code' => '389',
                'deleted_at' => NULL,
            ),
            140 => 
            array (
                'id' => 141,
                'name' => 'Mali',
                'calling_code' => '223',
                'deleted_at' => NULL,
            ),
            141 => 
            array (
                'id' => 142,
                'name' => 'Malta',
                'calling_code' => '356',
                'deleted_at' => NULL,
            ),
            142 => 
            array (
                'id' => 143,
                'name' => 'Myanmar',
                'calling_code' => '95',
                'deleted_at' => NULL,
            ),
            143 => 
            array (
                'id' => 144,
                'name' => 'Montenegro',
                'calling_code' => '382',
                'deleted_at' => NULL,
            ),
            144 => 
            array (
                'id' => 145,
                'name' => 'Mongolia',
                'calling_code' => '976',
                'deleted_at' => NULL,
            ),
            145 => 
            array (
                'id' => 146,
                'name' => 'Northern Mariana Islands',
                'calling_code' => '1670',
                'deleted_at' => NULL,
            ),
            146 => 
            array (
                'id' => 147,
                'name' => 'Mozambique',
                'calling_code' => '258',
                'deleted_at' => NULL,
            ),
            147 => 
            array (
                'id' => 148,
                'name' => 'Mauritania',
                'calling_code' => '222',
                'deleted_at' => NULL,
            ),
            148 => 
            array (
                'id' => 149,
                'name' => 'Montserrat',
                'calling_code' => '1664',
                'deleted_at' => NULL,
            ),
            149 => 
            array (
                'id' => 150,
                'name' => 'Martinique',
                'calling_code' => '596',
                'deleted_at' => NULL,
            ),
            150 => 
            array (
                'id' => 151,
                'name' => 'Mauritius',
                'calling_code' => '230',
                'deleted_at' => NULL,
            ),
            151 => 
            array (
                'id' => 152,
                'name' => 'Malawi',
                'calling_code' => '265',
                'deleted_at' => NULL,
            ),
            152 => 
            array (
                'id' => 153,
                'name' => 'Malaysia',
                'calling_code' => '60',
                'deleted_at' => NULL,
            ),
            153 => 
            array (
                'id' => 154,
                'name' => 'Mayotte',
                'calling_code' => '262',
                'deleted_at' => NULL,
            ),
            154 => 
            array (
                'id' => 155,
                'name' => 'Namibia',
                'calling_code' => '264',
                'deleted_at' => NULL,
            ),
            155 => 
            array (
                'id' => 156,
                'name' => 'New Caledonia',
                'calling_code' => '687',
                'deleted_at' => NULL,
            ),
            156 => 
            array (
                'id' => 157,
                'name' => 'Niger',
                'calling_code' => '227',
                'deleted_at' => NULL,
            ),
            157 => 
            array (
                'id' => 158,
                'name' => 'Norfolk Island',
                'calling_code' => '672',
                'deleted_at' => NULL,
            ),
            158 => 
            array (
                'id' => 159,
                'name' => 'Nigeria',
                'calling_code' => '234',
                'deleted_at' => NULL,
            ),
            159 => 
            array (
                'id' => 160,
                'name' => 'Nicaragua',
                'calling_code' => '505',
                'deleted_at' => NULL,
            ),
            160 => 
            array (
                'id' => 161,
                'name' => 'Niue',
                'calling_code' => '683',
                'deleted_at' => NULL,
            ),
            161 => 
            array (
                'id' => 162,
                'name' => 'Netherlands',
                'calling_code' => '31',
                'deleted_at' => NULL,
            ),
            162 => 
            array (
                'id' => 163,
                'name' => 'Norway',
                'calling_code' => '47',
                'deleted_at' => NULL,
            ),
            163 => 
            array (
                'id' => 164,
                'name' => 'Nepal',
                'calling_code' => '977',
                'deleted_at' => NULL,
            ),
            164 => 
            array (
                'id' => 165,
                'name' => 'Nauru',
                'calling_code' => '674',
                'deleted_at' => NULL,
            ),
            165 => 
            array (
                'id' => 166,
                'name' => 'New Zealand',
                'calling_code' => '64',
                'deleted_at' => NULL,
            ),
            166 => 
            array (
                'id' => 167,
                'name' => 'Oman',
                'calling_code' => '968',
                'deleted_at' => NULL,
            ),
            167 => 
            array (
                'id' => 168,
                'name' => 'Pakistan',
                'calling_code' => '92',
                'deleted_at' => NULL,
            ),
            168 => 
            array (
                'id' => 169,
                'name' => 'Panama',
                'calling_code' => '507',
                'deleted_at' => NULL,
            ),
            169 => 
            array (
                'id' => 170,
                'name' => 'Pitcairn Islands',
                'calling_code' => '64',
                'deleted_at' => NULL,
            ),
            170 => 
            array (
                'id' => 171,
                'name' => 'Peru',
                'calling_code' => '51',
                'deleted_at' => NULL,
            ),
            171 => 
            array (
                'id' => 172,
                'name' => 'Philippines',
                'calling_code' => '63',
                'deleted_at' => NULL,
            ),
            172 => 
            array (
                'id' => 173,
                'name' => 'Palau',
                'calling_code' => '680',
                'deleted_at' => NULL,
            ),
            173 => 
            array (
                'id' => 174,
                'name' => 'Papua New Guinea',
                'calling_code' => '675',
                'deleted_at' => NULL,
            ),
            174 => 
            array (
                'id' => 175,
                'name' => 'Poland',
                'calling_code' => '48',
                'deleted_at' => NULL,
            ),
            175 => 
            array (
                'id' => 176,
                'name' => 'Puerto Rico',
                'calling_code' => '1787',
                'deleted_at' => NULL,
            ),
            176 => 
            array (
                'id' => 177,
                'name' => 'North Korea',
                'calling_code' => '850',
                'deleted_at' => NULL,
            ),
            177 => 
            array (
                'id' => 178,
                'name' => 'Portugal',
                'calling_code' => '351',
                'deleted_at' => NULL,
            ),
            178 => 
            array (
                'id' => 179,
                'name' => 'Paraguay',
                'calling_code' => '595',
                'deleted_at' => NULL,
            ),
            179 => 
            array (
                'id' => 180,
                'name' => 'Palestine',
                'calling_code' => '970',
                'deleted_at' => NULL,
            ),
            180 => 
            array (
                'id' => 181,
                'name' => 'French Polynesia',
                'calling_code' => '689',
                'deleted_at' => NULL,
            ),
            181 => 
            array (
                'id' => 182,
                'name' => 'Qatar',
                'calling_code' => '974',
                'deleted_at' => NULL,
            ),
            182 => 
            array (
                'id' => 183,
                'name' => 'Réunion',
                'calling_code' => '262',
                'deleted_at' => NULL,
            ),
            183 => 
            array (
                'id' => 184,
                'name' => 'Romania',
                'calling_code' => '40',
                'deleted_at' => NULL,
            ),
            184 => 
            array (
                'id' => 185,
                'name' => 'Russia',
                'calling_code' => '7',
                'deleted_at' => NULL,
            ),
            185 => 
            array (
                'id' => 186,
                'name' => 'Rwanda',
                'calling_code' => '250',
                'deleted_at' => NULL,
            ),
            186 => 
            array (
                'id' => 187,
                'name' => 'Saudi Arabia',
                'calling_code' => '966',
                'deleted_at' => NULL,
            ),
            187 => 
            array (
                'id' => 188,
                'name' => 'Sudan',
                'calling_code' => '249',
                'deleted_at' => NULL,
            ),
            188 => 
            array (
                'id' => 189,
                'name' => 'Senegal',
                'calling_code' => '221',
                'deleted_at' => NULL,
            ),
            189 => 
            array (
                'id' => 190,
                'name' => 'Singapore',
                'calling_code' => '65',
                'deleted_at' => NULL,
            ),
            190 => 
            array (
                'id' => 191,
                'name' => 'South Georgia',
                'calling_code' => '500',
                'deleted_at' => NULL,
            ),
            191 => 
            array (
                'id' => 192,
                'name' => 'Svalbard and Jan Mayen',
                'calling_code' => '4779',
                'deleted_at' => NULL,
            ),
            192 => 
            array (
                'id' => 193,
                'name' => 'Solomon Islands',
                'calling_code' => '677',
                'deleted_at' => NULL,
            ),
            193 => 
            array (
                'id' => 194,
                'name' => 'Sierra Leone',
                'calling_code' => '232',
                'deleted_at' => NULL,
            ),
            194 => 
            array (
                'id' => 195,
                'name' => 'El Salvador',
                'calling_code' => '503',
                'deleted_at' => NULL,
            ),
            195 => 
            array (
                'id' => 196,
                'name' => 'San Marino',
                'calling_code' => '378',
                'deleted_at' => NULL,
            ),
            196 => 
            array (
                'id' => 197,
                'name' => 'Somalia',
                'calling_code' => '252',
                'deleted_at' => NULL,
            ),
            197 => 
            array (
                'id' => 198,
                'name' => 'Saint Pierre and Miquelon',
                'calling_code' => '508',
                'deleted_at' => NULL,
            ),
            198 => 
            array (
                'id' => 199,
                'name' => 'Serbia',
                'calling_code' => '381',
                'deleted_at' => NULL,
            ),
            199 => 
            array (
                'id' => 200,
                'name' => 'South Sudan',
                'calling_code' => '211',
                'deleted_at' => NULL,
            ),
            200 => 
            array (
                'id' => 201,
                'name' => 'São Tomé and Príncipe',
                'calling_code' => '239',
                'deleted_at' => NULL,
            ),
            201 => 
            array (
                'id' => 202,
                'name' => 'Suriname',
                'calling_code' => '597',
                'deleted_at' => NULL,
            ),
            202 => 
            array (
                'id' => 203,
                'name' => 'Slovakia',
                'calling_code' => '421',
                'deleted_at' => NULL,
            ),
            203 => 
            array (
                'id' => 204,
                'name' => 'Slovenia',
                'calling_code' => '386',
                'deleted_at' => NULL,
            ),
            204 => 
            array (
                'id' => 205,
                'name' => 'Sweden',
                'calling_code' => '46',
                'deleted_at' => NULL,
            ),
            205 => 
            array (
                'id' => 206,
                'name' => 'Swaziland',
                'calling_code' => '268',
                'deleted_at' => NULL,
            ),
            206 => 
            array (
                'id' => 207,
                'name' => 'Sint Maarten',
                'calling_code' => '1721',
                'deleted_at' => NULL,
            ),
            207 => 
            array (
                'id' => 208,
                'name' => 'Seychelles',
                'calling_code' => '248',
                'deleted_at' => NULL,
            ),
            208 => 
            array (
                'id' => 209,
                'name' => 'Syria',
                'calling_code' => '963',
                'deleted_at' => NULL,
            ),
            209 => 
            array (
                'id' => 210,
                'name' => 'Turks and Caicos Islands',
                'calling_code' => '1649',
                'deleted_at' => NULL,
            ),
            210 => 
            array (
                'id' => 211,
                'name' => 'Chad',
                'calling_code' => '235',
                'deleted_at' => NULL,
            ),
            211 => 
            array (
                'id' => 212,
                'name' => 'Togo',
                'calling_code' => '228',
                'deleted_at' => NULL,
            ),
            212 => 
            array (
                'id' => 213,
                'name' => 'Thailand',
                'calling_code' => '66',
                'deleted_at' => NULL,
            ),
            213 => 
            array (
                'id' => 214,
                'name' => 'Tajikistan',
                'calling_code' => '992',
                'deleted_at' => NULL,
            ),
            214 => 
            array (
                'id' => 215,
                'name' => 'Tokelau',
                'calling_code' => '690',
                'deleted_at' => NULL,
            ),
            215 => 
            array (
                'id' => 216,
                'name' => 'Turkmenistan',
                'calling_code' => '993',
                'deleted_at' => NULL,
            ),
            216 => 
            array (
                'id' => 217,
                'name' => 'Timor-Leste',
                'calling_code' => '670',
                'deleted_at' => NULL,
            ),
            217 => 
            array (
                'id' => 218,
                'name' => 'Tonga',
                'calling_code' => '676',
                'deleted_at' => NULL,
            ),
            218 => 
            array (
                'id' => 219,
                'name' => 'Trinidad and Tobago',
                'calling_code' => '1868',
                'deleted_at' => NULL,
            ),
            219 => 
            array (
                'id' => 220,
                'name' => 'Tunisia',
                'calling_code' => '216',
                'deleted_at' => NULL,
            ),
            220 => 
            array (
                'id' => 221,
                'name' => 'Turkey',
                'calling_code' => '90',
                'deleted_at' => NULL,
            ),
            221 => 
            array (
                'id' => 222,
                'name' => 'Tuvalu',
                'calling_code' => '688',
                'deleted_at' => NULL,
            ),
            222 => 
            array (
                'id' => 223,
                'name' => 'Taiwan',
                'calling_code' => '886',
                'deleted_at' => NULL,
            ),
            223 => 
            array (
                'id' => 224,
                'name' => 'Tanzania',
                'calling_code' => '255',
                'deleted_at' => NULL,
            ),
            224 => 
            array (
                'id' => 225,
                'name' => 'Uganda',
                'calling_code' => '256',
                'deleted_at' => NULL,
            ),
            225 => 
            array (
                'id' => 226,
                'name' => 'Ukraine',
                'calling_code' => '380',
                'deleted_at' => NULL,
            ),
            226 => 
            array (
                'id' => 227,
                'name' => 'Uruguay',
                'calling_code' => '598',
                'deleted_at' => NULL,
            ),
            227 => 
            array (
                'id' => 228,
                'name' => 'United States',
                'calling_code' => '1',
                'deleted_at' => NULL,
            ),
            228 => 
            array (
                'id' => 229,
                'name' => 'Uzbekistan',
                'calling_code' => '998',
                'deleted_at' => NULL,
            ),
            229 => 
            array (
                'id' => 230,
                'name' => 'Vatican City',
                'calling_code' => '3906698',
                'deleted_at' => NULL,
            ),
            230 => 
            array (
                'id' => 231,
                'name' => 'Saint Vincent and the Grenadines',
                'calling_code' => '1784',
                'deleted_at' => NULL,
            ),
            231 => 
            array (
                'id' => 232,
                'name' => 'Venezuela',
                'calling_code' => '58',
                'deleted_at' => NULL,
            ),
            232 => 
            array (
                'id' => 233,
                'name' => 'British Virgin Islands',
                'calling_code' => '1284',
                'deleted_at' => NULL,
            ),
            233 => 
            array (
                'id' => 234,
                'name' => 'United States Virgin Islands',
                'calling_code' => '1340',
                'deleted_at' => NULL,
            ),
            234 => 
            array (
                'id' => 235,
                'name' => 'Vietnam',
                'calling_code' => '84',
                'deleted_at' => NULL,
            ),
            235 => 
            array (
                'id' => 236,
                'name' => 'Vanuatu',
                'calling_code' => '678',
                'deleted_at' => NULL,
            ),
            236 => 
            array (
                'id' => 237,
                'name' => 'Wallis and Futuna',
                'calling_code' => '681',
                'deleted_at' => NULL,
            ),
            237 => 
            array (
                'id' => 238,
                'name' => 'Samoa',
                'calling_code' => '685',
                'deleted_at' => NULL,
            ),
            238 => 
            array (
                'id' => 239,
                'name' => 'Yemen',
                'calling_code' => '967',
                'deleted_at' => NULL,
            ),
            239 => 
            array (
                'id' => 240,
                'name' => 'South Africa',
                'calling_code' => '27',
                'deleted_at' => NULL,
            ),
            240 => 
            array (
                'id' => 241,
                'name' => 'Zambia',
                'calling_code' => '260',
                'deleted_at' => NULL,
            ),
            241 => 
            array (
                'id' => 242,
                'name' => 'Zimbabwe',
                'calling_code' => '263',
                'deleted_at' => NULL,
            ),
            242 => 
            array (
                'id' => 243,
                'name' => 'Saint Helena',
                'calling_code' => '290',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}
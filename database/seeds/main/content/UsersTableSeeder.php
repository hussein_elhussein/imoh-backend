<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        \DB::table('users')->insert([
            [
                'id' => 1,
                'role_id' => 1,
                'email' => 'function.op@gmail.com',
                'username' => 'function.op',
                'password' => '$2y$10$6Iu.QUjwgz/oWAc6n1NSOeGPVldFMEdxZ6SD0xIro1EqxEY.q3q36',
                'name' => 'Hussein',
                'owner' => 1,
                'verified' => 1,
                'token' => NULL,
                'deleted_at' => NULL,
                'remember_token' => NULL,
                'settings' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ],
            [
                'id' => 2,
                'role_id' => 1,
                'email' => 'function.op2@gmail.com',
                'username' => 'function.op2',
                'password' => '$2y$10$6Iu.QUjwgz/oWAc6n1NSOeGPVldFMEdxZ6SD0xIro1EqxEY.q3q36',
                'name' => 'Hussein 2',
                'owner' => 1,
                'verified' => 1,
                'token' => NULL,
                'deleted_at' => NULL,
                'remember_token' => NULL,
                'settings' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ]
        ]);
        \DB::table('profiles')->insert([
            [
                'id' => 1,
                'primary' => 1,
                'membership_id' => 1,
                'content_type_id' => 1,
                'location_id' => 11,
                'country_id' => 121,
                'name' => 'Hussein',
                'avatar' => NULL,
                'loc_lat' => '33.91561950',
                'loc_lng' => '35.60225570',
                'profile_id' => '55049215',
                'qr_code' => 'sf032r9s3dd0fzkt01njla7rs7k9sbortp87',
                'socket_id' => NULL,
                'status' => 1,
                'online' => 0,
                'last_active' => NULL,
            ],
            [
                'id' => 2,
                'primary' => 1,
                'membership_id' => 1,
                'content_type_id' => 1,
                'location_id' => 11,
                'country_id' => 121,
                'name' => 'Hussein 2',
                'avatar' => NULL,
                'loc_lat' => '33.91561950',
                'loc_lng' => '35.60225570',
                'profile_id' => '5504592156',
                'qr_code' => 'sf032r9s3ddbf6m01njla7rs7k9sbortp87',
                'socket_id' => NULL,
                'status' => 1,
                'online' => 0,
                'last_active' => NULL,
            ]
        ]);
        for($i = 1; $i < 3; $i++){
            $profile = \App\Profile::findOrFail($i);
            $user = \App\User::findOrFail($i);
            \Illuminate\Support\Facades\Log::info('profile:', [$profile]);
            $this->attachProfile($profile,$user);
        }
        factory(App\User::class,20)->create()->each(function ($random_user) {
            $random_profile = $random_user->profiles()->save(factory(\App\Profile::class)->states('user')->make());
            if($random_profile){
                $content_type = \App\ContentType::find(1);
                if($content_type){
                    foreach ($content_type->sections()->get() as $section){
                        foreach ($section->fields()->get() as $field) {
                            $value = new \App\Value();
                            $value->field_id = $field->id;
                            $faker = Faker\Factory::create();
                            if($field->type === 'checkbox'){
                                $value->value = $faker->boolean(50);
                            }else{
                                $value->value = $faker->sentence(3);
                            }
                            $value->save();
                        }
                    }

                }
            }
        });
    }
    private function attachProfile($profile,$user){
        $content_type = \App\ContentType::find(1);
        foreach ($content_type->sections()->get() as $section){
            foreach ($section->fields()->get() as $field) {
                $value = new \App\Value();
                $value->field_id = $field->id;
                if($field->type === 'checkbox'){
                    $value->value = "1";
                }else{
                    $value->value = "male";
                }
                $value->save();
                $profile->values()->attach($value->id);
            }
        }
        $user->profiles()->attach($profile->id);
    }
}
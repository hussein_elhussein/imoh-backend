<?php

use Illuminate\Database\Seeder;

class OauthClientsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('oauth_clients')->delete();
        
        \DB::table('oauth_clients')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => NULL,
                'name' => 'Laravel Personal Access Client',
                'secret' => 'mDX6d6vmKzO9ZE9T6Ru2M5TQZnS9oBSgC9utVxMf',
                'redirect' => 'http://localhost',
                'personal_access_client' => 1,
                'password_client' => 0,
                'revoked' => 0,
                'created_at' => '2018-03-14 22:46:25',
                'updated_at' => '2018-03-14 22:46:25',
            ),
            1 => 
            array (
                'id' => 2,
                'user_id' => NULL,
                'name' => 'Laravel Password Grant Client',
                'secret' => 'zZY2ezKwPJLTCp0HVmj0vQ6KQmQoYLrVqEt2gDQr',
                'redirect' => 'http://localhost',
                'personal_access_client' => 0,
                'password_client' => 1,
                'revoked' => 0,
                'created_at' => '2018-03-14 22:46:25',
                'updated_at' => '2018-03-14 22:46:25',
            ),
        ));
        
        
    }
}
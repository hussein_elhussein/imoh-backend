<?php

use Illuminate\Database\Seeder;

class OauthAccessTokensTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('oauth_access_tokens')->delete();
        
        \DB::table('oauth_access_tokens')->insert(array (
            0 => 
            array (
                'id' => '2d605c9c1d0baf8e5172c18c5da0d2d921d4b31fd4e161a2fbfdc84b3be3080f300e4f4b7a484d8a',
                'user_id' => 1,
                'client_id' => 2,
                'name' => NULL,
                'scopes' => '[]',
                'revoked' => 0,
                'created_at' => '2018-05-26 17:45:47',
                'updated_at' => '2018-05-26 17:45:47',
                'expires_at' => '2019-05-26 17:45:47',
            ),
            1 => 
            array (
                'id' => '6a0257f48375b099762e9ce517b1039dbce2c90c8894c679636d4d127c279238293891c8149f568a',
                'user_id' => 1,
                'client_id' => 2,
                'name' => NULL,
                'scopes' => '[]',
                'revoked' => 0,
                'created_at' => '2018-06-27 20:18:53',
                'updated_at' => '2018-06-27 20:18:53',
                'expires_at' => '2019-06-27 20:18:53',
            ),
            2 => 
            array (
                'id' => '7e5992dd7a23b434b3bb62a6ed4d376c203609b2b4c6811acbad9564bc64672e22162f39d5258adc',
                'user_id' => 1,
                'client_id' => 2,
                'name' => NULL,
                'scopes' => '[]',
                'revoked' => 0,
                'created_at' => '2018-03-14 22:48:33',
                'updated_at' => '2018-03-14 22:48:33',
                'expires_at' => '2019-03-14 22:48:33',
            ),
            3 => 
            array (
                'id' => 'ddefcda756eb4cd6ec2a27b1ef9ae43c9a9028220aeccf53e593c28ae6fcf6462f09547d0d45fb84',
                'user_id' => 1,
                'client_id' => 2,
                'name' => NULL,
                'scopes' => '[]',
                'revoked' => 0,
                'created_at' => '2018-05-25 14:12:04',
                'updated_at' => '2018-05-25 14:12:04',
                'expires_at' => '2019-05-25 14:12:04',
            ),
        ));
        
        
    }
}
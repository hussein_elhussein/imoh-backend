<?php

use Illuminate\Database\Seeder;

class OauthRefreshTokensTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('oauth_refresh_tokens')->delete();
        
        \DB::table('oauth_refresh_tokens')->insert(array (
            0 => 
            array (
                'id' => 'aeac3c6293f9fbad9e0a1be42ec20e39e1f4a026f2f96c33c8df261d48e20d5006f06721af2cbc1d',
                'access_token_id' => '6a0257f48375b099762e9ce517b1039dbce2c90c8894c679636d4d127c279238293891c8149f568a',
                'revoked' => 0,
                'expires_at' => '2019-06-27 20:18:53',
            ),
        ));
        
        
    }
}
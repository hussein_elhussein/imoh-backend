<?php

use Illuminate\Database\Seeder;

class MembershipTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('membership_types')->delete();
        \DB::table('membership_types')->insert([
            [
                'id' => 1,
                'title' => 'Free',
                'price' => 0,
                'validity_duration' => 12,
                'validity_unit' => "m",
                'is_free' => 1,
                'is_default' => 1,
            ],
            [
                'id' => 2,
                'title' => 'Premium',
                'price' => 100,
                'validity_duration' => 12,
                'validity_unit' => "m",
                'is_free' => 0,
                'is_default' => 0,
            ],
            [
                'id' => 3,
                'title' => 'Gold',
                'price' => 500.50,
                'validity_duration' => 12,
                'validity_unit' => "m",
                'is_free' => 0,
                'is_default' => 0,
            ],

        ]);
    }
}

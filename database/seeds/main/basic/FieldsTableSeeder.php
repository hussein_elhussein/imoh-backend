<?php

use Illuminate\Database\Seeder;

class FieldsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('fields')->delete();
        
        \DB::table('fields')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Gender',
                'type' => 'text',
                'default_body' => 0,
                'default_title' => 0,
                'label' => 'Gender',
                'required' => 1,
                'order' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'options' => NULL,
                'notes' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Direct offer',
                'type' => 'checkbox',
                'default_body' => 0,
                'default_title' => 0,
                'label' => 'Direct offer',
                'required' => 1,
                'order' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'options' => NULL,
                'notes' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Test',
                'type' => 'text',
                'default_body' => 0,
                'default_title' => 0,
                'label' => 'Test',
                'required' => 1,
                'order' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'options' => NULL,
                'notes' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            array (
                'id' => 4,
                'name' => 'Start Date',
                'type' => 'start_date',
                'default_body' => 0,
                'default_title' => 0,
                'label' => 'Start Date',
                'required' => 1,
                'order' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'options' => NULL,
                'notes' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            array (
                'id' => 5,
                'name' => 'End Date',
                'type' => 'end_date',
                'default_body' => 0,
                'default_title' => 0,
                'label' => 'End Date',
                'required' => 1,
                'order' => 2,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'options' => NULL,
                'notes' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}
<?php

use Illuminate\Database\Seeder;

class ContentTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('content_types')->delete();
        
        \DB::table('content_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'type' => 'User',
                'order' => 3,
                'parent_id' => NULL,
                'menu' => 0,
                'tab' => 1,
                'icon' => "people",
                'strict_mode' => 0,
                'show_map' => 0,
                'has_updates' => 0,
                'profile_limit' => 0,
                'moderated' => 0,
                'membership_type_id' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'type' => 'Organization',
                'order' => 1,
                'parent_id' => 1,
                'menu' => 0,
                'tab' => 0,
                'icon' => NULL,
                'strict_mode' => 0,
                'show_map' => 0,
                'has_updates' => 0,
                'profile_limit' => 0,
                'moderated' => 1,
                'membership_type_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'type' => 'Service',
                'order' => 2,
                'parent_id' => NULL,
                'menu' => 0,
                'tab' => 1,
                'icon' => 'imoh-services-icon',
                'strict_mode' => 0,
                'show_map' => 0,
                'has_updates' => 0,
                'profile_limit' => 0,
                'moderated' => 0,
                'membership_type_id' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'type' => 'Required',
                'order' => 1,
                'parent_id' => 3,
                'menu' => 0,
                'tab' => 0,
                'icon' => NULL,
                'strict_mode' => 0,
                'show_map' => 1,
                'has_updates' => 1,
                'profile_limit' => 0,
                'moderated' => 0,
                'membership_type_id' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'type' => 'Offered',
                'order' => 1,
                'parent_id' => 3,
                'menu' => 0,
                'tab' => 0,
                'icon' => NULL,
                'strict_mode' => 0,
                'show_map' => 1,
                'has_updates' => 1,
                'profile_limit' => 0,
                'moderated' => 0,
                'membership_type_id' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            array (
                'id' => 6,
                'type' => 'Office',
                'order' => 1,
                'parent_id' => 2,
                'menu' => 0,
                'tab' => 0,
                'icon' => NULL,
                'strict_mode' => 0,
                'show_map' => 1,
                'has_updates' => 0,
                'profile_limit' => 0,
                'moderated' => 0,
                'membership_type_id' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            array (
                'id' => 7,
                'type' => 'News',
                'order' => 1,
                'parent_id' => 2,
                'menu' => 0,
                'tab' => 0,
                'icon' => 'paper',
                'strict_mode' => 0,
                'show_map' => 0,
                'has_updates' => 0,
                'profile_limit' => 0,
                'moderated' => 0,
                'membership_type_id' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            array (
                'id' => 8,
                'type' => 'Event',
                'order' => 1,
                'parent_id' => 2,
                'menu' => 0,
                'tab' => 0,
                'icon' => 'calendar',
                'strict_mode' => 0,
                'show_map' => NULL,
                'has_updates' => NULL,
                'profile_limit' => 0,
                'moderated' => 0,
                'membership_type_id' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}
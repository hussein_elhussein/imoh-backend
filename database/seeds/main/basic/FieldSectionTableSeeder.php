<?php

use Illuminate\Database\Seeder;

class FieldSectionTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('field_section')->delete();
        
        \DB::table('field_section')->insert(array (
            0 => 
            array (
                'fs_assoc_id' => 1,
                'field_id' => 1,
                'section_id' => 1,
            ),
            1 => 
            array (
                'fs_assoc_id' => 2,
                'field_id' => 2,
                'section_id' => 2,
            ),
            2 => 
            array (
                'fs_assoc_id' => 3,
                'field_id' => 3,
                'section_id' => 3,
            ),
            3 =>
            array (
                'fs_assoc_id' => 4,
                'field_id' => 4,
                'section_id' => 4,
             ),
            4 =>
            array (
                'fs_assoc_id' => 5,
                'field_id' => 5,
                'section_id' => 4,
            ),
        ));
        
        
    }
}
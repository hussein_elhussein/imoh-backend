<?php

use Illuminate\Database\Seeder;

class BasicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            MembershipTypesTableSeeder::class,
            CategoriesTableSeeder::class,
            ContentTypesTableSeeder::class,
            FieldsTableSeeder::class,
            SectionsTableSeeder::class,
            FieldSectionTableSeeder::class,
            ContentTypeSectionTableSeeder::class,
            CategoryContentTypeTableSeeder::class,
            TaggablesTableSeeder::class,
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'parent_id' => NULL,
                'order' => 1,
                'name' => 'Legal',
                'image' => 'categories/May2018/Fo5E7vgLfLfjvEvcFYgs.png',
                'slug' => 'legal',
                
                'created_at' => '2018-05-25 14:56:57',
                'updated_at' => '2018-05-26 17:04:41',
            ),
            1 => 
            array (
                'id' => 2,
                'parent_id' => 1,
                'order' => 1,
                'name' => 'Sub Legal',
                'image' => 'categories/May2018/UcSg3E0Vc4rVdg7U903U.png',
                'slug' => 'sub-legal',
                
                'created_at' => '2018-05-25 14:57:15',
                'updated_at' => '2018-05-26 17:04:21',
            ),
            2 => 
            array (
                'id' => 3,
                'parent_id' => NULL,
                'order' => 1,
                'name' => 'Medical',
                'image' => 'categories/May2018/qdlDbueYybre9xV9P3Pm.png',
                'slug' => 'medical',
                
                'created_at' => '2018-05-25 16:57:46',
                'updated_at' => '2018-05-26 17:04:01',
            ),
            3 => 
            array (
                'id' => 4,
                'parent_id' => 3,
                'order' => 1,
                'name' => 'Sub-medical',
                'image' => 'categories/May2018/L7rY6r5Rr9R3BGFZD1Of.png',
                'slug' => 'sub-medical',
                
                'created_at' => '2018-05-25 16:58:05',
                'updated_at' => '2018-05-26 17:03:42',
            ),
            4 => 
            array (
                'id' => 5,
                'parent_id' => NULL,
                'order' => 1,
                'name' => 'Food',
                'image' => 'categories/May2018/l5bOXbcs0E9u0CvUhnSc.png',
                'slug' => 'food',
                
                'created_at' => '2018-05-26 18:00:06',
                'updated_at' => '2018-05-26 18:00:06',
            ),
            5 => 
            array (
                'id' => 6,
                'parent_id' => 5,
                'order' => 1,
                'name' => 'Sub-food',
                'image' => 'categories/May2018/nVDJgjwOMWivnMWR5SHL.png',
                'slug' => 'sub-food',
                
                'created_at' => '2018-05-26 18:00:43',
                'updated_at' => '2018-05-26 18:00:43',
            ),
            6 =>
                array (
                    'id' => 7,
                    'parent_id' => 2,
                    'order' => 3,
                    'name' => 'Sub sub legal',
                    'image' => 'categories/May2018/UcSg3E0Vc4rVdg7U903U.png',
                    'slug' => 'sub-sub-legal',
                    'created_at' => '2018-05-26 18:00:43',
                    'updated_at' => '2018-05-26 18:00:43',
                ),
        ));
        
        
    }
}
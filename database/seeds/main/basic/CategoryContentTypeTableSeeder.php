<?php

use Illuminate\Database\Seeder;

class CategoryContentTypeTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('category_content_type')->delete();

        \DB::table('category_content_type')->insert([
            [
                'id' => 1,
                'category_id' => 2,
                'content_type_id' => 5,
            ],
            [
                'id' => 2,
                'category_id' => 7,
                'content_type_id' => 4,
            ],
            [
                'id' => 3,
                'category_id' => 1,
                'content_type_id' => 6,
            ],
            [
                'id' => 4,
                'category_id' => 2,
                'content_type_id' => 6,
            ],
            [
                'id' => 5,
                'category_id' => 7,
                'content_type_id' => 6,
            ]
        ]);
        
        
    }
}
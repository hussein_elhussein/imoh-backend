<?php

use Illuminate\Database\Seeder;

class ContentTypeSectionTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('content_type_section')->delete();
        
        \DB::table('content_type_section')->insert(array (
            0 => 
            array (
                'id' => 1,
                'content_type_id' => 1,
                'section_id' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'content_type_id' => 5,
                'section_id' => 2,
            ),
            2 => 
            array (
                'id' => 3,
                'content_type_id' => 4,
                'section_id' => 3,
            ),
            3 =>
            array (
                'id' => 4,
                'content_type_id' => 8,
                'section_id' => 4,
            ),
        ));
        
        
    }
}
import vueResource from 'vue-resource';
import draggable from 'vuedraggable'
Vue.use(vueResource);
const api_url = window.location.origin + '/';
const current_url = window.location.href;
import ContentType from './components/content-type.vue';
import Sections from './components/sections.vue';
import Fields from './components/fields.vue';
const app = new Vue({
    el: '#app',

    components: {
        'content-type': ContentType,
        'sections': Sections,
        'fields': Fields,
        'draggable': draggable,
    }
});
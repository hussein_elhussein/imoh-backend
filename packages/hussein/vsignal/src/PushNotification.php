<?php
namespace VSignal;



use Illuminate\Notifications\Notification;
use VSignal\Events\PushCanceled;
use VSignal\Events\PushError;
use VSignal\Events\PushSent;
use VSignal\Interfaces\Push;

class PushNotification
{
    /**
     * sends a message;
     * @param Push $push
     * @param Notification $notification
     */
    public static function send(Push $push, Notification $notification){
        $message = $push->getMessage();
        $fields = [
            'app_id' => env('ONESIGNAL_APP_ID'),
            'headings' => $message['headings'],
            'contents' => $message['contents'],
        ];
        if($push->getDevices()){
            $fields['include_player_ids'] = $push->getDevices();
        }else{
            $fields['included_segments'] = ["Active Users"];
        }
        if($push->getData()){
            $fields['data'] = $push->getData();
        }
        if($push->getSchedule()){
            $fields['send_after'] = $push->getSchedule();
        }
        $fields = json_encode($fields);
        $env = env('APP_ENV','remote');
        if($env === 'local'){
            return;
        }
        $ch = curl_init();
        curl_setopt_array($ch,[
            CURLOPT_URL => "https://onesignal.com/api/v1/notifications",
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json; charset=utf-8',
                'Authorization: Basic ' . env('ONESIGNAL_API_KEY')
            ],
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HEADER => FALSE,
            CURLOPT_POST => TRUE,
            CURLOPT_POSTFIELDS => $fields,
            CURLOPT_SSL_VERIFYPEER => FALSE,
        ]);
        $response = curl_exec($ch);
        $response = json_decode($response);
        $info = curl_getinfo($ch);
        if($info['http_code'] === 200){
            event(new PushSent($response,$notification));
        }else{
            event(new PushError($response,$notification));
        }
        curl_close($ch);
    }


    /**
     * Cancels a scheduled notification
     * @param string $id: the notification id
     */
    public static function cancel(string $id){
        $env = env('APP_ENV','remote');
        if($env === 'local'){
            return;
        }
        $base_url = "https://onesignal.com/api/v1/notifications/";
        $base_url .= $id . "?";
        $base_url .= "app_id=" . env('ONESIGNAL_APP_ID');
        $ch = curl_init();
        curl_setopt_array($ch,[
            CURLOPT_URL => $base_url,
            CURLOPT_HTTPHEADER => [
                'Authorization: Basic ' . env('ONESIGNAL_API_KEY')
            ],
            CURLOPT_CUSTOMREQUEST => "DELETE",
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_SSL_VERIFYPEER => FALSE,
        ]);
        $response = curl_exec($ch);
        $response = json_decode($response);
        $info = curl_getinfo($ch);
        if($info['http_code'] === 200){
            event(new PushCanceled($response));
        }else{
            event(new PushError($response,null));
        }
        curl_close($ch);
    }
}
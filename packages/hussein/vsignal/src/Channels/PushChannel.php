<?php

namespace VSignal\Channels;

use VSignal\PushNotification;
use Illuminate\Notifications\Notification;
class PushChannel
{

    /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        $push = $notification->toPush($notifiable);
        PushNotification::send($push,$notification);
    }
}
<?php

namespace VSignal\Events;

use Illuminate\Notifications\Notification;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class PushSent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $response;
    public $notification;
    /**
     * Create a new event instance.
     * @param mixed $response: The http response.
     * @param Notification $notification: The notification.
     * @return void
     */
    public function __construct($response, $notification)
    {
        $this->response         = $response;
        $this->notification     = $notification;
    }
}

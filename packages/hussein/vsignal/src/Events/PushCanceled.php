<?php

namespace VSignal\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class PushCanceled
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $response;
    /**
     * Create a new event instance.
     * @param mixed $response: The http response.
     * @return void
     */
    public function __construct($response)
    {
        $this->response = $response;
    }
}

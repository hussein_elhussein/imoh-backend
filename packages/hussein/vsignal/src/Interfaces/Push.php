<?php

namespace VSignal\Interfaces;


class Push
{
    private $devices;
    private $schedule;
    private $data;
    private $message;
    /**
     * IPush constructor.
     * @param array $devices
     * @param string $schedule
     * @param array $data
     */
    function __construct($devices = null, $schedule = null, $data = null)
    {
        $this->devices      = $devices;
        $this->schedule     = $schedule;
        $this->data         = $data;
        $this->message      = [];
    }

    /**
     * Adds the message
     * @param string $body
     * @param string $title
     * @param string $locale
     */
    public function setMessage($body, $title = null, $locale = "en"){
        $message = [
            'contents' => [
                $locale => $body
            ],
        ];
        if($title){
            $message['headings'] = [
                $locale => $title,
            ];
        }
        $this->message = $message;
    }

    /**
     * @return array
     */
    public function getMessage(){
        return $this->message;
    }

    /**
     * @return array|null
     */
    public function getDevices(){
        if($this->devices && count($this->devices)){
            return $this->devices;
        };
        return null;
    }

    /**
     * @return null|string
     */
    public function getSchedule(){
        return $this->schedule;
    }

    /**
     * @return array|null
     */
    public function getData(){
        if($this->data && count($this->data)){
            return $this->data;
        }
        return null;
    }

    /** adds additional locale to the message
     * @param string $body
     * @param string $title
     * @param $locale
     * @return bool: true on success
     */
    public function addLocale($body, $title = null, $locale){
        if($this->message){
            $this->message['contents'][$locale] = $body;
            if($title){
                $this->message['headings'][$locale] = $title;
            }
            return true;
        }
        return false;
    }

}
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/storage/{name}', 'FilesController@view')
//  ->where('name', '.*');
Voyager::routes();
Route::get('/category', 'CategoryController@index');
Route::get('/content-type', 'ContentTypeController@indexMin');
Route::post('/content-type', 'ContentTypeController@store');
Route::get('/membership-types', 'MembershipTypeController@index');
Route::get('/profile/approve/{id}', 'ProfileController@approve');
Route::post('/category/children', 'CategoryController@getChildren');
Route::get('/category/children/{id}', 'CategoryController@getChildren');
Route::get('/role/{min?}', 'RoleController@all');
Route::post('/service-types/child-cats', 'ServiceTypesController@getChildCats');
Route::get('/file/view/{name}', 'FilesController@view')
    ->where('name', '.*');

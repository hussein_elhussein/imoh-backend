<?php
Route::post('/test','TestController@test');
Route::post('/register', 'AuthController@register');
Route::post('login', 'AuthController@issueToken');
Route::get('/roles', 'ProfileController@getPublicRoles');
Route::get('/country', 'CountriesController@index');
Route::get('/file/{name}', 'FilesController@view')
  ->where('name', '.*');

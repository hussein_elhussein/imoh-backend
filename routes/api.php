<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([

  'middleware' => 'api',
  'prefix' => 'auth'

], function ($router) {
  Route::get('/check', 'AuthController@checkAuth');
  Route::post('logout', 'AuthController@logout');
  Route::post('refresh', 'AuthController@refresh');
  Route::get('me', 'ProfileController@me');
});

Route::get('/country', 'CountriesController@index');
Route::post('/test', 'TestController@test');

// ---- CONVERSATION ----
Route::post('/conversation/send', 'ConversationController@send');
Route::post('/conversation/delete', 'ConversationController@destroyMany');
Route::post('/conversation/entered/{id}', 'ConversationController@entered');
Route::post('/conversation/read', 'ConversationController@setRead');
Route::post('/conversation/left/{id}', 'ConversationController@left');
Route::get('/conversation/index/{pag?}', 'ConversationController@index');
Route::resource('/conversation', 'ConversationController',['except' => ['index','create','edit']]);

// ---- CONTENT ----
Route::post('/file/upload', 'FilesController@upload');
Route::get('/file/view/{name}', 'FilesController@view')
  ->where('name', '.*');
Route::resource('/home', 'HomeController', ['except' => [
  'create', 'edit',
]]);


Route::get('/category/min', 'CategoryController@indexMin');
Route::resource('/category', 'CategoryController',['except' => [
  'create', 'edit',
]]);

//like/follow/unfollow
Route::post('/social/like', 'SocialController@like');
Route::post('/social/follow', 'SocialController@follow');

// ---- RATING ----
Route::get('/rating/{rateable}/{id}', 'RatingController@show');
Route::post('/rating/{rateable}', 'RatingController@store');

// ---- ACTION ----
Route::resource('/action', 'ActionController',['except' => ['create','edit']]);

// ---- CONFIRMATION ----
Route::get('/confirmation','ConfirmationController@index');
Route::post('/confirmation','ConfirmationController@filter');

// ---- PACKAGE ----
Route::get('/package','PackageController@index');

// ---- NOTIFICATIONS ----
Route::get('/notification/saved/{update_id}/{type}','NotificationController@getSavedCode');
Route::get('/notification','NotificationController@index');
Route::get('/notification/count','NotificationController@count');
Route::get('/notification/unread','NotificationController@unread');
Route::get('/notification/read_all','NotificationController@readAll');
Route::post('/notification/read','NotificationController@read');

// ---- REMINDERS ----
Route::resource('/reminder','ReminderController',['only' => ['index','update']]);


// ---- CONTENT TYPE ----
Route::resource('/content-type', 'ContentTypeController');

// ---- SETTINGS ----
Route::get('/settings', 'SettingsController@index');

// ---- CONTENT UPDATE ----
Route::get('/content_update/{id}', 'ContentUpdateController@show');
Route::post('/content_update/many', 'ContentUpdateController@showMany');
Route::post('/content_update/{id}', 'ContentUpdateController@store');

// ---- CONTENT ----
Route::post('/content/filter', 'ContentController@filterContent');
Route::resource('/content', 'ContentController');

// ---- COMMENT ----
Route::get('/comment/profile/{id}', 'CommentController@indexProfile');
Route::resource('/comment', 'CommentController');


// ---- MEMBERSHIP TYPES ----
Route::get('/membership-types', 'MembershipTypeController@index');

// ---- ORGANIZATION ----
Route::get('/organization/my_organizations', 'OrganizationController@myOrganizations');
Route::resource('/organization', 'OrganizationController',['except' => ['create','edit']]);

// ---- ACCOUNT ----
Route::post('/profile/updateLocation', 'ProfileController@updateLocation');
Route::post('/profile/disconnected', 'ProfileController@disconnected');

Route::get('/profile/socket/{user_id}', 'ProfileController@getSocket');
Route::post('/profile/socket', 'ProfileController@updateSocketID');
Route::post('/profile/last_seen/{id}', 'ProfileController@updateLastSeen');
Route::post('/profile/device', 'ProfileController@updateDevice');
Route::post('/profile/change_type', 'ProfileController@changeType');
Route::resource('/profile', 'ProfileController');

// ---- ROLE ----
Route::get('/role', 'RoleController@all');

//todo:add rating controller


// ---- BACK-END ----
Route::post('/file/up', 'FilesController@upload');
Route::get('/file/{name}', 'FilesController@view')
  ->where('name', '.*');
Route::post('/file/remove/{name}', 'FilesController@remove')
  ->where('name', '.*');
Route::get('/home', 'HomeController@index');
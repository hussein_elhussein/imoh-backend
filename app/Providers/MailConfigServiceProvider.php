<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
class MailConfigServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
//        $table_exists = Schema::hasTable('settings');
//        if($table_exists){
//          $mail_settings_table = DB::table('settings')->where('group','Mail')->get();
//          $settings = [];
//          foreach ($mail_settings_table as $setting){
//            $settings[$setting->key] = $setting->value;
//          }
//          $mail = [
//            'driver'     => 'smtp',
//            'host'       => $settings['mail.smtp_host'],
//            'port'       => $settings['mail.smtp_port'],
//            'from'       => array('address' => $settings['mail.from_address'], 'name' => $settings['mail.from_name']),
//            'encryption' => $settings['mail.encryption'],
//            'username'   => $settings['mail.username'],
//            'password'   => $settings['mail.password'],
//            'sendmail'   => '/usr/sbin/sendmail -bs',
//          ];
//          if($mail['encryption'] === 'none'){
//            $mail['encryption'] = null;
//          }
//          Config::set('mail',$mail);
//        }

    }
}

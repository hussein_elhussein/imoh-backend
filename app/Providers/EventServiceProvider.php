<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\UpdateCreated' => [
            'App\Listeners\UpdateCreated',
        ],
        'App\Events\UpdateUpdated' => [
            'App\Listeners\UpdateUpdated',
        ],
        'App\Events\ReminderCreated' => [
            'App\Listeners\SendReminder',
        ],
        'App\Events\ReminderCanceled' => [
            'App\Listeners\CancelNotification',
        ],
        'VSignal\Events\PushSent' => [
            'App\Listeners\UpdateNotification',
        ],
        'VSignal\Events\PushError' => [
            'App\Listeners\HandlePushError',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}

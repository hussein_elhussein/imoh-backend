<?php
/**
 * Created by PhpStorm.
 * User: Hussein
 * Date: 5/27/2018
 * Time: 1:44 AM
 */

namespace App\Interfaces;


class IResponse
{
    public $error = null;
    public $success = null;
    public $details = null;
    public $id = null;
    function __construct(bool $error = false, bool $success = true, string $details = null, int $id = null)
    {
        $this->error    = $error;
        $this->success  = $success;
        $this->details  = $details;
        $this->id       = $id;
    }
}
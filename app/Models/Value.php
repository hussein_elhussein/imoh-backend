<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Value extends Model
{
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'field_id',
        'value'
    ];
    protected $hidden = [
        'pivot'
    ];
    public $timestamps = false;
    public function fieldMin(){
        return $this->belongsTo('App\Field','field_id')
            ->select('fields.id');
    }
    public function field(){
      return $this->belongsTo('App\Field');
    }
}

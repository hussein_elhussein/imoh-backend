<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Permission
 *
 * @property int $id
 * @property string $key
 * @property string|null $table_name
 * @property int|null $permission_group_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission wherePermissionGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereTableName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property \Carbon\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Permission onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Permission withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Permission withoutTrashed()
 */
class Permission extends Model
{
    protected $dates = ['deleted_at'];
    protected $hidden = [
        'pivot'
    ];

    public function roles(){
      return $this->belongsToMany('App\Role');
    }

}

<?php

namespace App;

use App\Models\Traits\Minify;
use App\Models\Traits\ProfileHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Like extends Model
{
    use SoftDeletes;
    use ProfileHelper;
    protected $dates = ['deleted_at'];
    protected $table = "likeables";
    protected $fillable = [
        'author_id',
        'likeable_type',
        'likeable_id',
        'like'
    ];
    protected $hidden = ['id','likeable_type','likeable_id','like','created_at','updated_at','deleted_at'];
    public function author(){
        return $this->belongsTo(Profile::class)->select($this->authorFields());
    }

}

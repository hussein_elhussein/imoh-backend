<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Section extends Model
{
    protected $dates = ['deleted_at'];
    public $timestamps = false;
    protected $hidden = ['pivot','created_at','updated_at'];
    public function fields(){
    return $this->belongsToMany('App\Field')->orderBy('fields.order','asc');
  }
}

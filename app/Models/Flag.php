<?php

namespace App;

use App\Models\Traits\ProfileHelper;
use Illuminate\Database\Eloquent\Model;

class Flag extends Model
{
    use ProfileHelper;
    public $fillable = ['comment'];
    protected $hidden = [
        'updated_at',
        'flaggable_type',
        'flaggable_id',
        'user_id'
    ];
    public function flaggable()
    {
        return $this->morphTo();
    }

    public function author()
    {
        return $this->belongsTo('App\User','author_id')
            ->select($this->authorFields());
    }
}

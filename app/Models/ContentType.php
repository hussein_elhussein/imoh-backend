<?php

namespace App;

use App\Models\Traits\Sectionable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \App\Models\Traits\Categoryable;
class ContentType extends Model
{
    protected $dates = ['deleted_at'];
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'pivot',
    ];
    protected $fillable = [
        "type",
        'order',
        'menu',
        'tab',
        'icon',
        'strict_mode',
        'show_map',
        'has_updates',
        'profile_limit',
        'moderated',
        'profile_tab',
    ];
    public $timestamps = false;
    public function getSortedCategoriesAttribute(){
        $sub_cats = $this->categories()->with('parent')->get()->toArray();
        $parents = [];

        // to get the parent categories:
        foreach ($sub_cats as $child){
            $parent = $child['parent'];
            $parent['children'] = [];
            if(count($parents)){
                foreach ($parents as $old_parent){
                    if($old_parent['id'] !== $parent['id']){
                        $parents[] = $parent;
                    }
                }
            }else{
                $parents[] = $parent;
            }
        }
        // to get the children categories:
        foreach ($sub_cats as $child){
            foreach ($parents as $key => $parent){
                if(isset ($parent['id']) && $parent['id'] === $child['parent']['id']){
                    $parents[$key]['children'][] = $child;
                }

            }
        }
        //clean up children parents:
        foreach ($parents as $p_key => $parent){
            foreach ($parent['children'] as $c_key => $child){
                unset($parents[$p_key]['children'][$c_key]['parent']);
            }
        }
        $parents = array_filter($parents, function($item){
            return isset($item['id']);
        });
        return $parents;
    }
    public function sections(){
        return $this->belongsToMany(Section::class);
    }
    public function contents(){
        return $this->hasMany(Content::class);
    }
    public function children(){
        return $this->hasMany(ContentType::class,'parent_id');
    }
    public function parent(){
        return $this->belongsTo(ContentType::class,'parent_id');
    }
    public function categories(){
        return $this->belongsToMany(Category::class)->select('categories.id','categories.parent_id');
    }
    public function role(){
        return $this->belongsTo(Role::class);
    }
    public function translations(){
        return $this->hasMany(Translation::class,'foreign_key')
            ->where('translations.table_name','content_types');
    }
}

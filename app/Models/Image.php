<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

class Image extends Model
{
  use SoftDeletes;
  protected $dates = ['deleted_at'];
  public function getCreatedAtAttribute($value)
  {
    $u = Carbon::parse($value,config('timezone'))->timestamp;
    return $u;
  }
  public function getUpdatedAtAttribute($value)
  {
    $u = Carbon::parse($value,config('timezone'))->timestamp;
    return $u;
  }
}

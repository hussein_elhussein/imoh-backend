<?php

namespace App;

use App\Models\Traits\ProfileHelper;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\Commentable;
use App\Models\Traits\Flaggable;
use App\Models\Traits\Liker;
use App\Models\Traits\Rateable;
use Illuminate\Notifications\Notifiable;
use Overtrue\LaravelFollow\Traits\CanBeFollowed;
use Overtrue\LaravelFollow\Traits\CanFollow;
class Profile extends Model
{
    use Notifiable;
    use Rateable;
    use Commentable;
    use Liker,CanFollow,CanBeFollowed;
    use Flaggable;
    use ProfileHelper;
    protected $fillable = [
        'name',
        'avatar',
        'socket_id',
    ];
    protected $hidden = ['pivot'];
    public function country()
    {
        return $this->belongsTo(Country::class);
    }
    public function scopeMinimal($query)
    {
        return $query->select($this->authorFields());
    }
    public function scopePrimaryProfile($q){
        return $q->where('primary', 1)->where('profile_user.is_member',0);
    }
    public function scopeOrganization($q){
        return $q->where('content_type_id',2);
    }
    public function conversations(){
        return $this->belongsToMany(
            Conversation::class,
            'conversation_profile',
            'profile_id',
            'conversation_id')
            ->with('receiversMin','opener','viewersMin');
    }
    public function sentMessages(){
        return $this->hasMany('App\Message','sender_id');
    }
    public function contents(){
        return $this->hasMany(Content::class);
    }
    public function actions(){
        return $this->morphMany('App\Action', 'actionable');
    }
    public function skills(){
        return $this->belongsToMany('App\Skill');
    }
    //social networks:
    public function sites()
    {
        return $this->morphMany('App\Site', 'siteable');
    }
    public function values(){
        return $this->belongsToMany(Value::class);
    }
    public function users()
    {
        return $this->belongsToMany(
            User::class,'profile_user',
            'profile_id',
            'user_id');
    }
    public function members(){
        $org_type = setting('organization_type_id',2);
        return $this->users()
            ->whereHas('profiles',function($q)use($org_type){
                $q->where([
                    ['content_type_id', $org_type],
                    ['profile_user.is_member', 1]
                ]);
            });
    }
    public function role(){
        return $this->contentType->role();
    }
    public function organizationProfile(){
        return $this->users()->with('organization');
    }
    public function getIsOfficeAttribute(){
        $org = $this->organizationProfile()->first()->organization;
        return $org->count() > 0;

    }
    public function contentType()
    {
        return $this->belongsTo(ContentType::class,'content_type_id');
    }
    public function confirmations(){
        return $this->belongsToMany(Confirmation::class);
    }
    public function reminders(){
        return $this->belongsToMany(Reminder::class);
    }
    public function packages(){
        return $this->hasMany(Package::class,'receiver_id');
    }
    public function routeNotificationForFirebase()
    {
        return "cV5MYMtIjxY:APA91bFrK0_v3C8lEoljcWf1ssxYt-3QfLn48n8E0yRCY2f62z5A1W-R99DHladt61Ws2JmA3yKvb08BOlHRulW22WwghIkVbO26-iDFhw-PeCOrepFcbabpFhjfy0eD_0RFxZ9ctSdTnkGRtCrh5VJuzcVjd5vWYg";
    }
}

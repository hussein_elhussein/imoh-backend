<?php

namespace App;

use App\Models\Traits\Commentable;
use App\Models\Traits\Flaggable;
use App\Models\Traits\Liker;
use App\Models\Traits\ProfileHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * App\Comment
 *
 * @property int $id
 * @property int $post_id
 * @property int $user_id
 * @property string $content
 * @property \Carbon\Carbon|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Post $Post
 * @property-read \App\User $user
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Comment onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment wherePostId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Comment withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Comment withoutTrashed()
 * @mixin \Eloquent
 */
class Comment extends Model
{
    use Liker;
    use Commentable;
    use ProfileHelper;
    use Flaggable;
    protected $fillable = [
        'profile_id',
        'commentable_type',
        'commentable_id',
        'content'
    ];
    public function getCreatedAtAttribute($value)
    {
      $u = Carbon::parse($value,config('timezone'))->timestamp;
      return $u;
    }
    public function getUpdatedAtAttribute($value)
    {
      $u = Carbon::parse($value,config('timezone'))->timestamp;
      return $u;
    }
    public function Post(){
      return $this->belongsTo('App\Post');
    }
    public function author(){
      return $this->belongsTo(Profile::class,'profile_id')->select($this->authorFields());
    }
}

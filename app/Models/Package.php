<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    public function confirmation(){
        return $this->belongsTo(Confirmation::class);
    }
    public function from(){
        return $this->belongsTo(Profile::class,'from_id');
    }
    public function to(){
        return $this->belongsTo(Profile::class,'to_id');
    }
    public function receiver(){
        return $this->belongsTo(Profile::class,'receiver_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\DataType
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $display_name_singular
 * @property string $display_name_plural
 * @property string|null $icon
 * @property string|null $model_name
 * @property string|null $policy_name
 * @property string|null $controller
 * @property string|null $description
 * @property int $generate_permissions
 * @property int $server_side
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DataType whereController($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DataType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DataType whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DataType whereDisplayNamePlural($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DataType whereDisplayNameSingular($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DataType whereGeneratePermissions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DataType whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DataType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DataType whereModelName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DataType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DataType wherePolicyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DataType whereServerSide($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DataType whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DataType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class DataType extends Model
{
    //
}

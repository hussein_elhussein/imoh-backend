<?php

namespace App;

use App\Models\Traits\ProfileHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Office
 *
 * @property int $id
 * @property int $organization_id
 * @property int|null $is_head_office
 * @property float|null $loc_lat
 * @property float|null $loc_lng
 * @property string|null $location_details
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Organization $organization
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Office whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Office whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Office whereIsHeadOffice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Office whereLocLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Office whereLocLng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Office whereLocationDetails($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Office whereOrganizationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Office whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $phone
 * @property string|null $second_phone
 * @property string $email
 * @property string|null $second_email
 * @property \Carbon\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Office onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Office whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Office whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Office wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Office whereSecondEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Office whereSecondPhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Office withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Office withoutTrashed()
 * @property int $user_id
 * @property-read \App\User $author
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Office whereUserId($value)
 * @property int $phone_country_id
 * @property int|null $second_phone_country_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Office wherePhoneCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Office whereSecondPhoneCountryId($value)
 */
class Office extends Model
{
    use ProfileHelper;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'is_head_office',
        'phone_country_id',
        'phone',
        'second_phone_country_id',
        'second_phone',
        'email',
        'second_email',
        'loc_lat',
        'loc_lng',
        'location_details'
    ];

    public function organization(){
        return $this->belongsTo('App\Organization');
    }
    public function author(){
        return $this->belongsTo('App\User')->select($this->authorFields());
    }
    public function location(){
        return $this->belongsTo('App\Location');
    }
}

<?php

namespace App;

use App\Models\Traits\ProfileHelper;
use Illuminate\Database\Eloquent\Model;

class ServiceUpdate extends Model
{
    use ProfileHelper;
    public function service(){
        return $this->belongsTo('App\Service','service_id');
    }
    public function author()
    {
        return $this->belongsTo('App\User','author_id')->select($this->authorFields());
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\ServiceCategory
 *
 * @property int $id
 * @property string $title
 * @property string|null $image
 * @property int|null $parent_id
 * @property-read \App\ServiceCategory|null $parentCategory
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ServiceCategory[] $subCategories
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ServiceCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ServiceCategory whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ServiceCategory whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ServiceCategory whereTitle($value)
 * @mixin \Eloquent
 * @property string|null $keywords
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ServiceCategory whereKeywords($value)
 * @property \Carbon\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\ServiceCategory onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ServiceCategory whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ServiceCategory withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\ServiceCategory withoutTrashed()
 */
class ServiceCategory extends Model
{
//    use SoftDeletes;
//    protected $dates = ['deleted_at'];
//
//    public $timestamps = false;
//
//    public function subCategories(){
//        return $this->hasMany('App\ServiceCategory','parent_id');
//    }
//    public function parentCategory(){
//        return $this->belongsTo('App\ServiceCategory','parent_id');
//    }
}

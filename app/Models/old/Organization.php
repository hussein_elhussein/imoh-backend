<?php

namespace App;

use App\Models\Traits\Flaggable;
use App\Models\Traits\Categoryable;
use App\Models\Traits\ProfileHelper;
use Illuminate\Database\Eloquent\Model;
use Overtrue\LaravelFollow\Traits\CanBeFollowed;
use willvincent\Rateable\Rateable;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Organization
 *
 * @property int $id
 * @property string $name
 * @property string|null $logo
 * @property string $status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Office[] $offices
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Organization whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Organization whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Organization whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Organization whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Organization whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Organization whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ServiceCategory[] $categories
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $members
 * @property string|null $about
 * @property \Carbon\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Organization onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Organization whereAbout($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Organization whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Organization withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Organization withoutTrashed()
 * @property int $user_id
 * @property-read \App\User $author
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Organization whereUserId($value)
 * @property-read mixed $average_rating
 * @property-read mixed $sum_rating
 * @property-read mixed $user_average_rating
 * @property-read mixed $user_sum_rating
 * @property-read \Illuminate\Database\Eloquent\Collection|\willvincent\Rateable\Rating[] $ratings
 */
class Organization extends Model
{
    use SoftDeletes;
    use Rateable;
    use CanBeFollowed;
    use ProfileHelper;
    use Flaggable;
    use Categoryable;
    protected $dates = ['deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'logo', 'about'];

    public function members(){
        return $this->belongsToMany('App\User')->select($this->authorFields());
    }
    public function author(){
        return $this->belongsTo('App\User')->select($this->authorFields());
    }
    public function offices(){
        return $this->hasMany('App\Office');
    }
    public function actions(){
        return $this->morphMany('App\Action', 'actionable');
    }
}

<?php

namespace App;

use App\Models\Traits\Rateable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Action extends Model
{
    use Rateable;
    public function actionable()
    {
        return $this->morphTo();
    }
    public function service(){
        return $this->belongsTo('App\Service');
    }
}

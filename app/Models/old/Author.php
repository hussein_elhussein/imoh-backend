<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    public function user(){
        return $this->hasOne('App\User','users.author')
            ->where('authors.author_type','App\User');
    }
    public function organization(){
        return $this->belongsTo('App\Organization','author_id')
            ->whereExists(function ($query){
                $query->where('authors.author_type','App\Organization');
            });
    }
}

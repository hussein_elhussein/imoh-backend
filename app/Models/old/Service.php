<?php

namespace App;

use App\Models\Traits\Flaggable;
use App\Models\Traits\Liker;
use App\Models\Traits\Rateable;
use App\Models\Traits\ProfileHelper;
use App\Models\Traits\Valueable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use \App\Models\Traits\Author;
/**
 * App\Service
 *
 * @property int $id
 * @property int $author_id
 * @property int $offered
 * @property int $featured
 * @property int $service_type_id
 * @property string|null $keywords
 * @property string $status
 * @property int|null $responder_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Author $author
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Value[] $fieldsValues
 * @property-read \App\User|null $responder
 * @property-read \App\ContentType $serviceType
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereFeatured($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereOffered($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereResponderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereServiceTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property \Carbon\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Service onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Service withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Service withoutTrashed()
 */
class Service extends Model
{
    use SoftDeletes;
    use Liker;
    use ProfileHelper;
    use Rateable;
    use Flaggable;
    use Author;
    use Valueable;
    protected $dates = ['deleted_at'];
    protected $hidden = ['deleted_at'];

//    public function getCreatedAtAttribute($value)
//    {
//      $u = Carbon::parse($value,config('timezone'))->timestamp;
//      return $u;
//    }
    public function getUpdatedAtAttribute($value)
    {
      $u = Carbon::parse($value,config('timezone'))->timestamp;
      return $u;
    }
    public function responder(){
      return $this->belongsTo('App\User','author_id')->select($this->authorFields());
    }
    public function serviceType(){
      return $this->belongsTo('App\ContentType');
    }

    public function updates(){
        return $this->hasMany('App\ServiceUpdate','service_id');
    }

}

<?php

namespace App;

use App\Models\Traits\Flaggable;
use Illuminate\Database\Eloquent\Model;
use \App\Models\Traits\Categoryable;
class Event extends Model
{
    use Categoryable;
    use Flaggable;
    protected $fillable = ['author_id','title','date','body','image','loc_lat','loc_lng'];

}

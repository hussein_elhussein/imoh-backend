<?php

namespace App;

use App\Models\Traits\Flaggable;
use Illuminate\Database\Eloquent\Model;
use \App\Models\Traits\Categoryable;

class News extends Model
{
    use Categoryable;
    use Flaggable;
    protected $fillable = [
        'author_id',
        'image',
        'title',
        'body'
    ];
}

<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends \TCG\Voyager\Models\User
{
    use HasApiTokens,Notifiable;
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'visible_name',
        'email' ,
        'phone',
        'phone_country_id',
        'username',
        'password',
        'gender' ,
        'avatar',
        'job_title',
        'family_members',
        'birthdate' ,
        'country_id',
        'about' ,
        'remember_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'password',
        'remember_token',
        'location_id',
        'country_id'
    ];

    public function getCreatedAtAttribute($value)
    {
      $u = Carbon::parse($value,config('timezone'))->timestamp;
      return $u;
    }
    public function getUpdatedAtAttribute($value)
    {
      $u = Carbon::parse($value,config('timezone'))->timestamp;
      return $u;
    }
    public function getProfileAttribute($relations = null){
        if($relations && count($relations)){
            return $this->profiles()
                ->with($relations)
                ->where('primary',1)
                ->wherePivot('is_member',0)->get();
        }else{
            return $this->profiles()
                ->where('primary',1)
                ->wherePivot('is_member',0)
                ->firstOrFail();
        }
    }
    public function OauthAccessToken(){
      return $this->hasMany('App\OauthAccessToken');
    }
    public function roles(){
        return $this->belongsToMany('App\Role','user_roles');
    }
    public function role(){
        return $this->belongsTo('App\Role');
    }
    public function country(){
      return $this->belongsTo('App\Country','country_id')
        ->select('id','name');
    }
    public function location(){
        return $this->belongsTo('App\Location');
    }
    public function profiles(){
        return $this->belongsToMany(Profile::class,'profile_user');
    }
    public function primaryProfile()
    {
        return $this->profiles()->primaryProfile();
    }
    public function organization()
    {
        return $this->profiles()->organization();
    }
}

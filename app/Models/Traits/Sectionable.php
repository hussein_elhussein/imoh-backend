<?php


namespace App\Models\Traits;
use App\Section;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

trait Sectionable
{
    public function sections(){
        return $this->morphToMany(Section::class,'sectionable');
    }

    /**
     * @param Model|Collection|array|Section $sections
     * @param bool $update
     */
    public function attachSection($sections, $update = false){
        //remove previous sections:
        if($update){
            $this->sections()->detach();
        }
        $ids = [];
        if(is_a($sections,Collection::class)){
            foreach ($sections as $sec){
                array_push($ids,$sec->id);
            }
        }
        if(is_a($sections,Section::class)){
            array_push($ids, $sections->id);
        }
        if(is_array($sections)){
            $ids = $sections;
        }
        if(is_string($sections) || is_int($sections)){
            array_push($ids,$sections);
        }
        foreach ($ids as $id){
            $sectionable = new \App\Sectionable();
            $this->sections()->attach($sectionable,['section_id' => $id]);
        }
    }

    /**
     * Check for values against fields sections.
     *
     * @param array $values: the values to check.
     * @return boolean: true if passed, false otherwise.
     */
    public function validate($values){
        $required_fields = [];
        $fields_ids = [];

        //look for required fields:
        function getRequiredFields($section){
            $req_fields = [];
            foreach ($section->fields as $field) {
                if($field->required === 1){
                    array_push($req_fields, $field->id);
                }
            }
            return $req_fields;
        }
        if(is_a($this->sections,Collection::class)){
            foreach ($this->sections as $section) {
                $required_fields = getRequiredFields($section);
            }
        }elseif(is_a($this->sections,Section::class)){
            $required_fields = getRequiredFields($this->sections);
        }
        //validate field values & existence
        foreach ($values as $value) {
            $passed = isset($value['value']) && isset($value['field_id']);
            if($passed){
                $string_val = (string)$value['value'];
                $filled = strlen($string_val);
                if($filled){
                    array_push($fields_ids, $value['field_id']);
                }
            }
        }
        //check for required fields:
        foreach ($required_fields as $required_field) {
            $founded = in_array($required_field, $fields_ids);
            if(!$founded){
                return false;
            }
        }
        return true;
    }
}
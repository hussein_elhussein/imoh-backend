<?php
/**
 * Created by PhpStorm.
 * User: Hussein
 * Date: 3/15/2018
 * Time: 6:00 PM
 */

namespace App\Models\Traits;


trait ProfileHelper
{
    public function authorFields(){

        return [
            'profiles.id',
            'profiles.name',
            'profiles.avatar',
            'profiles.content_type_id',
            'profiles.location_id',
            'profiles.country_id',
            'profiles.loc_lat',
            'profiles.loc_lng',
            'profiles.online',
            'profiles.last_active',
            'socket_id',
        ];
    }
}
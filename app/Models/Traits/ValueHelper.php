<?php
/**
 * Created by PhpStorm.
 * User: Hussein
 * Date: 6/24/2018
 * Time: 7:11 PM
 */

namespace App\Models\Traits;


use App\Content;
use App\Section;
use App\Value;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
trait ValueHelper
{
    /**
     * Attach values to a content
     *
     * @param Model $content
     * @param array $values
     */
    public function attachValues($content, array $values){
        $this->cleanUpValues($values);
        $old_values = new Collection();
        $ids = [];
        foreach ($values as $value){
            if(isset($value['id'])){
                $ids[] = $value['id'];
            }
        }
        if(count($ids)){
            $old_values = Value::findMany($ids);
        }
        $_values = [];
        foreach ($values as $value) {
            if(isset($value['value'])){
                $filled = null;
                if(gettype($value['value']) === "boolean"){
                    $filled = true;
                }else{
                    $string_val = (string)$value['value'];
                    $filled = strlen($string_val);
                }
                $is_new = !isset($value['id']);
                if($filled){
                    $field_val = null;
                    if($is_new){
                        $field_val = $value;
                    }else{
                        $field_val = $old_values->filter(function($item) use($value){
                            return $item->id === $value['id'];
                        });
                        if($field_val->count()){
                            $field_val = $field_val->first()->toArray();
                            $field_val['value'] = $value['value'];
                        }
                    }
                    if($field_val){
                        //check for duplicates before adding:
                        $_values = array_filter($_values, function($item)use($field_val){
                            return $item['field_id'] !== $field_val['field_id'];
                        });
                        $_values[] = $field_val;
                    }
                }
            }
        }
        //save values:
        foreach ($_values as $key => $value){
            if(isset($value['id'])){
                $val_model = $old_values->filter(function($item) use ($value){
                    return $item->id === $value['id'];
                });
                if($val_model->count()){
                    $val_model->first()->value = $value['value'];
                    $val_model->first()->update();
                }
            }else{
                $val_model = new Value();
                $val_model->fill($value);
                $val_model->save();
                $content->values()->attach($val_model);
            }
        }
    }

    /**
     * Validates values against fields of a content type
     *
     * @param Model $content_type
     * @param array $values
     * @return bool
     */
    public function validateValues($content_type, array $values){
        $required_fields = [];
        $fields_ids = [];
        foreach ($content_type->sections as $section) {
            $required_fields = $this->getRequiredFields($section);
        }
        //validate field values & existence
        foreach ($values as $value) {
            $exist = isset($value['value']) && isset($value['field_id']);
            if($exist){
                $string_val = (string)$value['value'];
                $filled = strlen($string_val);
                if($filled){
                    array_push($fields_ids, $value['field_id']);
                }
            }
        }
        //check for required fields:
        foreach ($required_fields as $required_field) {
            $founded = in_array($required_field->id, $fields_ids);
            if(!$founded){
                return false;
            }
        }
        return true;
    }

    /**
     * Cleans up old & optional fields values
     *
     * @param array $values
     */
    private function cleanUpValues(array $values){
        $ids = [];
        foreach ($values as $value){
            $filled = strlen($value['value']);
            if(isset($value['id']) && !$filled){
                $ids[] = $value['id'];
            }
        }
        if(count($ids)){
            Value::destroy($ids);
        }
    }

    /**
     * Gets the required fields for a section
     *
     * @param Model $section
     * @return array
     */
    private function getRequiredFields(Model $section){
        $req_fields = [];
        foreach ($section->fields as $field) {
            if($field->required === 1){
                array_push($req_fields, $field);
            }
        }
        return $req_fields;
    }

    //todo: validate the value against field type:
    private function checkValueType(Collection $fields, $field_val){
        $found = $fields->filter(function ($item) use($field_val){
            return $item->id === $field_val['field_id'];
        });
        if(!$found->count()){
            return false;
        }
        $target = $found->first();
        switch ($target->type){
            case "number":
                return gettype($field_val['value']) === "integer";
                break;
        }
    }
}
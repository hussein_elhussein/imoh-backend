<?php
/**
 * Created by PhpStorm.
 * User: Hussein
 * Date: 3/17/2018
 * Time: 6:27 PM
 */

namespace App\Models\Traits;


use Illuminate\Database\Eloquent\Builder;

trait Minify
{
    public function scopeMinAuthor(Builder $query){
        return $query->with('author,','author:id');
    }
    public function scopeMinifylikes(Builder $query){
        return $query->select([
            'likes',
            'likes.id',
            'likes.like',
            'likes.author_id',
            'likes.likeable_type',
            'likes.likeable_id',
        ]);
    }
}
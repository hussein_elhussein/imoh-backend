<?php


namespace App\Models\Traits;
use App\Organization;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

trait Author
{
    public function author(){
        return $this->belongsTo('App\Author')
            ->leftJoin('users',function ($join){
               $join->on('users.id','authors.author_id')
               ->where('authors.author_type','App\User');
            })
            ->join('organizations',function ($join){
                $join->on('organizations.id','authors.author_id')
                ->where('authors.author_type','App\Organization');
            });

    }

    /**
     * @param Model $type
     */
    public function attach($type){
        $author_type = get_class($type);
        $author = new \App\Author();
        $author->author_id = $type->id;
        $author->author_type = $author_type;
        $author->save();
        $this->author_id = $author->id;
    }

}
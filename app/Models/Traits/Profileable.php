<?php


namespace App\Models\Traits;
use App\Organization;
use App\Profile;
use App\ProfileType;
use App\Section;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

trait Profileable
{

    /** creates and attach a new profile.
     * @param array $values: the values for the profile fields.
     * @param string $status: the profile status (PENDING or ACTIVE..).
     * @param string $type: type of the profile (default or organization..).
     */
    public function createProfile(array $values, $status = 'ACTIVE',$type = 'default'){
        $profile_type = ProfileType::firstOrCreate(['type' => $type]);
        $profile = new Profile();
        $profile->profile_type_id = $profile_type->id;
        $profile->user_id = $this->id;
        $profile->status = $status;
        $profile->save();
        $profile->attachValues($values);
    }

    public function profiles(){
        return $this->hasMany(Profile::class);
    }
}
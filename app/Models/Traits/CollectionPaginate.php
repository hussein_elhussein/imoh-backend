<?php
/**
 * Created by PhpStorm.
 * User: Hussein
 * Date: 6/25/2018
 * Time: 1:50 AM
 */

namespace App\Models\Traits;


use Illuminate\Database\Eloquent\Collection;

trait CollectionPaginate
{
    /**
     * Paginate collection of items
     *
     * @param Collection $collection
     * @param int $perPage
     * @param string $pageName
     * @param null $fragment
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    function paginate(Collection $collection, int $perPage, string $pageName = 'page', $fragment = null)
    {
        $currentPage = \Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPage($pageName);
        $currentPageItems = $collection->slice(($currentPage - 1) * $perPage, $perPage);
        parse_str(request()->getQueryString(), $query);
        unset($query[$pageName]);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator(
            $currentPageItems,
            $collection->count(),
            $perPage,
            $currentPage,
            [
                'pageName' => $pageName,
                'path' => \Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPath(),
                'query' => $query,
                'fragment' => $fragment
            ]
        );

        return $paginator;
    }
}
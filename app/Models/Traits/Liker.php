<?php


namespace App\Models\Traits;


use App\Like;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

trait Liker
{

    /**
     * get a target likes by users
     * @return mixed
     */
    public function likes(){
        return $this->morphMany('App\Like', 'likeable')->where('like',true);
    }

    /**
     * get a target dislikes by users
     * @return mixed
     */
    public function dislikes(){
        return $this->morphMany('App\Like', 'likeable')->where('like',false);
    }



    public function liked()
    {
        return $this->hasMany('App\Like');
    }
    public function profiles()
    {
        return $this->morphedByMany('App\Profile', 'likeable');
    }

    /** Like a model
     * @param Model $target
     */
    public function like(Model $target){
        $this->doLike($target);
    }


    /** Dislike a model
     * @param Model $target
     */
    public function dislike(Model $target){
        $this->doLike($target,false);
    }


    /** perform the like/dislike
     * @param Model $target
     * @param bool $like
     */
    private function doLike(Model $target, bool $like = true){
        $author = Auth::user()->profile;
        $target_class = get_class($target);
        $like_model = Like::withTrashed()
            ->whereProfileId($author->id)
            ->whereLikeableId($target->id)
            ->whereLikeableType($target_class)
            ->first();
        if($like_model){
            $trashed = $like_model->trashed();
            if((bool)$like_model->like == $like){
                if($trashed){
                    //dd($like_model->like);
                    $like_model->restore();
                }else{
                    $like_model->delete();
                }
            }else{
                if($trashed){
                    $like_model->restore();
                    $like_model->like = $like;
                    $like_model->update();
                }else{
                    $like_model->like = $like;
                    $like_model->update();
                }
            }
        }else{
            Like::create([
                'author_id'       => $author->id,
                'likeable_id'   => $target->id,
                'likeable_type' => $target_class,
                'like' => $like,
            ]);
        }
    }

    
    
}
<?php

namespace App\Models\Traits;

trait Commentable
{
    public function topComment(){
        return $this->morphMany('App\Comment','commentable')->withCount('likes')->orderBy('created_at');
    }
    //todo: limit comments to 5
    public function comments(){
        return $this->morphMany('App\Comment','commentable')->orderBy('created_at','asc');
    }

}
<?php


namespace App\Models\Traits;


use Illuminate\Database\Eloquent\Builder;

/**
 * Trait Translatable
 * @package App\Models\Traits
 */
trait Translatable
{

    /**
     * @param Builder $query
     * @param string $column the columns from target table to select
     * @param string $as get the translation as
     * @return Builder
     */
    public function scopeTranslation(Builder $query,string $column, string $as = null)
    {
        $class = get_class($this);
        $model = new $class();
        $table = $model->getTable();
        //dd($this->getTableColumns());
        return $query->join('translations',function($join) use($table,$column){
              $join->on($table.'.id','=','translations.foreign_key');
              $join->where([
                  ['translations.table_name',$table],
                  ['translations.column_name',$column],
              ]);
          })->select($table . '.*', $as ? 'translations.value as ' . $as: 'translations.value');
    }

//    private function initiateTransSelect($table){
//        $select = [['translations.table_name',$table]];
////        foreach ($this->getTranslatable() as $item) {
////            $add_select = [];
////            array_push($select,$item);
////        }
//        return $select;
//    }
//    private function getTableColumns() {
//        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
//    }
//    private function getTranslatable(){
//        return $this->translatable;
//    }
//    private function selectColumns($table){
//        $fields = $this->translatable;
//        $items = [];
//        $trans_array = [];
//        foreach ($fields as $item){
//            $res = list($key, $value) = each($fields);
//            array_push($items,$res);
//        }
//        foreach ($items as $item) {
//            $trans = null;
//            if(is_string($item['key'])){
//                $trans = $table . '.'. $item['key'] . ' as ' . $item['value'];
//            }else{
//                $trans = $table . '.'. $item['value'];
//            }
//            array_push($trans_array,$trans);
//        }
//        return $trans_array;
//    }
//    private function selectTranslationColumns($table){
//        $fields = $this->translatable;
//        $items = [];
//        $trans_array = [];
//        foreach ($fields as $item){
//            $res = list($key, $value) = each($fields);
//            array_push($items,$res);
//        }
//        foreach ($items as $item) {
//            $trans = null;
//            if(is_string($item['key'])){
//                $trans = $table . '.'. $item['key'] . ' as ' . $item['value'];
//            }else{
//                $trans = $table . '.'. $item['value'];
//            }
//            array_push($trans_array,$trans);
//        }
//        return $trans_array;
//    }


}
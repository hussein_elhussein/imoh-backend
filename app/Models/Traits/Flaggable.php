<?php


namespace App\Models\Traits;
trait Flaggable
{
    public function flags()
    {
        return $this->morphMany('App\Flag', 'flaggable');
    }
}
<?php


namespace App\Models\Traits;
use App\Value;
use Illuminate\Database\Eloquent\Collection;
trait Valueable
{
    // todo: change to many to many relation
    public function values(){
        return $this->morphToMany(Value::class,'valueable');
    }


    public function attachValues(array $values){
        $this->cleanup($values);
        $old_values = new Collection();
        $ids = [];
        foreach ($values as $value){
            if(isset($value['id'])){
                $ids[] = $value['id'];
            }
        }
        if(count($ids)){
            $old_values = Value::findMany($ids);
        }
        $_values = [];
        foreach ($values as $value) {
            if(isset($value['value'])){
                $string_val = (string)$value['value'];
                $filled = strlen($string_val);
                $is_new = !isset($value['id']);
                if($filled){
                    $field_val = null;
                    if($is_new){
                        $field_val = $value;
                    }else{
                        $field_val = $old_values->filter(function($item) use($value){
                            return $item->id === $value['id'];
                        });
                        if($field_val->count()){
                            $field_val = $field_val->first()->toArray();
                            $field_val['value'] = $value['value'];
                        }
                    }
                    if($field_val){
                        //check for duplicates before adding:
                        $_values = array_filter($_values, function($item)use($field_val){
                            return $item['field_id'] !== $field_val['field_id'];
                        });
                        $_values[] = $field_val;
                    }
                }
            }
        }
        //save values:
        foreach ($_values as $key => $value){
            if(isset($value['id'])){
                $val_model = $old_values->filter(function($item) use ($value){
                   return $item->id === $value['id'];
                });
                if($val_model->count()){
                    $val_model->first()->value = $value['value'];
                    $val_model->update();
                }
            }else{
                $val_model = new Value();
                $val_model->fill($value);
                $val_model->save();
                $valueable = new \App\Valueable();
                $this->values()->attach($valueable,['value_id' => $val_model->id]);
            }
        }
    }

    private function cleanup($values){
        $ids = [];
        foreach ($values as $value){
            $filled = strlen($value['value']);
            if(isset($value['id']) && !$filled){
                $ids[] = $value['id'];
            }
        }
        if(count($ids)){
            Value::destroy($ids);
        }
    }
}
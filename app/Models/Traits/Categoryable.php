<?php
/**
 * Created by PhpStorm.
 * User: Hussein
 * Date: 3/13/2018
 * Time: 4:56 PM
 */

namespace App\Models\Traits;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

trait Categoryable
{
    public function categories()
    {
        return $this->morphToMany('App\Category', 'categoryable');
    }
    public function attachCats(Request $request, $update = false){
        //remove previous categories:
        if($update){
            $this->categories()->detach();
        }
        //save categories:
        //if we have sub_categories attach them, otherwise attach categories
        $target_cats = null;
        if(count($request->categories)){
            if(count($request->sub_categories)){
                $target_cats = $request->sub_categories;
            }else{
                $target_cats = $request->categories;
            }
        }
        if($target_cats){
            foreach ($target_cats as $category){
                $new_cat = new \App\Categoryable();
                $this->categories()->attach($new_cat,['category_id' => $category]);
            }
        }
    }

}
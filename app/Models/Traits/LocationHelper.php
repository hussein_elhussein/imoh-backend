<?php
/**
 * Created by PhpStorm.
 * User: Hussein
 * Date: 6/25/2018
 * Time: 12:35 AM
 */

namespace App\Models\Traits;


use App\Country;
use App\Location;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
trait LocationHelper
{
    /**
     * @param Model $content
     * @param array $request_content
     * @return Model
     */
    public function attachLocation($content, $request_content){
        $loc_arr = $request_content['location'];
        $country = Country::where('name',$loc_arr['country']['name'])->select('id')->first();
        $location = Location::where('sub_administrative_area',$loc_arr['sub_administrative_area'])->first();
        if($country){
            if(!$location){
                $location = new Location();
                $location->country_id = $country->id;
                $location->administrative_area = $loc_arr['administrative_area'];
                $location->sub_administrative_area = $loc_arr['sub_administrative_area'];
                $location->postal_code = $loc_arr['postal_code'];
                $location->save();
            }
            $content->location_id = $location->id;
        }
        return $content;
    }
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Category
 *
 * @property int $id
 * @property int|null $parent_id
 * @property int $order
 * @property string $name
 * @property string $slug
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Post[] $posts
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property \Carbon\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Category onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Category withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Category withoutTrashed()
 */
class Category extends Model
{
    //use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $hidden = [
        'pivot',
        'category_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    protected $maps = ['id' => 'category_id'];
    protected $appends = [
        'id',
    ];
    public function getCreatedAtAttribute($value)
    {
        $u = Carbon::parse($value,config('timezone'))->timestamp;
        return $u;
    }
    public function getUpdatedAtAttribute($value)
    {
        $u = Carbon::parse($value,config('timezone'))->timestamp;
        return $u;
    }
    public function getIdAttribute($val){
        if(isset($this->attributes['category_id'])){
            return $this->attributes['category_id'];
        }else{
            return $this->attributes['id'];
        }

    }
    public function categoryable()
    {
        return $this->morphTo();
    }
    public function parent(){
        return $this->belongsTo('App\Category','parent_id');
    }
    public function parentMin(){
        return $this->belongsTo('App\Category','parent_id')->select('categories.id','categories.parent_id');
    }
    public function children(){
        return $this->hasMany('App\Category','parent_id');
    }
    public function childrenMin(){
        return $this->hasMany('App\Category','parent_id')->select('id','parent_id');
    }
    public function posts()
    {
        return $this->morphedByMany('App\Post', 'categoryable');
    }
    public function news()
    {
        return $this->morphedByMany('App\News', 'categoryable');
    }
    public function events()
    {
        return $this->morphedByMany('App\Event', 'categoryable');
    }
    public function serviceTypes()
    {
        return $this->morphedByMany('App\ContentType', 'categoryable');
    }
}

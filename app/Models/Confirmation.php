<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Confirmation extends Model
{
    protected $hidden = ['pivot'];
    public function profiles(){
        return $this->belongsToMany(Profile::class);
    }
    public function content(){
        return $this->belongsTo(Content::class);
    }
    public function contentUpdate(){
        return $this->belongsTo(ContentUpdate::class);
    }
}

<?php

namespace App;

use App\Models\Traits\ProfileHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Message extends Model {
  use ProfileHelper;

  public function author(){
      return $this->belongsTo(Profile::class,'sender_id')->select($this->authorFields());
  }
  public function readers(){
      return $this->belongsToMany(Profile::class,
          'message_reader',
          'message_id',
          'reader_id');
  }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\Flaggable;
use App\Models\Traits\Liker;
use App\Models\Traits\Rateable;
use App\Models\Traits\ProfileHelper;
use App\Models\Traits\Valueable;
use Illuminate\Support\Carbon;
use \App\Models\Traits\Author;

class Content extends Model
{
    use Liker;
    use ProfileHelper;
    use Rateable;
    use Flaggable;
    protected $hidden = [
        'pivot'
    ];
    protected $fillable = [
        'profile_id',
        'content_type_id',
        'location_id',
        'title',
        'body',
        'excerpt',
        'quantity',
        'status',
        'featured',
    ];
    public function author(){
        return $this->belongsTo(Profile::class,'profile_id')->select($this->authorFields());
    }
    public function profile(){
        return $this->belongsTo(Profile::class,'profile_id');
    }
    public function contentType(){
        return $this->belongsTo(ContentType::class);
    }
    public function updates(){
        return $this->hasMany(ContentUpdate::class)->orderBy('content_updates.created_at','asc');
    }
    public function primaryUpdates(){
        return $this->updates()->primary();
    }
    public function values(){
        return $this->belongsToMany(Value::class);
    }
    public function location(){
        return $this->belongsTo(Location::class);
    }
    public function categories(){
        return $this->belongsToMany(Category::class)->select('categories.id');
    }
    public function scopeResolved($q){
        return $q->whereHas('updates', function($query){
            $query->where('content_updates.status',10);
        });
    }
    public function getShouldResolveAttribute()
    {
        $updates = $this->updates()
            ->where('content_updates.status',8)
            ->orWhere('content_updates.status',9)
            ->get();
        $delivered = $updates->filter(function($item){
            return $item->status == 8;
        })->first();
        $received = $updates->filter(function($item){
            return $item->status == 9;
        })->first();
        return $delivered && $received;
    }

    /**
     * @return bool
     */
    public function getReceivedAttribute()
    {
        $updates = $this->updates()
            ->where('content_updates.status',6)
            ->orWhere('content_updates.status',7)
            ->get();
        $sent = $updates->filter(function($item){
            return $item->status == 6;
        })->first();
        $received = $updates->filter(function($item){
            return $item->status == 7;
        })->first();
        return $sent && $received;
    }
}

<?php

namespace App;

use App\Models\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends Model
{
    use SoftDeletes;
    use Translatable;
    protected $dates = ['deleted_at'];
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'calling_code'
    ];
    public function users(){
      return $this->hasMany('App\User')
          ->select('users.id',
              'users.name',
              'users.visible_name',
              'users.avatar',
              'users.role_id',
              'users.location_id',
              'users.country_id',
              'users.loc_lat',
              'users.loc_lng');
    }
    public function posts(){
      return $this->belongsToMany('App\Post');
    }
    public function locations(){
        return $this->hasMany('App\Location');
    }


}

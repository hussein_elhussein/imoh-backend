<?php

namespace App;

use App\Events\ConversationCreated;
use App\Models\Traits\ProfileHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Conversation extends Model
{
    use ProfileHelper;
    public function messages(){
      return $this->hasMany(Message::class);
    }
    public function opener(){
        return $this->belongsTo(Profile::class,'opener_id')
            ->select($this->authorFields());
    }
    public function receivers(){
      return $this->belongsToMany(
          Profile::class,
          'conversation_profile',
          'conversation_id',
          'profile_id');
    }
    public function receiversMin(){
        return $this->receivers()->select($this->authorFields());
    }
    public function viewers(){
        return $this->belongsToMany(
            Profile::class,
            'conversation_viewer',
            'conversation_id',
            'profile_id');
    }
    public function deleters(){
        return $this->belongsToMany(
            Profile::class,
            'conversation_deleter',
            'conversation_id',
            'deleter_id');
    }
    public function viewersMin(){
        return $this->viewers()->select($this->authorFields());
    }
    public function deletersMin(){
        return $this->deleters()->select('conversation_deleter.deleter_id');
    }
    public function lastMessage(){
        return $this->messages()->orderBy('created_at','desc')->limit(1);
    }
}

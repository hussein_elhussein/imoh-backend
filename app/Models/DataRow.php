<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\DataRow
 *
 * @property int $id
 * @property int $data_type_id
 * @property string $field
 * @property string $type
 * @property string $display_name
 * @property int $required
 * @property int $browse
 * @property int $read
 * @property int $edit
 * @property int $add
 * @property int $delete
 * @property string|null $details
 * @property int $order
 * @property-read \App\DataType $data_type
 * @property-read mixed $created_at
 * @property-read mixed $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DataRow whereAdd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DataRow whereBrowse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DataRow whereDataTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DataRow whereDelete($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DataRow whereDetails($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DataRow whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DataRow whereEdit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DataRow whereField($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DataRow whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DataRow whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DataRow whereRead($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DataRow whereRequired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DataRow whereType($value)
 * @mixin \Eloquent
 */
class DataRow extends Model
{
    public function getCreatedAtAttribute($value)
    {
      $u = Carbon::parse($value,config('timezone'))->timestamp;
      return $u;
    }
    public function getUpdatedAtAttribute($value)
    {
      $u = Carbon::parse($value,config('timezone'))->timestamp;
      return $u;
    }
    public function data_type(){
      return $this->belongsTo('App\DataType','data_type_id');
    }
}

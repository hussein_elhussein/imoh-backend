<?php

namespace App;

use App\Models\Traits\ProfileHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Location extends Model
{
    use SoftDeletes;
    use ProfileHelper;
    protected $dates = ['deleted_at'];
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'postal_code'
    ];
    protected $fillable = [
        'administrative_area',
        'sub_administrative_area',
        'postal_code'
    ];
    public $timestamps = false;
    public function country(){
        return $this->belongsTo('App\Country');
    }
}

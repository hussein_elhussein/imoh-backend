<?php

namespace App;

use App\Events\UpdateCreated;
use App\Events\UpdateUpdated;
use App\Models\Traits\ProfileHelper;
use Illuminate\Database\Eloquent\Model;

class ContentUpdate extends Model
{
    use ProfileHelper;
    protected $fillable = [
        'office_id',
        'direct',
        'schedule',
        'content_id',
        'primary',
        'status',
        'details',
        'lat',
        'lng',
    ];
    protected $hidden = [
        'pivot'
    ];

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => UpdateCreated::class,
        'updated' => UpdateUpdated::class,
    ];
    public function content(){
        return $this->belongsTo(Content::class);
    }
    public function parent(){
        return $this->belongsTo(ContentUpdate::class,'parent_id');
    }
    public function office(){
        return $this->belongsTo(Content::class,'office_id');
    }
    public function children(){
        return $this->hasMany(ContentUpdate::class,'previous_id');
    }
    public function previous(){
        return $this->belongsTo(ContentUpdate::class,'previous_id');
    }
    public function author()
    {
        return $this->belongsTo(Profile::class,'profile_id');
    }
    public function scopePrimary($query)
    {
        return $query->where('primary', 1);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reminder extends Model
{
    public function profiles(){
        return $this->belongsToMany(Profile::class);
    }
    public function contentUpdate(){
        return $this->belongsTo(ContentUpdate::class);
    }
}

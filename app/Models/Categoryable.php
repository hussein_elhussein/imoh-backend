<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoryable extends Model
{
    protected $fillable = [
        'category_id',
        'categoryable_id',
        'categoryable_type'
    ];


}

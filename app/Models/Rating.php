<?php

namespace App;

use App\Models\Traits\Flaggable;
use App\Models\Traits\ProfileHelper;
use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    use ProfileHelper;
    use Flaggable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = ['rating'];
    protected $hidden = [
        'updated_at',
        'rateable_type',
        'rateable_id',
        'user_id'
    ];

    /**
     * @return mixed
     */
    public function rateable()
    {
        return $this->morphTo();
    }

    public function author()
    {
        return $this->belongsTo(Profile::class,'profile_id')
            ->select($this->authorFields());
    }
}

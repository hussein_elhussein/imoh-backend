<?php

namespace App;

use App\Models\Traits\ProfileHelper;
use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    use ProfileHelper;
    public function profiles(){
        return $this->belongsToMany(Profile::class)->select($this->authorFields());
    }
}

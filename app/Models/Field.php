<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    public $timestamps = false;
    protected $hidden = ['pivot','created_at','updated_at'];
    protected $fillable = [
        'name',
        'label',
        'type',
        'order',
        'required',
        'browse',
        'edit',
        'add',
        'delete',
        'default_title',
        'default_body',
        'options',
        'notes'
    ];
    public function value(){
      return $this->hasMany('App\Value');
    }
    public function translations(){
        return $this->hasMany(Translation::class,'foreign_key')
            ->where('translations.table_name','fields');
    }
}

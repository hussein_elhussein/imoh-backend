<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Membership extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    public $timestamps = false;
  public function getCreatedAtAttribute($value)
  {
    $u = Carbon::parse($value,config('timezone'))->timestamp;
    return $u;
  }
  public function getUpdatedAtAttribute($value)
  {
    $u = Carbon::parse($value,config('timezone'))->timestamp;
    return $u;
  }
}

<?php

namespace App\Tasks;


use App\ContentUpdate;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

class UpdateContent
{
    /**
     * Find and update the status of expired content updates
     */
    public static function run(){
        $updates = ContentUpdate::where('status',2)->with('author')->get();
        foreach ($updates as $update) {
            $timezone = $update->author->timezone;
            if(!$timezone){
                $timezone = env('TIMEZONE');
            }
            $schedule = Carbon::createFromTimestamp($update->schedule,$timezone);
            if($schedule->isPast()){
                $update->status = 11;
                $update->timestamps = false;
                $update->update();
               Log::info('UpdateContent: updated ' . $update->id);
            }
        }
    }
}
<?php

namespace App\Mail;

use App\EmailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewCampaign extends Mailable
{
    use Queueable, SerializesModels;
    protected $template;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(EmailTemplate $template)
    {
        $this->template = $template;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->template->title)
          ->view('mail.new_campaign',['body' => $this->template->body]);
    }
}

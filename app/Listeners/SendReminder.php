<?php

namespace App\Listeners;

use App\Events\ReminderCreated;
use App\Notifications\ReminderNote;
use App\Profile;
use App\Reminder;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use App\Notifications\ReminderCreated as ReminderNotification;
class SendReminder implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ReminderCreated  $event
     * @return void
     */
    public function handle(ReminderCreated $event)
    {
        $profiles = $event->profiles;
        Log::info('sending reminders to:', [$profiles->first()->id,$profiles->last()->id]);
        $text = "This is to remind you of your meeting with ";
        $text_a =  $text . $profiles->first()->name;
        $text_b = $text . $profiles->last()->name;
        //Log::info('profiles to remind:', $author);
        //Send immediate notifications for both users:
        Notification::send($profiles,
            new ReminderNotification($event->reminder)
        );
        // Send scheduled notification
        // Notify first profile:
        if($profiles->first()->device_token){
            $this->notifyProfile(
               $profiles->first(),
                $event->contentUpdate->schedule,
                $event->reminder,
                $text_a);
        }

        // Notify the second profile:
        if($profiles->last()->device_token){
            $this->notifyProfile(
                $profiles->last(),
                $event->contentUpdate->schedule,
                $event->reminder,
                $text_b);
        }
    }

    /**
     * @param Profile $profile
     * @param string $schedule
     * @param Reminder $reminder
     * @param string $text
     */
    private function notifyProfile($profile, $schedule, $reminder, $text){
        Log::info('listeners->SendReminder->notifyProfile');
        $timezone = "Asia/Beirut";
        if($profile->timezone){
            $timezone = $profile->timezone;
        }
        $date_inst = Carbon::createFromTimestamp($schedule, $timezone);
        $format = "Y-m-d H:i:s \G\M\TO";
        $date_b = $date_inst->subMinutes(2)->format($format);
        $date_c = $date_inst->subMinutes(1)->format($format);
        $schedules = [
            $date_b,
            $date_c,
        ];
        for($i = 0; $i < count($schedules); $i++){
            $profile->notify(new ReminderNote($reminder, $schedules[$i], $text));
        }
    }
}

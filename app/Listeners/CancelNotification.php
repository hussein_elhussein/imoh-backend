<?php

namespace App\Listeners;

use App\Events\ReminderCanceled;
use App\Notifications\ReminderNote;
use Illuminate\Contracts\Queue\ShouldQueue;
use VSignal\PushNotification;

class CancelNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ReminderCanceled  $event
     * @return void
     */
    public function handle(ReminderCanceled $event)
    {
        foreach ($event->reminder->profiles as $profile) {
            $notifications = $profile->notifications->filter(function ($item) use ($event){
                if( isset($item->data['data']['id']) &&
                    isset($item->data['push_id']) &&
                    $item->type === ReminderNote::class) {
                    return $item->data['data']['id'] === $event->reminder->id;
                }else{
                    return false;
                }
            });
            if(count($notifications->toArray())){
                foreach ($notifications as $item) {
                    PushNotification::cancel($item->data['push_id']);
                }
            }
        }
    }
}

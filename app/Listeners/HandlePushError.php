<?php

namespace App\Listeners;

use Illuminate\Support\Facades\Log;
use VSignal\Events\PushError;
use Illuminate\Contracts\Queue\ShouldQueue;

class HandlePushError implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PushError  $event
     * @return void
     */
    public function handle(PushError $event)
    {
        Log::info('push error:', [$event->response]);
    }
}

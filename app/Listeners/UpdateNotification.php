<?php

namespace App\Listeners;


use VSignal\Events\PushSent;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class UpdateNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PushSent  $event
     * @return void
     */
    public function handle(PushSent $event)
    {
       if(isset($event->response->id)){
           $notification  = DatabaseNotification::find($event->notification->id);
           if($notification){
               $new_data = $notification->data;
               $new_data['push_id'] = $event->response->id;
               $notification->timestamps = false;
               $notification->data = $new_data;
               $notification->update();
           }
       }
    }
}

<?php

namespace App\Listeners;

use App\Confirmation;
use App\Content;
use App\ContentUpdate;
use App\Events\UpdateCreated as CreatedEvent;
use App\Events\ReminderCreated;
use App\Notifications\ConfirmationCreated;
use App\Package;
use App\Reminder;
use Illuminate\Contracts\Queue\ShouldQueue;
use \App\Notifications\UpdateCreated as UpdateNote;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

class UpdateCreated implements ShouldQueue
{
    private $update;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CreatedEvent  $event
     * @return void
     */
    public function handle(CreatedEvent $event)
    {
        $update = $event->contentUpdate;
        $update->load('content.profile','content.values');
        $confirmed = false;
        $office_status = [6,7,8,12,13,14,15,16,17];
        $isOfficeStatus = $update->office_id && in_array($update->status, $office_status);
        $this->update = $update;
        if($isOfficeStatus){
            //for office waiting status
            if($update->status === 12){
                $this->waitingOffice();
            }elseif($update->status === 6 && $update->previous_id){
                $this->packageDelivery(true);
            }elseif($update->status === 7 && $update->previous_id){
                $this->packageDelivery();
            }elseif($update->status === 8 && $update->previous_id){
                $this->packageDelivered();
            }elseif($update->status === 15){
                $this->appointmentRequest();
            }elseif($update->status === 16 || $update->status === 17){
                $this->appointmentResponse();
            }else{
                $this->otherStatus($update->previous_id);
            }
        }else if($update->status === 9){
            $this->needyReceived();
        }else{
            $this->otherStatus($update->parent_id);
        }
        // if accepted, send reminder and qr code:
        if($update->status === 2 && $update->parent_id && !$update->office_id){
            $this->accepted();
        }
        if($update->status === 13 && $update->parent_id){
            $this->officeAccepted();
        }
        if($update->status === 16){
            $this->needyAccepted();
        }
        //for resolved
        // cancel reminders:
        $resolved = $update->status === 10 || ($update->status === 7 && $confirmed);
        if($resolved){
            $this->updateReminders();
        }
    }
    private function waitingOffice(){
        $update = $this->update;
        $update->load('author','parent.author');
        $update->load('office.profile');
        $update->office->profile->notify(new UpdateNote($update));

        //disable actions on the main update (which made by claimer):
        $parent = $update->parent;
        if($parent){
            $parent->timestamps = false;
            $parent->actions_allowed = 0;
            $parent->update();
        }
    }
    private function createPackage($from_id, $to_id, $receiver_id, $confirmation_id){
        $package = new Package();
        $package->from_id = $from_id;
        $package->to_id = $to_id;
        $package->receiver_id = $receiver_id;
        $package->confirmation_id = $confirmation_id;
        $package->save();
        consoleOutput('warning', 'package created:', $package);
    }

    // when the package is delivered/received by office/helper:
    private function packageDelivery($sent = false){
        $update = $this->update;
        // helper sent the package
        $update->load('previous.author');
        $prev_author = $update->previous->author;
        $receiver_id = $prev_author->id;
        if($prev_author->id === $update->author->id){
            $prev_update = ContentUpdate::with('author')->find($update->previous->previous_id);
            $prev_author = $prev_update->author;
            $receiver_id = $update->author->id;
        }
        $prev_author->notify(new UpdateNote($update));
        if(!$update->content->received){
            $profiles = collect([$prev_author,$update->author]);
            $confirmation = $this->createPackageConfirmation($update,$profiles);
            //create another update with received status 7:
            $status = $sent? 7: 6;
            $this->createUpdate($update,$prev_author->id, $status);
            $this->updateReminders(true);
            $parent = ContentUpdate::with('author')->find($update->parent_id);
            $this->createPackage($update->content->profile->id,$parent->author->id,$receiver_id,$confirmation->id);
        }
    }
    //when the package is delivered to needy:
    private function packageDelivered(){
        $update = $this->update;
        // delivered by office, notify the needy:
        $update->load('parent.author');
        $update->parent->author->notify(new UpdateNote($update));
        $resolved = $this->checkResolved($update);
        if(!$resolved){
            $this->createUpdate($update,$update->parent->author->id, 9);
        }
    }
    private function appointmentRequest(){
        $update = $this->update;
        //if the office requesting appointment with the needy, notify the needy:
        $update->load('office');
        $parent = ContentUpdate::with('author')->find($update->parent_id);
        $parent->author->notify(new UpdateNote($update));
    }
    private function appointmentResponse(){
        // if the needy accepted or rejected the appointment:
        $update = $this->update;
        $update->load('office');
        $previous = ContentUpdate::with('author')->find($update->previous_id);
        $previous->author->notify(new UpdateNote($update));
        $previous->timestamps = false;
        $previous->actions_allowed = 0;
        $previous->update();
    }
    private function needyReceived(){
        $update = $this->update;
        $update->load('office.profile');
        $update->office->profile->notify(new UpdateNote($update));
        $resolved = $this->checkResolved($update);
        if(!$resolved){
            $this->createUpdate($update, $update->office->profile->id,8);
        }
    }
    private function accepted(){
        $update = $this->update;
        $update->load('author','parent.author');
        $this->createReminders($update);
        $this->createConfirmation($update);
    }
    private function officeAccepted(){
        $update = $this->update;
        //send reminder to the office, the author, and first update author:
        $update->load('author');
        $otherParty = $update->content->profile;
        $this->createReminders($update, $otherParty);
        $this->createConfirmation($update, true);
    }
    private function needyAccepted(){
        $update = $this->update;
        $update->load('author');
        $otherParty = $update->previous->author;
        $this->createReminders($update,$otherParty);
    }
    private function otherStatus($old_id = null){
        // for other office status:
        if($old_id){
            $old_update = ContentUpdate::with('author')->find($old_id);
            $old_update->timestamps = false;
            $old_update->actions_allowed = 0;
            $old_update->update();
            $old_update->author->notify(new UpdateNote($this->update));
        }else{
            $this->update->load('author');
            $this->update->content->profile->notify(new UpdateNote($this->update));
        }
    }
    private function checkResolved($update){
        $content = $update->content;
        if($content->shouldResolve && $content->status !== 10){
            $content->status = 10;
            $content->timestamps = false;
            $content->update();
            $this->updateReminders();
            return true;
        }else{
            return false;
        }
    }
    private function createUpdate($update, $author_id, $status){
        $new_up = new ContentUpdate();
        $new_up->fill($update->toArray());
        $new_up->profile_id = $author_id;
        $new_up->previous_id = $update->id;
        $new_up->parent_id = $update->parent_id;
        $new_up->status = $status;
        $new_up->primary = 1;
        $new_up->save();
    }
    private function updateReminders($office = null){
        $update = $this->update;
        if($office){
            $reminder_update = $update->content->updates()
                ->where('content_updates.status', 13)
                ->first();
            $reminders = Reminder::where('content_update_id', $reminder_update->id)->get();
        }else{
            $reminders = Reminder::where('content_update_id', $update->previous_id)->get();
        }
        Log::info('reminders to update:',[$reminders->pluck('id')->toArray()]);
        foreach ($reminders as $reminder) {
            $reminder->actions_allowed = false;
            $reminder->status = 1;
            $reminder->update();
        }
    }
    private function createReminders($update, $otherParty = null){
        // make a reminder
        $ids = [];
        $ids[] = $update->author->id;
        $profiles = collect([$update->author]);
        if($otherParty){
            $profiles->push($otherParty);
            $ids[] = $otherParty->id;
        }else{
            $profiles->push($update->parent->author);
            $ids[] = $update->parent->author->id;
        }
        $reminder = new Reminder();
        $reminder->content_update_id = $update->id;
        $reminder->save();
        $reminder->profiles()->attach($ids);
        $_update = $otherParty? $update->previous: $update->parent;
        event(new ReminderCreated($reminder, $profiles, $_update));
    }
    private function createPackageConfirmation($update, $profiles){
        if(!$update->relationLoaded('content')){
            $update->load('content.values');
        }
        if(!$update->relationLoaded('office')){
            $update->load('office.profile');
        }
        $code = generateUniqueID('confirmations','code',100);
        $confirmation = new Confirmation();
        $confirmation->code = $code;
        $confirmation->content_id = $update->content_id;
        $confirmation->content_update_id = $update->id;
        $confirmation->save();
        $ids = $profiles->pluck('id')->toArray();
        $confirmation->profiles()->attach($ids);
        $confirmation->setAttribute('contentUpdate',$update);
        $confirmation->setAttribute('profiles',$profiles);
//        $confirmation->load(['profiles','contentUpdate.office.profile','contentUpdate.content.values']);
        Notification::send($profiles,new ConfirmationCreated($confirmation));
        return $confirmation;
    }
    private function createConfirmation($update,$office = false){
        $code = generateUniqueID('confirmations','code',100);
        $profiles = Collect([$update->author,$update->parent->author]);
        if($office){
            $profiles->push($update->content->profile);
        }
        $conf = new Confirmation();
        $conf->code = $code;
        $conf->content_id = $update->content_id;
        $conf->content_update_id = $update->id;
        $conf->save();
        $ids = $profiles->pluck('id')->toArray();
        $conf->profiles()->attach($ids);
        Notification::send($profiles,new ConfirmationCreated($conf));
    }
}

<?php

namespace App\Listeners;

use App\Events\UpdateUpdated as UpdateEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateUpdated implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdateEvent  $event
     * @return void
     */
    public function handle(UpdateEvent $event)
    {
        $update = $event->contentUpdate;
        if($update->status === 11 && $update->parent_id){

        }
    }
}

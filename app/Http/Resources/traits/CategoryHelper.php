<?php
/**
 * Created by PhpStorm.
 * User: Hussein
 * Date: 6/23/2018
 * Time: 4:12 PM
 */

namespace App\Http\Resources\traits;


trait CategoryHelper
{
    public function getSortedCategories($categories,$parent_attr = 'parent',$children_attr = 'children'){
        $sub_cats = $categories->toArray();
        $parents = [];

        // to get the parent categories:
        foreach ($sub_cats as $child){
            $parent = $child[$parent_attr];
            $parent[$children_attr] = [];
            if(count($parents)){
                foreach ($parents as $old_parent){
                    if($old_parent['id'] !== $parent['id']){
                        $parents[] = $parent;
                    }
                }
            }else{
                $parents[] = $parent;
            }
        }
        // to get the children categories:
        foreach ($sub_cats as $child){
            foreach ($parents as $key => $parent){
                if(isset ($parent['id']) && $parent['id'] === $child[$parent_attr]['id']){
                    $parents[$key][$children_attr][] = $child;
                }

            }
        }
        //clean up children parents:
        foreach ($parents as $p_key => $parent){
            foreach ($parent[$children_attr] as $c_key => $child){
                unset($parents[$p_key][$children_attr][$c_key][$parent_attr]);
            }
        }
        $parents = array_filter($parents, function($item){
            return isset($item['id']);
        });
        return $parents;
    }
}
<?php

namespace App\Http\Resources;

use App\Http\Resources\traits\CategoryHelper;
use Illuminate\Http\Resources\Json\JsonResource;

class Category extends JsonResource
{
    use CategoryHelper;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}

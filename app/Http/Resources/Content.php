<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Content extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            "id" =>  $this->id,
            'profile_id' => $this->profile_id,
            "content_type_id" => $this->content_type_id,
            "parent_id"=> null,
            "title"=> $this->title,
            "status"=> $this->status,
            "lat"=> $this->loc_lat,
            "lng"=> $this->loc_lng,
            "created_at"=> $this->created_at->timestamp,
            "updated_at"=> $this->updated_at->timestamp,

        ];
        if($this->body && strlen($this->body)){
            $data['body'] = $this->body;
        }
        if($this->excerpt && strlen($this->excerpt)){
            $data['excerpt'] = $this->excerpt;
        }
        if($this->featured){
            $data['featured'] = $this->featured;
        }
        if($this->parent_id){
            $data['parent_id'] = $this->parent_id;
        }
        if($this->relationLoaded('values')){
            if($this->values->count()){
                $data['values'] = $this->values;
            }
        }
        if($this->relationLoaded('updates')){
            if($this->updates->count()){
                $data['updates'] = new ContentUpdateCollection($this->updates);
            }
        }
        if($this->relationLoaded('primaryUpdates')){
            if($this->primaryUpdates->count()){
                $data['updates'] = new ContentUpdateCollection($this->primaryUpdates->sortBy('status'));
            }
        }
        if($this->relationLoaded('location')){
            if($this->location){
                $data['location'] = $this->location;
                if($this->location->relationLoaded('country')){
                    unset($data['location']['country_id']);
                    unset($data['location']['country']['name']);
                }
            }
        }
        if($this->relationLoaded('categories')){
            $cats = [];
            foreach ($this->categories as $category) {
                $cats[] = [
                    'id' => $category->id
                ];
            }
            $data['categories'] = $cats;
        }
        if($this->relationLoaded('author')){
            $data['author'] = new Profile($this->author,null,true);
        }
        if($this->relationLoaded('profile')){
            $data['author'] = new Profile($this->profile,null,true);
        }
        return $data;
    }
}

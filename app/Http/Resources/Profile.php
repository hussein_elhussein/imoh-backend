<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use function GuzzleHttp\Psr7\str;
use Illuminate\Http\Resources\Json\JsonResource;

class Profile extends JsonResource
{
    protected $min;
    protected $contentType;
    public function __construct($resource, $count = null, $min = false)
    {
        $this->min = $min;
        parent::__construct($resource);
    }

    public function min($value){
        $this->min = $value;
        return $this;
    }
    public function contentType($value){
        $this->min = $value;
        return $this;
    }
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [];
        if($this->min){
            $auth_fields = $this->minFieldsArr();
            foreach ($auth_fields as $auth_field){
                $str_val = (string) $this->$auth_field;
                $has_val = strlen($str_val);
                if(gettype($this->$auth_field) === 'boolean'){
                    $has_val = true;
                }
                if($has_val){
                    $data[$auth_field] = $this->$auth_field;
                }
            }
            if(!$this->name || !strlen($this->name)){
                $data['name'] = "User";
            }
        }else{
            $data = [
                'id' => $this->id,
                'primary' => $this->primary? true: false,
                'membership_id' => $this->membership_id,
                'content_type_id' => $this->content_type_id,
                'location_id' => $this->location_id,
                'country_id' => $this->country_id,
                'profile_id' => $this->profile_id,
                'qr_code' => $this->qr_code,
                'name' => $this->name? $this->name: "User",
                'lat' => $this->loc_lat,
                'lng' => $this->loc_lng,
                'socket_id' => $this->socket_id,
                'status' => $this->status,
                'online' => $this->online? true: false,
                'last_active' => $this->last_active,
                'isOffice' => $this->isOffice,

            ];
            //load profile members:
            if($this->relationLoaded('members')){
                if($this->members->count()){
                    $data['profiles'] = User::collection($this->members);
                }
            }
            if($this->relationLoaded('organizationProfile')){
                if($this->organizationProfile->count()){
                    $orgProfile = $this->organizationProfile->first()->organization->first();
                    $data['organization'] = new Profile($orgProfile);
                }
            }
            if($this->relationLoaded('ratings')){
                $data['rating_average'] = $this->ratings->avg('rating');
                $data['rating_count'] = $this->ratings->count();
            }
            if($this->relationLoaded('membership_id')){
                unset($data['membership_id']);
                $data['membership'] = $this->membership;
            }
            if($this->relationLoaded('contentType')){
                unset($data['content_type_id']);
                $data['content_type'] = $this->contentType;
            }
            if($this->relationLoaded('location')){
                unset($data['location_id']);
                $data['location'] = $this->location;
            }
            if($this->relationLoaded('country')){
                unset($data['country_id']);
                $data['country'] = $this->country;
            }
            if($this->relationLoaded('values')){
                $data['values'] = $this->values;
            }
            if($this->relationLoaded('contents')){
                $data['contents'] = Content::collection($this->contents);
            }
            if(isset($this->roles)){
                $data['roles'] = $this->roles;
            }
            if($this->access_token){
                $data['access_token'] = $this->access_token;
            }
        }
        return $data;
    }
    private function minFieldsArr(){
        return [
            'id',
            'name',
            'avatar',
            'content_type_id',
            'qr_code',
            'profile_id',
            'isOffice',
            'socket_id'
        ];
    }
}

<?php

namespace App\Http\Resources;

use App\Notifications\UpdateCreated;
use App\Notifications\UpdateUpdated;
use Illuminate\Http\Resources\Json\JsonResource;

class Notification extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $type = null;
        switch ($this->type){
            case UpdateCreated::class;
                $type = "update_created";
                break;
            case UpdateUpdated::class;
                $type = "update_updated";
                break;
        }
        $data = $this->data;
        $data['id'] = $this->id;
        $data['type'] = $type;
        $data['read_at'] = $this->read_at;
        $data['created_at'] = $this->created_at->timestamp;
        return $data;
    }
}

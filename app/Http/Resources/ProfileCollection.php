<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ProfileCollection extends ResourceCollection
{
    protected $min;
    public function __construct($resource, $min)
    {
        $this->min = $min;
        parent::__construct($resource);
    }

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // set the minimal field $min via Profile->min()
        $this->collection->each->min($this->min);
        return parent::toArray($request);
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Package extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'created_at' => $this->created_at->timestamp,
        ];
        if($this->relationLoaded('from')){
            $data['from'] = new Profile($this->from,null,true);
        }
        if($this->relationLoaded('to')){
            $data['to'] = new Profile($this->to,null,true);
        }
        if($this->relationLoaded('receiver')){
            $data['receiver'] = new Profile($this->receiver,null,true);
        }
        if($this->relationLoaded('confirmation')){
            $data['confirmation'] = new Confirmation($this->confirmation);
        }
        return $data;
    }
}

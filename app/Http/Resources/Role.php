<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Role extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'display_name' => $this->display_name,
            'default' => $this->default === 1? true: false,
        ];
        if($this->relationLoaded('permissions')){
            $data['permissions'] = Permission::collection($this->permissions);
        }
        return $data;
    }
}

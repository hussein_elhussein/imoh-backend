<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Reminder extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'status' => $this->status,
            'actions_allowed' => $this->actions_allowed? true: false,
        ];
        if($this->relationLoaded('contentUpdate')){
            $data['content_update'] = new ContentUpdate($this->contentUpdate);
        }
        if($this->relationLoaded('profiles')){
            $data['profiles'] = [];
            foreach ($this->profiles as $profile) {
                $data['profiles'][] = ['id' => $profile->id];
            }
        }
        return $data;
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ContentTypeCollection extends ResourceCollection
{
    protected $pag;
    public function __construct($resource, $pag = false)
    {
        $this->pag = $pag;
        parent::__construct($resource);
    }

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if($this->pag){
            return [
                'data' => $this->removeDublicate(),
            ];
        }
        return $this->removeDublicate();
    }
    private function removeDublicate(){
        return $data = $this->collection->filter(function($item){
            return !$item->parent_id;
        })->toArray();
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Conversation extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'socket_id' => $this->socket_id,
            'created_at' => $this->created_at->timestamp,
        ];
        if($this->relationLoaded('opener')){
            $data['opener'] = new Profile($this->opener,null,true);
        }
        if($this->relationLoaded('receiversMin')){
            $data['receivers'] = [];
            foreach ($this->receiversMin as $receiver){
                if($this->relationLoaded('opener')){
                    if($receiver->id !== $this->opener->id){
                        $data['receivers'][] = new Profile($receiver,null,true);
                    }
                }else{
                    $data['receivers'][] = new Profile($receiver,null,true);
                }
            }
        }
        if($this->relationLoaded('viewersMin')){
            $data['viewers'] = [];
            foreach ($this->viewersMin as $viewer){
                $data['viewers'][] = new Profile($viewer,null,true);
            }
        }
        if($this->relationLoaded('messages')){
            $data['messages'] = Message::collection($this->messages);
        }
        $data['last_message'] = new Message($this->lastMessage()->first());
        return $data;
    }
}

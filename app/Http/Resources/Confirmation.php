<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Confirmation extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       $data = [
           'id' => $this->id,
           'content_id' => $this->content_id,
           'content_update_id' => $this->content_update_id,
           'code' => $this->code,
           'created_at' => $this->created_at->timestamp,
       ];
       if($this->isLoaded('contentUpdate')){
           $data['content_update'] = new ContentUpdate($this->contentUpdate);
       }
       if($this->isLoaded('content')){
            $data['content'] = new Content($this->content);
       }
       if($this->isLoaded('profiles')){
           $data['profiles'] = new ProfileCollection($this->profiles,true);
       }
       return $data;
    }

    private function isLoaded($relation){
        if($this->relationLoaded($relation)){
            return true;
        }else{
            $attributes = $this->getAttributes();
            return isset($attributes[$relation]);
        }
    }
}

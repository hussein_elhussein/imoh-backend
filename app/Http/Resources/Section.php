<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Section extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
{
        if($this->fields->count()){
            return [
                'id' => $this->id,
                'title' => $this->title,
                'order' => $this->order,
                'fields' => new FieldCollection($this->fields),
            ];
        }
        return null;
    }
}

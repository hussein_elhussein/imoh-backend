<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Message extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'text' => $this->content,
            'created_at' => $this->created_at->timestamp,
            'sender' => $this->sender_id,
        ];
        if($this->attachment_path){
            $data['attachment'] = [
                'path' => $this->attachment_path,
            ];
        }
        if($this->relationLoaded('author')){
            $data['author'] = new Profile($this->author,null,true);
        }
        return $data;
    }
}

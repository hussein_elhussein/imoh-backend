<?php

namespace App\Http\Resources;

use App\Http\Resources\traits\CategoryHelper;
use Illuminate\Http\Resources\Json\JsonResource;
class ContentType extends JsonResource
{
    use CategoryHelper;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data  = [
            'id' => $this->id,
            'type' => $this->type,
            'order' => $this->order,
            'menu' => $this->menu ? true: false,
            'tab' => $this->tab? true: false,
            'strict_mode' => $this->strict_mode? true: false,
            'moderated' => $this->moderated? true: false,
            'profile_tab' => $this->profile_tab? true: false,
            'show_map' => $this->show_map? true: false,
            'has_updates' => $this->has_updates? true: false,
        ];
        if($this->role_id){
            $data['role_id'] = $this->role_id;
        }
        if($this->membership_type_id){
            $data['membership_type_id'] = $this->membership_type_id;
        }
        if($this->icon && strlen($this->icon)){
            $data['icon'] = $this->icon;
        }
        if($this->profile_limit){
            $data['profile_limit'] = $this->profile_limit;
        }
        if($this->sections->count()){
            $data['sections'] = new SectionCollection($this->sections);
        }
        if($this->parent_id){
            $data['parent_id'] = $this->parent_id;
        }
        if($this->relationLoaded('children')){
            if($this->children->count()){
                $data['children'] = ContentType::collection($this->children);
            }
        }
        if($this->relationLoaded('categories')){
            if($this->categories->count()){
                $data['categories'] = $this->categories;
            }
        }
        if($this->relationLoaded('translations')){
            if($this->categories->count()){
                $data['translations'] = $this->translations;
            }
        }

        return $data;
    }
}

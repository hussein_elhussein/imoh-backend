<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CategoryCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->removeDublicate();
    }

    private function removeDublicate(){
        return $data = $this->collection->filter(function($item){
            return !$item->parent_id;
        })->toArray();
    }
}

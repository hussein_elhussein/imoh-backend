<?php

namespace App\Http\Resources;

use App\Models\Traits\ProfileHelper;
use Illuminate\Http\Resources\Json\JsonResource;

class ContentUpdate extends JsonResource
{
    use ProfileHelper;
    public $loaded_author;
    public function __construct($resource, $count = null, $loaded_author = null)
    {
        $this->loaded_author = $loaded_author;
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'content_id' => $this->content_id,
            'parent_id' => $this->parent_id,
            'previous_id' => $this->previous_id,
            'status' => $this->status,
            'actions_allowed' => $this->actions_allowed === 1,
            'primary' => $this->primary === 1,
            'schedule' => $this->schedule,
            "created_at"=> $this->created_at->timestamp,
            "updated_at"=> $this->updated_at->timestamp,
        ];
        if(isset($this->children_count)){
            $data['children_count'] = $this->children_count;
        }
        if($this->relationLoaded('author')){
            $data['author'] = new Profile($this->author,null,true);
        }elseif($this->loaded_author){
            $data['author'] = new Profile($this->loaded_author,null,true);
        }
        if($this->relationLoaded('content')){
            $data['content'] = new Content($this->content);
        }
        if($this->relationLoaded('children')){
            $data['children'] = ContentUpdate::collection($this->children);
        }
        if($this->relationLoaded('parent')){
            $data['parent'] = new ContentUpdate($this->parent);
        }
        if($this->relationLoaded('office')){
            $data['office'] = new Content($this->office);
        }
        if($this->office_id){
            $data['office_id'] = $this->office_id;
        }
        if($this->previous_id){
            $data['previous_id'] = $this->previous_id;
        }
        if($this->details && strlen($this->details)){
            $data['details'] = $this->details;
        }
        if($this->lat && $this->lng){
            $data['lat'] = $this->lat;
            $data['lng'] = $this->lng;
        }
        return $data;
    }
}

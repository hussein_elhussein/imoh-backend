<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [];
        if($this->relationLoaded('profiles')){
            $profile = new Profile($this->profiles->first());
            $data = $profile->toArray($request);
        }
        if($this->relationLoaded('roles')){
            if($this->roles->count()){
                $data['roles'] = $this->roles;
            }
        }
        if($this->relationLoaded('primaryProfile')){
            $profile = new Profile($this->primaryProfile->first());
            $data = $profile->toArray($request);
        }
        if($this->relationLoaded('role')){
            $data['roles'][] = [
                'display_name' => $this->role->display_name
            ];
        }else{
            $data['role_id'] = $this->role_id;
        }
        return $data;
    }
}

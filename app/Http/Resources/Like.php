<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Like extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'like' => $this->like? true: false,
            'created_at' => $this->created_at->timestamp,
        ];
        if($this->relationLoaded('author')){
            $data['author'] = new Profile($this->author,null,true);
        }
        return $data;
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Field extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'type' => $this->type,
            'name' => $this->name,
            'label' => $this->label,
            'required' => $this->required? true: false,
            'default_body' => $this->default_body? true: false,
            'default_title' => $this->default_body? true: false,
            'order' => $this->order,
            'browse' => $this->browse? true: false,
            'read' => $this->read? true: false,
            'edit' => $this->edit? true: false,
            'add' => $this->add? true: false,
            'delete' => $this->delete? true: false,
        ];
        if($this->options && strlen($this->options)){
            $data['options'] = $this->options;
        }
        if($this->notes && strlen($this->notes)){
            $data['notes'] = $this->notes;
        }
        if($this->relationLoaded('translations')){
            if($this->categories->count()){
                $data['translations'] = $this->translations;
            }
        }
        return $data;
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Rating extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'rating' => $this->rating,
            'comment' => $this->comment,
        ];
        if($this->relationLoaded('author')){
            $data['author'] = new Profile($this->author,null,true);
        }
        return $data;
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Comment extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'content' => $this->content,
            'created_at' => $this->created_at,
        ];
        if($this->relationLoaded('author')){
            $data['author'] = new Profile($this->author,null,true);
        }
        if($this->relationLoaded('likes')){
            $data['likes'] = Like::collection($this->likes);
        }
        if($this->relationLoaded('dislikes')){
            $data['dislikes'] = Like::collection($this->dislikes);
        }
        return $data;
    }
}

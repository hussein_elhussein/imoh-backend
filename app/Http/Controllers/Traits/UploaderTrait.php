<?php
namespace App\Traits;
use Illuminate\Support\Facades\Log;
use \Illuminate\Support\Facades\Storage;
trait UploaderTrait
{
    public function uploadFile($file_field, $visibility = "public", $request = null){
        $res = null;
        if($file_field == 'image'){
          $res  = Storage::putFile('images', $request->file($file_field));
        }else{
          $fileObj = $request->file($file_field);
          $dir = storage_path('app/files');
          if($visibility === 'public'){
              $dir = storage_path('app/public/files');
          }
          $fileName = $this->generateName(30) .".". $fileObj->getClientOriginalExtension();
          $fileObj->move($dir, $fileName);
          $res = "files/" . $fileName;
        }
        return $res;
    }
    public function removeFile($name){
        $exists = Storage::disk('local')->exists($name);
        if($exists){
            Storage::delete($name);
            return true;
        }else{
            return false;
        }
    }
  function generateName($length = 6) {
    $str = "";
    $characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
    $max = count($characters) - 1;
    for ($i = 0; $i < $length; $i++) {
      $rand = mt_rand(0, $max);
      $str .= $characters[$rand];
    }
    return $str;
  }
}
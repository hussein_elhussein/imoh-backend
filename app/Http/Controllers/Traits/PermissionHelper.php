<?php
namespace App\Traits;
use App\User;
use Illuminate\Support\Facades\DB;

trait PermissionHelper {
  public function CheckPermission($requiredPer)
  {
    $user = \Auth::user();
    $permissions = $this->GetPermissions($user->email);
    if(isset($permissions[$requiredPer])){
      return true;
    }else{
      return false;
    }
  }
  public function GetPermissions($email)
  {
    $permissions = [];
    if($email){
      $foundedUser = DB::table('users')->where('email', $email)->first();
      $userObj = User::find($foundedUser->id);
      foreach ($userObj->role->permissions->all() as $permission){
        $permissions[$permission->key] = true;
      }
    }
    return $permissions;
  }
}
<?php
namespace App\Traits;
use App\Field;
use App\Section;
use App\ContentType;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

trait ContentTypeHelper {


    /** The the default title for services, it's null if you didn't call getDefaultFields before
     * @var Model
     */
    public $default_title = null;

    /** The the default body for services, it's null if you didn't call getDefaultFields before
     * @var Model
     */
    public $default_body = null;
    /**
     * Gets the default fields associated with the service type
     *
     * @return Collection
     */
    public function getDefaultFields(){
        $fields = Field::where('default_title',1)->orWhere('default_body',1)->get();
        foreach ($fields as $field){
            if($field->default_title){
                $this->default_title = $field;
            }
            if($field->default_body){
                $this->default_body = $field;
            }
        }
        return $fields;
    }

    /**
     * check whether a specific field is a default field
     *
     * @param $id: The field id
     * @return boolean
     */
    public function isDefaultField($id){
        $fields = $this->getDefaultFields()->pluck('id')->toArray();
       return in_array($id,$fields);
    }

    /**
     * Gets the default section for service types;
     *
     * @return Model
     */
    public function getDefaultSection(){
        return Section::where('default',1)->first();
    }

    /**
     * Gets the default service type;
     *
     * @return Model
     */
    public function getDefaultServiceType(){
        return ContentType::where('default',1)->first();
    }

    /**
     * Gets true value from 'on' or numeric field based on "Voyager Row' type
     *
     * @param Model $dataType: the voyager data type model
     * @return string
     */
    public function getTrueValue($dataType){
        $field_type = 'checkbox';
        $true_val = '1';
        foreach ($dataType->editRows as $row){
            if($row->field === 'browse'){
                $field_type = $row->type;
            }
        }
        switch ($field_type){
            case 'checkbox':
                $true_val = 'on';
                break;
            case 'number':
            case 'text':
                $true_val = '1';
                break;
            default:
                $true_val = '1';
        }
        return $true_val;
    }
}
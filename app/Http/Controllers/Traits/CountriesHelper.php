<?php
namespace App\Traits;
use App\Country;

trait CountriesHelper {
  public function getCallingCodes(){
      $countries = Country::all()->sortBy('name');
      $calling_codes = [];
      foreach ($countries as $country) {
          if(isset($country->calling_code)){
              $calling_code = $country->name . ' (+' . $country->calling_code . ')';
              $calling_codes[$country->id] = $calling_code;
          }
      }
      return $calling_codes;
  }
}
<?php
namespace App\Traits;
use Illuminate\Http\Request;

trait PostHelper {
  public function isStaffPost($post){
    if($post->staff_post != null || $post->staff_post != 0){
      return true;
    }else{
      return false;
    }
  }
  public function isCreatingStaffPost(Request $request){
    if(isset($request->staff_post)){
      if($request->staff_post != null || $request->staff_post != 0){
        return true;
      }else{
        return false;
      }
    }else{
      return false;
    }
  }

}
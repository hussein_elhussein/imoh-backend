<?php

namespace App\Http\Controllers;
use App\Notifications\TestPushNotification;
use App\User;
use VSignal\PushNotification;

class TestController extends Controller
{
    public function test(){
        $user = User::with('profiles')->find(1);
        foreach ($user->profiles as $profile) {
            $profile->timestamps = false;
            if($profile->content_type_id === 1){
                $profile->primary = 1;
            }else{
                $profile->primary = 0;
            }
            $profile->update();
        }
        dd($user->profiles()->get()->toArray());
    }
}

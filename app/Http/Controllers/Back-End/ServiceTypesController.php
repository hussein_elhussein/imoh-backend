<?php

namespace App\Http\Controllers;

use App\Category;
use App\Categoryable;
use App\ContentType;
use App\Traits\ContentTypeHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Events\BreadImagesDeleted;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class ServiceTypesController extends VoyagerBaseController
{
    use ContentTypeHelper;
    public function index(Request $request)
    {
        if($request->ajax()){
            $types = ContentType::with('sections.fields')->get();
            foreach ($types as $service_type){
                $service_type->categories = $service_type->sortedCategories;
            }
            return response()->json($types);
        }
        return parent::index($request);
    }
    public function create(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        $dataTypeContent = (strlen($dataType->model_name) != 0)
            ? new $dataType->model_name()
            : false;

        foreach ($dataType->addRows as $key => $row) {
            $details = json_decode($row->details);
            $dataType->addRows[$key]['col_width'] = isset($details->width) ? $details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'add');

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = 'voyager::bread.edit-add';

        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }

        $categories = Category::all();
        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable','categories'));
    }
    public function edit(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $relationships = $this->getRelationships($dataType);

        $dataTypeContent = (strlen($dataType->model_name) != 0)
            ? app($dataType->model_name)->with($relationships)->findOrFail($id)
            : DB::table($dataType->name)->where('id', $id)->first(); // If Model doest exist, get data from table name

        foreach ($dataType->editRows as $key => $row) {
            $details = json_decode($row->details);
            $dataType->editRows[$key]['col_width'] = isset($details->width) ? $details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');

        // Check permission
        $this->authorize('edit', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = 'voyager::bread.edit-add';

        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }
        $categories = Category::all();
        $sorted_cats = $dataTypeContent->sortedCategories;
        $selectedCats = [];
        foreach ($sorted_cats as $cat){
            $selectedCats[$cat['id']] = $cat;

        }
        return Voyager::view($view, compact('dataType', 'dataTypeContent','selectedCats', 'isModelTranslatable','categories'));
    }
    public function update(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Compatibility with Model binding.
        $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;

        $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

        // Check permission
        $this->authorize('edit', $data);

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $slug, $id);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        if (!$request->ajax()) {

            // Set the default values for the service type:
            $default_section = (string)$this->getDefaultSection()->id;
            $sections = $request->service_type_belongstomany_section_relationship;
            $updated_fields = [];
            if(!$sections){
                $sections = [];
            }

            // Check if default section is removed and add it
            if(array_search($default_section,$sections) === FALSE){
                array_push($sections,$default_section);
                $updated_fields['service_type_belongstomany_section_relationship'] = $sections;

            }

            // Update the request parameters
            if(!empty($updated_fields)){
                $request->merge($updated_fields);
            }

            $this->insertUpdateData($request, $slug, $dataType->editRows, $data);

            // Update categories;
            $data->attachCats($request, true);

            event(new BreadDataUpdated($dataType, $data));
            return redirect()
                ->route("voyager.{$dataType->slug}.index")
                ->with([
                    'message'    => __('voyager::generic.successfully_updated')." {$dataType->display_name_singular}",
                    'alert-type' => 'success',
                ]);
        }
    }
    public function store(Request $request)
    {
        //todo: force the type to have at least one section with with required fields title & body
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        if (!$request->has('_validate')) {

            // Set the default values for the service type:
            $default_section = (string)$this->getDefaultSection()->id;
            $sections = $request->service_type_belongstomany_section_relationship;
            $updated_fields = [];
            if(!$sections){
                $sections = [];
            }

            // Check if default section is removed and add it
            if(array_search($default_section,$sections) === FALSE){
                array_push($sections,$default_section);
                $updated_fields['service_type_belongstomany_section_relationship'] = $sections;

            }

            // Update the request parameters
            if(!empty($updated_fields)){
                $request->merge($updated_fields);
            }
            $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());

            //attach categories
            $data->attachCats($request);

            event(new BreadDataAdded($dataType, $data));
            if ($request->ajax()) {
                return response()->json(['success' => true, 'data' => $data]);
            }

            return redirect()
                ->route("voyager.{$dataType->slug}.index")
                ->with([
                    'message'    => __('voyager::generic.successfully_added_new')." Service type",
                    'alert-type' => 'success',
                ]);
        }
    }
    public function destroy(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('delete', app($dataType->model_name));

        // Init array of IDs
        $ids = [];
        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }
        foreach ($ids as $id) {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
            $this->cleanup($dataType, $data);
        }

        $displayName = count($ids) > 1 ? $dataType->display_name_plural : $dataType->display_name_singular;
        // keeps the default fields from being deleted:
        $default = $this->getDefaultServiceType();
        $index = array_search($default->id,$ids);

        if($index !== FALSE){
            unset($ids[$index]);
        }

        if(!empty($ids)){
            $data->categories()->detach();
        }
        $res = $data->destroy($ids);
        $data = $res
            ? [
                'message'    => __('voyager::generic.successfully_deleted')." {$displayName}",
                'alert-type' => 'success',
            ]
            : [
                'message'    => __('voyager::generic.error_deleting')." {$displayName}",
                'alert-type' => 'error',
            ];

        if ($res) {
            event(new BreadDataDeleted($dataType, $data));
        }

        return redirect()->route("voyager.{$dataType->slug}.index")->with($data);
    }
    public function getChildCats(Request $request){
        $request->validate([
            'service_type' => 'required',
            'parents' => 'required|array',
        ]);
        $service_type = ContentType::findOrFail($request->service_type);
        $children = [];
        $categories = $service_type->sortedCategories;
        foreach ($categories as $category){
            $children = array_merge($category['children'],$children);
        }
        return response()->json($children);
    }

}

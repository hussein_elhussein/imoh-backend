<?php

namespace App\Http\Controllers;

use App\DataRow;
use App\DataType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Events\BreadImagesDeleted;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use TCG\Voyager\Http\Controllers\VoyagerBreadController;

class TranslationController extends VoyagerBreadController
{
//    public function index(Request $request) {
//      $dt_rows = DataRow::all();
//      $tables = [];
//      $tables_rows = [];
//      foreach ($dt_rows as $dt_row) {
//        $dt_type = [
//        'name' => $dt_row->data_type->display_name_plural,
//        'columns' => []
//        ];
//        $tables[$dt_row->data_type->id] = $dt_type;
//        $tables_row = [
//          'table_id' => $dt_row->data_type->id,
//          'id' => $dt_row->id,
//          'name' => $dt_row->display_name,
//        ];
//        array_push($tables_rows, $tables_row);
//      }
//      foreach ($tables_rows as $tables_row) {
//        $tables[$tables_row['table_id']]['columns'][$tables_row['id']] = $tables_row['name'];
//      }
//      //dd($tables);
//
//      // GET THE SLUG, ex. 'posts', 'pages', etc.
//      $slug = $this->getSlug($request);
//
//      // GET THE DataType based on the slug
//      $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
//
//      // Check permission
//      $this->authorize('browse', app($dataType->model_name));
//
//      $getter = $dataType->server_side ? 'paginate' : 'get';
//
//      $search = (object) ['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];
//      $searchable = $dataType->server_side ? array_keys(SchemaManager::describeTable(app($dataType->model_name)->getTable())->toArray()) : '';
//      $orderBy = $request->get('order_by');
//      $sortOrder = $request->get('sort_order', null);
//
//      // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
//      if (strlen($dataType->model_name) != 0) {
//        $model = app($dataType->model_name);
//        $query = $model::select('*');
//
//        $relationships = $this->getRelationships($dataType);
//
//        // If a column has a relationship associated with it, we do not want to show that field
//        $this->removeRelationshipField($dataType, 'browse');
//
//        if ($search->value && $search->key && $search->filter) {
//          $search_filter = ($search->filter == 'equals') ? '=' : 'LIKE';
//          $search_value = ($search->filter == 'equals') ? $search->value : '%'.$search->value.'%';
//          $query->where($search->key, $search_filter, $search_value);
//        }
//
//        if ($orderBy && in_array($orderBy, $dataType->fields())) {
//          $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'DESC';
//          $dataTypeContent = call_user_func([
//            $query->with($relationships)->orderBy($orderBy, $querySortOrder),
//            $getter,
//          ]);
//        } elseif ($model->timestamps) {
//          $dataTypeContent = call_user_func([$query->latest($model::CREATED_AT), $getter]);
//        } else {
//          $dataTypeContent = call_user_func([$query->with($relationships)->orderBy($model->getKeyName(), 'DESC'), $getter]);
//        }
//
//        // Replace relationships' keys for labels and create READ links if a slug is provided.
//        $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
//      } else {
//        // If Model doesn't exist, get data from table name
//        $dataTypeContent = call_user_func([DB::table($dataType->name), $getter]);
//        $model = false;
//      }
//
//      // Check if BREAD is Translatable
//      if (($isModelTranslatable = is_bread_translatable($model))) {
//        $dataTypeContent->load('translations');
//      }
//
//      // Check if server side pagination is enabled
//      $isServerSide = isset($dataType->server_side) && $dataType->server_side;
//
//      $view = 'voyager::bread.browse';
//
//      if (view()->exists("voyager::$slug.browse")) {
//        $view = "voyager::$slug.browse";
//      }
//      return Voyager::view($view, compact(
//        'dataType',
//        'dataTypeContent',
//        'isModelTranslatable',
//        'search',
//        'orderBy',
//        'sortOrder',
//        'searchable',
//        'isServerSide'
//      ));
//    }
}

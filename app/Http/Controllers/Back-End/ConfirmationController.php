<?php

namespace App\Http\Controllers;

use App\Confirmation;
use App\Http\Resources\Confirmation as ConfirmationResource;
use App\Http\Resources\Notification;
use App\Http\Resources\NotificationCollection;
use App\Interfaces\IResponse;
use function foo\func;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ConfirmationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index()
    {
        $profile = Auth::user()->profile;
        $conf = $profile->confirmations()->with(
            'profiles',
            'content.author',
            'contentUpdate.author')->paginate(10);
        return ConfirmationResource::collection($conf);
    }
    public function filter(Request $request){
        $request->validate([
            'profiles' => 'required|array|min:1',
            'profiles.*' => 'integer',
            'content_update_id' => 'integer',
            'content_id' => 'integer'
        ]);
        $filters = [];
        if($request->content_id){
            $filter[] = ['content_id',$request->content_id];
        }
        if($request->content_update_id){
            $filter[] = ['content_update_id',$request->content_update_id];
        }
        $query = Confirmation::whereHas('profiles',function($q)use($request){
            $q->whereIn('profiles.id',$request->profiles);
        });
        if(count($filter)){
            $query->where($filters);
        }
        $confirmations = $query->get();
        return response()->json($confirmations);
    }
}

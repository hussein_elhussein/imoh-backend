<?php

namespace App\Http\Controllers;

use App\ContentUpdate;
use App\Events\ReminderCanceled;
use App\Http\Resources\Reminder as ReminderResource;
use App\Interfaces\IResponse;
use App\Reminder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReminderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index()
    {
        $profile = Auth::user()->profile;
        $reminders = $profile->reminders()->with(
            'contentUpdate.author',
            'contentUpdate.office.author',
            'contentUpdate.content.values',
            'contentUpdate.content.author',
            'profiles:profiles.id')
            ->orderBy('reminders.created_at','DESC')
            ->paginate(10);
        return ReminderResource::collection($reminders);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'status' => 'required|integer'
        ]);
        $reminder = Reminder::findOrFail($id);
        $reminder->status = $request->status;
        if($request->status === 2){
            $reminder->actions_allowed = 0;
        }
        $reminder->update();
        if($request->status === 2){
            event(new ReminderCanceled($reminder));
        }
        $res = new IResponse();
        $res->id = $reminder->id;
        $res->details = 'Reminder updated successfully';
        return response()->json($res);
    }
}

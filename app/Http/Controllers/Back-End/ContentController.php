<?php

namespace App\Http\Controllers;

use App\Content;
use App\ContentType;
use App\Helpers\Permissions;
use App\Http\Resources\ContentCollection;
use App\Interfaces\IResponse;
use App\Models\Traits\CollectionPaginate;
use App\Models\Traits\LocationHelper;
use App\Models\Traits\ValueHelper;
use App\Services\ProfileService;
use App\Value;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;
use \App\Http\Resources\Content as ContentResource;
class ContentController extends VoyagerBaseController
{
    use ValueHelper;
    use LocationHelper;
    use CollectionPaginate;
    private $profileService;
    public function __construct(ProfileService $profileService)
    {
        $this->profileService = $profileService;
    }

    public function index(Request $request)
    {
        if($request->ajax()){
           $content = Content::orderBy('created_at','desc')->paginate(10);
           return response()->json($content);
        }
        return parent::index($request);
    }

    public function filterContent(Request $request)
    {
        $stop = null;
        $av_relations = [
            'author',
            'contentType',
            'updates',
            'values',
            'location',
            'categories',
        ];
        $request->validate([
            'contentTypes' => 'required|array',
            'contentTypes.*' => 'integer',
            'categories' => 'array',
            'categories.*' => 'integer',
            'relations' => 'array|min:1',
            'relations.*' => [Rule::in($av_relations)],
            'profiles' => 'array',
            'profiles.*' => 'integer',
            'excludeLoggedIn' => 'boolean',
            'minimal' => 'boolean',
            'pagination' => 'boolean',
            'location' => 'array',
            'location.id' => 'integer',
            'location.country' => 'array',
            'location.country.name' => 'string',
            'location.sub_administrative_area' => 'string',
            'location.administrative_area' => 'string'
        ]);
        $types_ids = $this->getAuthorized('browse',$request->contentTypes);
        if(!count($types_ids)){
            $res = new IResponse(true,false,"You can't browse this content");
            return response()->json($res,403);
        }
        $excludeLoggedIn = $request->excludeLoggedIn?  true: false;
        $location = [];
        $profiles = null;
        $categories = null;
        // Apply location filters:
        if($request->location){
            if(isset($request->location['country'])){
                if(isset($request->location['country']['name'])){
                    $location['country'] = [];
                    $location['country']['name'] = $request->location['country']['name'];
                }
                if(isset($request->location['country']['id'])){
                    $location['country'] = [];
                    $location['country']['id'] = $request->location['country']['id'];
                }
            }
            if(isset($request->location['sub_administrative_area'])){
                $location['area'] = $request->location['sub_administrative_area'];
            }
            if(isset($request->location['administrative_area'])){
                $location['area']= $request->location['administrative_area'];
            }
            if(isset($request->location['id'])){
                $location['id'] = $request->location['id'];
            }

        }

        // Apply profiles filter:
        if($request->profiles && count($request->profiles)){
            $profiles = $request->profiles;
        }
        // Apply profiles filter:
        if($request->categories && count($request->categories)){
            $categories = $request->categories;
        }
        $relations = [
            'location.country',
            'categories',
            'author',
            'values',
        ];

        //Apply minimal filter
        //todo: add updates/responder to content
        if($request->minimal){
            $relations = ['values'];
        }else{
            $relations = array_merge($relations,['values.fieldMin','author.country','updates']);

        }
        if($request->relations){
            $relations = $request->relations;
        }
        //Get content:
        $contents = $this->getTypeContents($types_ids,$relations,$location,$excludeLoggedIn,$profiles,$categories);
        if($request->pagination){
            if(get_class($contents) === Collection::class){
                return new ContentCollection($this->paginate($contents,10));
            }
            return new ContentCollection($contents->paginate(10));
        }else{
            return new ContentCollection($contents);
        }
    }
    public function show(Request $request, $id)
    {
        if(!$request->ajax()){
            return parent::show($request,$id);
        }
        $content = Content::with(
            'location.country',
            'categories',
            'author',
            'values',
            'primaryUpdates.author')->findOrFail($id);
        return new ContentResource($content);
    }
    public function store(Request $request)
    {
        if(!$request->ajax()){
            return parent::store($request); // TODO: Change the autogenerated stub

        }
        $author = Auth::user();
        $result = new IResponse(false,true,'Content saved',null);
        // validate request:
        $office_type_id = (int) setting('content-types.office_type_id',6);
        $isOffice = false;
        if($request->has('ids')){
            $ids = $request->get('ids');
            $types = ContentType::with('sections.fields')->findOrFail($ids);

            //default content type:
            $content_type = $types->filter(function ($type) use ($office_type_id){
                return $type->id === $office_type_id;
            })->first();

            //related content type:
            $profile_type_id = (int) setting('content-types.profile_type_id',1);
            $profile_type = $types->filter(function ($type) use ($profile_type_id){
                return $type->id === $profile_type_id;
            })->first();
            if($request->has($content_type->type)){
                if($office_type_id === $request->get($content_type->type)['content_type_id']){
                    $isOffice = true;
                    $this->validateContent($request,false,$content_type,$profile_type);
                    //validate that current user can attach a member to his profile:
                    $profile = $author->profile;
                    $org_type_id = (int) setting('content-types.organization_type_id',2);
                    if($profile->content_type_id !== $org_type_id){
                        $result->error = true;
                        $result->success = false;
                        $result->details = "You can't add members to your profile";
                        return response()->json($result,403);
                    }
                }
            }
        }else{
            $this->validateContent($request);
        }
        if(!isset($content_type)){
            $content_type = ContentType::with('sections.fields')
                ->findOrFail($request->content_type_id);
        }
        $req_content = null;
        $req_fields = null;
        if($isOffice){
            $req_content = $request->toArray()[$content_type->type];
        }else{
            $req_content = $request->toArray();
        }

        $req_fields = $req_content['values'];
        if(!$this->validateValues($content_type,$req_fields)){
            $result->error = true;
        }
        if(isset($profile_type)){
            $fields = $request->get($profile_type->type)['values'];
            if(!$this->validateValues($profile_type,$fields)){
                $result->error = true;
            }
        }
        if($result->error){
            $result->success = false;
            $result->details = "Required field is not valid";
            return response()->json($result);
        }

        //save the content:
        $req_content['profile_id'] = $author->profile->id;
        $content = new Content();
        $content->fill($req_content);
        $content->content_type_id = $content_type->id;
        $content->status = isset($req_content['status'])? $req_content['status']: 1;
        if(isset($req_content['location'])){
            $content = $this->attachLocation($content,$req_content);
            Log::info('location id:', [$content->location_id]);
        }
        $content->loc_lat = $req_content['lat'];
        $content->loc_lng = $req_content['lng'];
        $content->save();
        $cats_ids = [];
        foreach ($req_content['categories'] as $category){
            $cats_ids[] = $category['id'];
        }
        $content->categories()->attach($cats_ids);
        // Attach fields values to content:
        $this->attachValues($content,$req_fields);
        $result->details = 'Content saved';
        $result->id = $content->id;
        return response()->json($result);
    }
    public function update(Request $request, $id)
    {
        if(!$request->ajax()){
            return parent::update($request,$id); // TODO: Change the autogenerated stub

        }
        $content = Content::findOrFail($id);
        $this->validateContent($request, true);
        $content_type = ContentType::with('sections.fields')
            ->findOrFail($request->content_type_id);
        $req_fields = $request->toArray()['values'];
        $result = new IResponse(false,true,'Service saved',null);
        if(!$this->validateValues($content_type,$req_fields)){
            $result->success = false;
            $result->error = true;
            $result->details = 'a required field is not valid';
        }
        $author = Auth::user();
        //save the service:
        $values = $request->toArray();
        $values['profile_id'] = $author->profile->id;
        $content->fill($values);
        $content->content_type_id = $content_type->id;
        $content->status = $request->status? $request->status: 1;
        if($request->location){
            $content = $this->attachLocation($content,$request);
        }
        $content->loc_lat = $request->lat;
        $content->loc_lng = $request->lng;
        $content->update();
        if($request->categories){
            $cats_ids = [];
            foreach ($request->categories as $category){
                $cats_ids[] = $category['id'];
            }
            $content->categories()->sync($cats_ids);
        }
        // Attach fields values to content:
        $this->attachValues($content,$req_fields, true);
        // Attach location
        $result->details = 'content saved';
        $result->id = $content->id;
        return response()->json($result);
    }

    public function destroy(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('delete', app($dataType->model_name));

        // Init array of IDs
        $ids = [];
        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }
        // Delete content value relation:
        $vals_ids = [];
        foreach ($ids as $id) {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
            $values = $data->values()->select('values.id')->get()->pluck('id')->toArray();
            $vals_ids = array_merge($vals_ids,$values);
            $this->cleanup($dataType, $data);
        }
        if(count($vals_ids)){
            Value::destroy($vals_ids);
        }

        $displayName = count($ids) > 1 ? $dataType->display_name_plural : $dataType->display_name_singular;

        $res = $data->destroy($ids);
        $data = $res
            ? [
                'message'    => __('voyager::generic.successfully_deleted')." {$displayName}",
                'alert-type' => 'success',
            ]
            : [
                'message'    => __('voyager::generic.error_deleting')." {$displayName}",
                'alert-type' => 'error',
            ];

        if ($res) {
            event(new BreadDataDeleted($dataType, $data));
        }

        return redirect()->route("voyager.{$dataType->slug}.index")->with($data);
    }
    /**
     * Gets the content of a type or it's children.
     * @param array|int $ids: the content type id or name.
     * @param array $relations: relationships to get with the contents.
     * @param Mixed $location: within specific location id or name.
     * @param bool excludeLoggedIn: whether to exclude the current user's contents.
     * @param array $profiles: filter by profiles.
     * @param array $categories: filter by categories.
     * @return Mixed: Collection or hasMany relationship.
     */
    private function getTypeContents($ids,
                                     array $relations = null,
                                     $location = null,
                                     $excludeLoggedIn = false,
                                     $profiles = null,
                                     $categories = null){
        $int_id = is_int($ids);
        $types = ContentType::findOrFail($ids);
        $filters = [
            'types' => $types,
            'int_it' => $int_id,
            'relations' => $relations,
            'location' => $location,
            'excludeLoggedIn' => $excludeLoggedIn,
            'profiles' => $profiles,
            'categories' => $categories,
        ];
        $loadData = function ($_q = null)use ($filters){
            $q = $_q;
            if(!$_q){
                $q = $filters['types']->contents();
            }
            if($filters['relations']){
                $q = $q->with($filters['relations']);
            }
            // Filter by location:
            if($filters['location']){
                $location = isset($filters['location'])? $filters['location']: null;
                $country = isset($location['country'])? $location['country']: null;
                $cID = $country && isset($country['id'])? $country['id']: null;
                $cName = $country && isset($country['name'])? $country['name']: null;
                $locID = $location && isset($location['id'])? $location['id']: null;
                $area = $location && isset($location['area'])? $location['area']: null;
                if($country && !$locID){
                    if($cID){
                        $q = $q->whereHas('location.country', function($query) use($cID){
                            $query->where('id',$cID);
                        });
                    }else{
                        $q = $q->whereHas('location.country', function($query) use($cName){
                            $query->where('name',$cName);
                        });
                    }
                }
                if($locID){
                    $q = $q->whereHas('location', function($query) use($locID){
                        $query->where('id',$locID);
                    });
                }elseif($area){
                    $q = $q->whereHas('location', function($query) use($area){
                        $query->where('sub_administrative_area',$area)
                            ->orWhere('administrative_area',$area);
                    });
                }
            }

            //Filter by profiles:
            $profiles = isset($filters['profiles'])? $filters['profiles']: null;
            $excludeUser = $filters['excludeLoggedIn'] === true;
            if($profiles){
                $q = $q->whereIn('profile_id', $profiles);
            }elseif($excludeUser){
                $id = Auth::user()->profile->id;
                $q = $q->whereNotIn('profile_id', [$id]);
            }

            // filter by categories:
            $cats = $filters['categories'];
            if($cats){
                $q = $q->whereHas('categories', function($query) use($cats){
                    $query->whereIn('categories.id',$cats);
                });
            }
            return $q->orderBy('created_at','desc')->get();
        };

        if(get_class($types) === Collection::class){
            $items = new Collection();
            foreach ($types as $type) {
                $q = $type->contents();
                $items = $items->merge($loadData($q));
            }
            $sorted = $items->sortByDesc('created_at');
            return $sorted;
        }
        return $loadData();
    }

    private function validateContent($request, $update = false, $officeCT = null,$profileCT = null){
        $prefix = "";
        if($officeCT){
            $prefix = $officeCT->type . '.';
        }
        $rules = [
            $prefix . 'ids'                 => 'array|min:1',
            $prefix . 'ids.*'               => 'integer',
            $prefix . 'content_type_id'     => 'required|integer',
            $prefix . 'values'              => 'required|array|min:1',
            $prefix . 'values.*.field_id'   => 'required|integer',
            $prefix . 'values.*.value'      => 'required',
            $prefix . 'title'               => 'required|string',
            $prefix . 'body'                => 'string',
            $prefix . 'excerpt'             => 'string',
            $prefix . 'quantity'            => 'numeric|min:1',
            $prefix . 'status'              => 'integer',
            $prefix . 'featured'            => 'boolean',
            $prefix . 'lat'                 => 'required|numeric',
            $prefix . 'lng'                 => 'required|numeric',
        ];
        if($update){
            $rules = $rules + [
                $prefix . 'categories'      => 'array|min:1',
                $prefix . 'categories.*.id' => 'required_with:categories|integer',
            ];
        }else{
            $rules = $rules + [
                $prefix . 'categories'      => 'required|array|min:1',
                $prefix . 'categories.*.id' => 'required|integer',
            ];
        }
        if($officeCT){
            $rules = $rules + [
                $officeCT->type => 'required|array',
            ];
            $this->profileService->validateFields($request,$update,true, $profileCT);
        }
        $request->validate($rules);
    }
    private function getAuthorized($ability,$types){
      // @todo: this is returning false positive:
        if(is_array($types)){
            $types_ids = $types;
            $authorized = [];
            foreach ($types_ids as $types_id) {
                if(Permissions::can($ability,$types_id)){
                    $authorized[] = $types_id;
                }
            }
            return $authorized;
        }else{
            return Permissions::can($ability,$types);
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Confirmation;
use App\Http\Resources\Confirmation as ConfirmationResource;
use App\Http\Resources\Notification;
use App\Http\Resources\NotificationCollection;
use App\Http\Resources\Package;
use App\Interfaces\IResponse;
use function foo\func;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index()
    {
        $profile = Auth::user()->profile;
        $packages = $profile->packages()->with(
            'from',
            'to',
            'receiver',
            'confirmation.contentUpdate.content.values',
            'confirmation.contentUpdate.office'
        )->paginate(10);
        return Package::collection($packages);
    }
}

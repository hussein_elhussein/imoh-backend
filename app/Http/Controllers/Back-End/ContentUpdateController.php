<?php

namespace App\Http\Controllers;

use App\Content;
use App\ContentUpdate;
use App\Interfaces\IResponse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use \App\Http\Resources\ContentUpdate as ContentUpdateResource;
use Illuminate\Support\Facades\Auth;

class ContentUpdateController extends Controller
{
    public function show($id){
        $update = ContentUpdate::with('author','content.values','office')->findOrFail($id);
        return new ContentUpdateResource($update);
    }
    public function showMany(Request $request){
        $request->validate([
           'ids' => 'required|array|min:1'
        ]);
        $updates = ContentUpdate::with(
            'author',
            'content.author',
            'content.values',
            'content.categories',
            'parent.author',
            'content.location.country',
            'office')->withCount('children')->findMany($request->ids);
        return ContentUpdateResource::Collection($updates);
    }

    /** insert new update records for a content
     * @param Request $request
     * @param $id: the content id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $id){

        // just to verify that content does exist in db:
        Content::with('profile')->findOrFail($id);

        // validate request
        $request->validate([
            'office_id' => 'integer|exists:contents,id',
            'parent_id' => 'integer|exists:content_updates,id',
            'previous_id' => 'integer|exists:content_updates,id',
            'details' => 'string',
            'status' => 'required|integer',
            'lat' => 'numeric',
            'lng' => 'numeric',
        ]);
        $res = new IResponse();
        // validate schedule:
        $author = Auth::user()->profile;
        $timezone = null;
        if($author->timezone){
            $timezone = $author->timezone;
        }else{
            $timezone = env('TIMEZONE');
        }
        $date_obj = Carbon::createFromTimestamp($request->schedule, $timezone);
        if($date_obj){
            $status_valid = $request->status === 4 ||
                $request->status === 14  ||
                $request->status === 10;
            if($date_obj->isPast() && !$status_valid){
                $res->error = true;
                $res->success = false;
                $res->details = "Date cannot be in the past";
                return response()->json($res,422);
            }
        }else{
            $res->error = true;
            $res->success = false;
            $res->details = "Date is not valid";
            return response()->json($res,422);
        }
        //save:
        $qr_code = generateUniqueID('content_updates','qr_code',50);
        $update = new ContentUpdate();
        $update->fill($request->toArray());
        $update->content_id = $id;
        $update->qr_code = $qr_code;
        $update->parent_id = $request->parent_id;
        $update->previous_id = $request->previous_id;
        $update->profile_id = $author->id;
        if($request->has('actions_allowed')){
            $update->actions_allowed = $request->actions_allowed === true? 1: 0;
        }
        if($request->has('primary')){
            $update->primary = $request->primary === true? 1: 0;
        }
        $update->save();
        $res->details = 'Update successfully saved!';
        $res->id = $update->id;
        return response()->json($res);
    }
}

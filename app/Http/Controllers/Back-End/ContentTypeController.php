<?php

namespace App\Http\Controllers;

use App\Category;
use App\ContentType;
use App\Field;
use App\Helpers\Permissions;
use App\Interfaces\IResponse;
use App\Section;
use App\Traits\ContentTypeHelper;
use App\Translation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;
use App\Http\Resources\ContentType as ContentTypeResource;
class ContentTypeController extends VoyagerBaseController
{
    use ContentTypeHelper;
    public function index(Request $request)
    {
        if($request->ajax()){
            $can = Voyager::can('browse_content_types');
            if(!$can){
                return response()->json("You can't browse content types",403);
            }
            $types = ContentType::with(
                'sections.fields',
                'translations',
                'categories.parentMin'
            )->orderBy('order')->get();
            return ContentTypeResource::collection($types);
        }
        return parent::index($request);
    }
    public function indexMin(Request $request){
        $can = Voyager::can('browse_content_types');
        if(!$can){
            if($request->ajax()){
                return response()->json("You can't read or browse Content types");
            }
            return redirect()->back(403);
        }
        $types = ContentType::select(['id','type'])->get();
        if($request->ajax()){
            return response()->json($types);
        }
        return response($types);
    }
    public function show(Request $request, $id){
        if($request->ajax()){
            $can = Voyager::can('read_content_types');
            if(!$can){
                return response()->json("You can't read Content types");
            }
            $type = ContentType::with(
                'translations',
                'sections.fields.translations',
                'categories'
            )->findOrFail($id);
            return response()->json($type);
        }
        return parent::show($request,$id);
    }
    public function create(Request $request)
    {
        $can = Voyager::can('add_content_types');
        if(!$can){
            return redirect()->back(403);
        }
        $slug = $this->getSlug($request);
        $view = 'voyager::bread.edit-add';
        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }
        return Voyager::view($view);

    }
    public function edit(Request $request, $id)
    {
        $can = Voyager::can('edit_content_types');
        if(!$can){
            return redirect()->back(403);
        }
        $slug = $this->getSlug($request);
        $view = 'voyager::bread.edit-add';
        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }
        return Voyager::view($view,compact('id'));

    }
    public function update(Request $request, $id)
    {

        // Validate:
        if(!$request->ajax()){
            return parent::update($request,$id);
        }
        $can = Voyager::can('edit_content_types');
        if(!$can){
            return response()->json("You can't edit Content types",403);
        }

        $this->validateReq($request, true);

        $type = ContentType::with('sections.fields','categories')->findOrFail($id);
        if($type->type !== $request->type){
            $exists = ContentType::whereType($request->type)->first();
            if($exists){
                $err = [
                    "errors" => [
                        "Type" => "the Type name already exists",
                    ]
                ];
                return response()->json($err);
            }
        }
        // delete previous permissions only if the type field changed:
        if($request->type !== $type->type){
            Permissions::delete($type);
        }
        // Update the content type
        $type->fill($request->toArray());
        if($request->parent){
            $type->parent_id = $request->parent;
        }
        if($request->membership_type_id){
            $type->membership_type_id = $request->membership_type_id;
        }
        if($request->role_id){
            $type->role_id = $request->role_id;
        }
        if($request->generate_permissions){
            Permissions::delete($type);
            Permissions::generate($type);
        }
        $type->update();

        // Update translations:
        if(isset($request->translations)){
            foreach ($request->translations as $item) {
                $new_tr = false;
                if(isset($item['id'])){
                    $translation = Translation::findOrFail($item['id']);
                }else{
                    $new_tr = true;
                    $translation = new Translation();
                }
                $translation->fill($item);
                if(isset($item['field'])){
                  $translation->column_name = $item['field'];
                } elseif(isset($item['column_name'])){
                  $translation->column_name = $item['column_name'];
                }
                if($new_tr){
                    $translation->table_name = "content_types";
                    $translation->foreign_key = $type->id;
                    $translation->save();
                }else{
                    $translation->update();
                }
            }
        }

        // Update and attach sections:
        if(isset($request->sections)){
            foreach ($request->sections as $section){
                $is_new = true;
                if(isset($section['id'])){
                    $is_new = false;
                    $new_section = Section::findOrFail($section['id']);
                }else{
                    $new_section = new Section();
                }
                $new_section->title = $section['title'];
                $new_section->order = $section['order'];
                if($is_new){
                    $new_section->save();
                    $type->sections()->attach($new_section);
                }else{
                    $new_section->update();
                }


                // Create and attach fields:
                foreach ($section['fields'] as $field){
                    $is_new = true;
                    if(isset($field['id'])){
                        $is_new = false;
                        $new_field = Field::findOrFail($field['id']);
                    }else{
                        $new_field = new Field();
                    }
                    $new_field->fill($field);
                    if($is_new){
                        $new_field->save();
                        $new_section->fields()->attach($new_field);
                    }else{
                        $new_field->update();
                    }
                    // Add/Update translations:
                    if(isset($field['translations'])){
                        foreach ($field['translations'] as $item) {
                            $new_f_tr = false;
                            if(isset($item['id'])){
                                $fieldTranslation = Translation::findOrFail($item['id']);
                            }else{
                                $new_f_tr = true;
                                $fieldTranslation = new Translation();
                            }
                            $fieldTranslation->fill($item);
                            if(isset($item['translatable'])){
                              $fieldTranslation->column_name = $item['translatable'];
                            }elseif(isset($item['column_name'])){
                              $fieldTranslation->column_name = $item['column_name'];
                            }
                            if($new_f_tr){
                                $fieldTranslation->table_name = "fields";
                                $fieldTranslation->foreign_key = $new_field->id;
                                $fieldTranslation->save();
                            }else{
                                $fieldTranslation->update();
                            }
                        }
                    }
                }
            }
        }

        // Attach categories
        $type->categories()->detach();
        $cats = $request->categories;
        $sub_cats = $request->sub_categories;
        $target = null;
        if($sub_cats && count($sub_cats)){
            $target = $sub_cats;
        }elseif($cats && count($cats)){
            $target = $cats;
        }
        if($target){
            $categories = Category::findMany($target);
            $type->categories()->attach($categories);
        }
        $this->cleanUpRelations($request, $type);
        $res = new IResponse();
        $res->details = "Content type updated successfully";
        $res->id = $type->id;
        //$updated = ContentType::with('sections.fields')->find($id);
        return response()->json($res);
    }
    public function store(Request $request)
    {
        // Validate:
        $can = Voyager::can('add_content_types');
        if(!$can){
            if($request->ajax()){
                return response()->json("You can't add Content types",403);
            }
            return redirect()->back(403);
        }
        $this->validateReq($request);

        // Create the content type
        $type = new ContentType();
        $type->fill($request->toArray());
        if($request->parent){
            $type->parent_id = $request->parent;
        }
        if($request->membership_type_id){
            $type->membership_type_id = $request->membership_type_id;
        }
        if($request->role_id){
            $type->role_id = $request->role_id;
        }

        $type->save();
        if($request->generate_permissions){
            Permissions::generate($type);
        }

        // Create and attach translations:
        if(isset($request->translations)){
            foreach ($request->translations as $item) {
                $translation = new Translation();
                $translation->fill($item);
                $translation->table_name = "content_types";
                $translation->column_name = $item['field'];
                $translation->foreign_key = $type->id;
                $translation->save();
            }
        }
        // Create and attach sections:
        if(isset($request->sections)){
            foreach ($request->sections as $section){
                $new_section = new Section();
                $new_section->title = $section['title'];
                $new_section->order = $section['order'];
                $new_section->save();
                $type->sections()->attach($new_section);

                // Create and attach fields:
                foreach ($section['fields'] as $field){
                    $new_field = new Field();
                    $new_field->fill($field);
                    $new_field->save();
                    $new_section->fields()->attach($new_field);
                    // Create and attach field translations:
                    if(isset($field['translations'])){
                        foreach ($field['translations'] as $item) {
                            $fieldTranslation = new Translation();
                            $fieldTranslation->fill($item);
                            $fieldTranslation->table_name = "fields";
                            $fieldTranslation->column_name = $item['translatable'];
                            $fieldTranslation->foreign_key = $new_field->id;
                            $fieldTranslation->save();
                        }
                    }
                }
            }
        }

        // Attach categories
        $cats = $request->categories;
        $sub_cats = $request->sub_categories;
        $target = null;
        if($sub_cats && count($sub_cats)){
            $target = $sub_cats;
        }elseif($cats && count($cats)){
            $target = $cats;
        }
        if($target){
            $categories = Category::findMany($target);
            $type->categories()->attach($categories);
        }
        $res = new IResponse();
        $res->details = "Content type created successfully";
        $res->id = $type->id;
        return response()->json($res);

    }
    public function destroy(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('delete', app($dataType->model_name));

        // Init array of IDs
        $ids = [];
        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }
        foreach ($ids as $id) {
            $data = ContentType::with(['sections.fields'])->findOrFail($id);
            foreach ($data->sections as $section){
                $section->fields()->detach();
                $fields_ids = [];
                foreach ($section->fields as $field){
                    $fields_ids[] = $field->id;
                }
                if(count($fields_ids)){
                    Field::destroy($fields_ids);
                }
            }
            $data->sections()->detach();
            if($data->sections->count()){
                $sec_ids = [];
                foreach ($data->sections as $section){
                    $sec_ids[] = $section->id;
                }
                if(count($sec_ids)){
                    Section::destroy($sec_ids);
                }
            }
            $data->categories()->detach();
            Permissions::delete($data);
        }

        $displayName = count($ids) > 1 ? $dataType->display_name_plural : $dataType->display_name_singular;
        $res = $data->destroy($ids);
        $data = $res
            ? [
                'message'    => __('voyager::generic.successfully_deleted')." {$displayName}",
                'alert-type' => 'success',
            ]
            : [
                'message'    => __('voyager::generic.error_deleting')." {$displayName}",
                'alert-type' => 'error',
            ];

        if ($res) {
            event(new BreadDataDeleted($dataType, $data));
        }

        return redirect()->route("voyager.{$dataType->slug}.index")->with($data);
    }
    public function getChildCats(Request $request){
        $request->validate([
            'service_type' => 'required',
            'parents' => 'required|array',
        ]);
        $service_type = ContentType::findOrFail($request->service_type);
        $children = [];
        $categories = $service_type->sortedCategories;
        foreach ($categories as $category){
            $children = array_merge($category['children'],$children);
        }
        return response()->json($children);
    }

    /**
     * Cleans up content type relations after update
     * @param Request $request
     * @param Model $content_type
     */
    private function cleanUpRelations(Request $request, $content_type){

        // cleanup up sections
        $req_sec_ids = [];
        if(isset($request->sections)){
            foreach ($request->sections as $section) {
                if(isset($section['id'])){
                    $req_sec_ids[] = (int)$section['id'];
                }
            }
        }
        if(count($req_sec_ids) || !isset($request->sections)){
            $unwanted_sections = $content_type->sections->filter(function ($item) use ($req_sec_ids){
                $exists = in_array($item->id,$req_sec_ids);
                return  !$exists;
            });
            if(count($unwanted_sections)){
                $fields_ids = [];
                $section_ids = [];
                foreach ($unwanted_sections as $section) {
                    $ids = $section->fields->pluck('id')->toArray();
                    $section->fields()->detach($ids);
                    foreach ($ids as $id) {
                        $fields_ids[] = $id;
                    }
                    $section_ids[] = $section->id;
                }
                if(count($fields_ids)){
                    Field::destroy($fields_ids);
                }
                if(count($section_ids)){
                    Section::destroy($section_ids);
                }
            }

        }

        // clean up fields
        if(isset($request->sections)){
            $fields_ids = [];
            foreach ($content_type->sections as $section){
                foreach ($section->fields as $field) {
                    $fields_ids[] = $field->id;
                }
            }
            if(count($fields_ids)){
                $req_fields = [];
                foreach ($request->sections as $section) {
                    foreach ($section['fields'] as $field) {
                        if(isset($field['id'])){
                            $req_fields[] = (int)$field['id'];
                        }
                    }
                }
                $unwanted_fields = [];
                foreach ($fields_ids as $field_id) {
                    if(!in_array($field_id,$req_fields)){
                        $unwanted_fields[] = $field_id;
                    }
                }
                if(count($unwanted_fields)){
                    Field::destroy($unwanted_fields);
                }
            }
        }
    }

    private function validateReq(Request $request, $edit = false){
        $fields_types = [
            'checkbox',
            'color',
            'date',
            'start_date',
            'end_date',
            'file',
            'image',
            'number',
            'password',
            'radio_btn',
            'rich_text_box',
            'code_editor',
            'markdown_editor',
            'select_dropdown',
            'text',
            'text_area',
            'timestamp',
            'hidden',
            'coordinates',
        ];
        $rules = [
            'type' => 'required|string|unique:content_types,type',
            'parent' => 'integer|exists:content_types,id',
            'order' => 'integer',
            'menu' => 'boolean',
            'tab' => 'boolean',
            'icon' => 'string',
            'strict_mode' => 'boolean',
            'show_map' => 'boolean',
            'has_updates' => 'boolean',
            'profile_limit' => 'integer',
            'moderated' => 'boolean',
            'profile_tab' => 'boolean',
            'membership_type_id' => "integer|exists:membership_types,id",
            'role_id' => "integer|exists:roles,id",
            'categories' => 'array|min:1',
            'sub_categories' => 'array|min:1',
            'translations' => 'array|min:1',
            'sections' => 'array|min:1',
            'sections.*.fields' => 'required|array|min:1',
            'sections.*.fields.*.name' => 'required',
            'sections.*.fields.*.label' => 'required',
            'sections.*.fields.*.type' => ['required',Rule::in($fields_types)],
            'sections.*.fields.*.translations' => 'array|min:1',
            'sections.*.fields.*.default_body' => 'required|boolean',
            'sections.*.fields.*.default_title' => 'required|boolean',
            'sections.*.fields.*.order' => 'integer',
            'sections.*.fields.*.required' => 'required|boolean',
            'sections.*.fields.*.browse' => 'required|boolean',
            'sections.*.fields.*.read' => 'required|boolean',
            'sections.*.fields.*.edit' => 'required|boolean',
            'sections.*.fields.*.add' => 'required|boolean',
            'sections.*.fields.*.delete' => 'required|boolean',
            'sections.*.fields.*.options' => 'json',
            'sections.*.title' => "string",
            'sections.*.order' => "required|integer",
        ];

        if($edit){
            $rules['type'] = 'required|string';
        }
        $request->validate($rules);
    }
}

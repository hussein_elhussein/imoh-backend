<?php

namespace App\Http\Controllers;

use App\ContentType;
use App\Country;
use App\Events\SocketConnected;
use App\Interfaces\IResponse;
use App\Location;
use App\Membership;
use App\MembershipType;
use App\Models\Traits\CollectionPaginate;
use App\Models\Traits\LocationHelper;
use App\Models\Traits\ValueHelper;
use App\Profile;
use App\Role;
use App\Services\ProfileService;
use App\User;
use App\Http\Resources\Profile as ProfileResource;
use App\Http\Resources\ProfileCollection as ProfileResourceCollection;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class ProfileController extends VoyagerBaseController
{
    private $service;
    public function __construct(ProfileService $service)
    {
        $this->service = $service;
    }

    use ValueHelper;
    use LocationHelper;
    use CollectionPaginate;
    use \App\Traits\PermissionHelper;
    public function index(Request $request)
    {
        if($request->ajax()){
            $profile_type = setting('content-types.profile_type_id',1);
            $profiles = Profile::where('content_type_id', $profile_type);
            return new ProfileResourceCollection($profiles->paginate(10),true);
        }
        return parent::index($request);
    }
    public function show(Request $request, $id)
    {
        if($request->ajax()){
            $profile = Profile::with(
                'contents',
                'ratings',
                'values',
                'organizationProfile',
                'members.primaryProfile.ratings',
                'members.primaryProfile.values')->findOrFail($id);
            return new ProfileResource($profile);
        }
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $relationships = $this->getRelationships($dataType);
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);
            $dataTypeContent = call_user_func([$model->with($relationships), 'findOrFail'], $id);
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }
        // Replace relationships' keys for labels and create READ links if a slug is provided.
        $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType, true);

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'read');

        // Check permission
        $this->authorize('read', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = 'voyager::bread.read';
        $dataTypeContent->load('values.field');
        if (view()->exists("voyager::$slug.read")) {
            $view = "voyager::$slug.read";
        }
        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
    }
    public function me(){
        $user = Auth::user()->load('roles');
        $profile = $user->profile->load('ratings','values');
        $profile->setAttribute('roles',$user->roles);
        return new ProfileResource($profile);
    }
    public function register(Request $request,$cType = null){
        $this->service->validateFields($request,false,true);
        $profile = $this->service->register($request->toArray(),$cType);
        if($cType){
            return $profile;
        }
        $res = new IResponse(false,true,"User registered",$profile->id);
        return response()->json($res);
    }
    public function changeType(Request $request){
        //validate
        $this->service->validateFields($request);
        $content_type = ContentType::with('sections.fields')
            ->findOrFail($request->content_type_id);
        $req_fields = $request->toArray()['values'];
        $result = new IResponse(false,true,'Profile saved',null);
        if(!$this->validateValues($content_type,$req_fields)){
            $result->success = false;
            $result->error = true;
            $result->details = 'a required field is not valid';
            return response()->json($result,422);
        }
        $profile_type_id = (int) setting('content-types.profile_type_id',1);
        $author = Auth::user();
        $currentProfile = $author->profile;
        if($currentProfile->content_type_id !== $profile_type_id){
            $result->success = false;
            $result->error = true;
            $result->details = "Currently you can't change your profile type";
            return response()->json($result,422);
        }
        $req_values = $request->toArray();
        $values = $req_values['values'];
        $profile = $this->service->saveProfile($req_values,$values,$content_type,false);
        $profile->timestamps = false;
        //attach user:
        if($content_type->moderated && !$author->owner){
            $profile->status = 2;
        }else{
            $profile = $this->attachMembership($profile,$content_type);
            $this->disableProfiles($author->id);
            $profile->status = 1;
            $profile->primary = true;
            $profile->users()->attach($author->id);
        }
        $profile->update();
        $result->id = $profile->id;
        return response()->json($result);
    }
    public function update(Request $request, $id)
    {
        if(!$request->ajax()){
            return parent::update($request,$id); // TODO: Change the autogenerated stub

        }
        $profile = Profile::findOrFail($id);
        $content_type = ContentType::with('sections.fields')
            ->findOrFail($request->content_type_id);
        $req_fields = $request->toArray()['values'];
        $result = new IResponse(false,true,'Profile updated',null);
        if(!$this->validateValues($content_type,$req_fields)){
            $result->success = false;
            $result->error = true;
            $result->details = 'a required field is not valid';
        }
        $author = Auth::user();
        //save the service:
        $values = $request->toArray();
        $values['profile_id'] = $author->profile->id;
        $profile->fill($values);
        $profile->content_type_id = $content_type->id;
        $profile->loc_lat = $request->lat;
        $profile->loc_lng = $request->lng;
        $profile->update();
        $cats_ids = [];
        if($request->categories){
            foreach ($request->categories as $category){
                $cats_ids[] = $category['id'];
            }
            $profile->categories()->sync($cats_ids);
        }
        // Attach fields values to profile:
        $this->attachValues($profile,$req_fields, true);
        // Attach location
        $result->id = $profile->id;
        return response()->json($result);
    }
    public function approve($id){
        $profile = Profile::with('users')->findOrFail($id);
        if($profile->users){
            if($profile->users->count() === 1){
                $user = $profile->users->first();
                $this->disableProfiles($user->id);
                $profile = $this->attachMembership($profile);
                $profile->timestamps = false;
                $profile->status = 1;
                $profile->primary = 1;
                $profile->update();
                return redirect()
                    ->route("voyager.profiles.index")
                    ->with([
                        'message'    => "successfully approved the profile",
                        'alert-type' => 'success',
                    ]);
            }else{
                return redirect()
                    ->route("voyager.profiles.index")
                    ->with([
                        'message'    => "this profile has multiple attached users",
                        'alert-type' => 'error',
                    ]);
            }
        }else{
            return redirect()
                ->route("voyager.profiles.index")
                ->with([
                    'message'    => "this profile doesn't have any attached user",
                    'alert-type' => 'error',
                ]);
        }
    }
    public function updateLocation(Request $request)
    {
        $request->validate([
            'coords' => 'required|array',
            'coords.latitude' => 'required',
            'coords.longitude' => 'required',
            'country' => 'required|array',
            'country.name' => 'required|string',
            'sub_administrative_area' => 'required|string',
            'administrative_area' => 'required|string'
        ]);
        $res = new IResponse();
        $profile = \Auth::user()->profile;
        $updated_str = 'location updated successfully';
        $new_location_str = 'new location saved';
        // find the country:
        $country = Country::where('name',$request->country['name'])->select('id')->first();
        $location = Location::where('sub_administrative_area',$request->sub_administrative_area)->first();
        if($country){
            $profile->country_id = $country->id;
            // save new location
            if(!$location){
                $location = new Location();
                $location->country_id = $country->id;
                $location->administrative_area = $request->administrative_area;
                $location->sub_administrative_area = $request->sub_administrative_area;
                $location->postal_code = $request->postal_code;
                $location->save();
            }else{
                $new_location_str = 'location already exists';
            }
        }
        $profile->location_id = $location->id;
        $profile->loc_lat = $request->coords['latitude'];
        $profile->loc_lng = $request->coords['longitude'];
        $profile->timestamps = false;
        $profile->update();
        $res->details = $updated_str . ', ' . $new_location_str;
        return response()->json($res);
    }
    public function updateDevice(Request $request)
    {
        $request->validate([
            'token' => 'required|string',
            'ar' => 'required|bool',
            'timezone' => 'required|string'
        ]);
        $profile = Auth::user()->profile;
        $profile->device_token = $request->token;
        $profile->locale = $request->ar? 2: 1;
        $profile->timezone = $request->timezone;
        $profile->timestamps = false;
        $profile->update();
        return response()->json(true);
    }
    public function updateSocketID(Request $request)
    {
        $id = $request['socket_id'];
        $profile = Auth::user()->profile;
        $profile->socket_id = $id;
        $profile->online = 1;
        $profile->timestamps = false;
        $profile->update();
        event(new SocketConnected($profile));
        return response()->json(true);
    }
    public function disconnected(Request $request)
    {
        $id = $request['socket_id'];
        $profile = Auth::user()->profile;
        $timezone = $profile->timezone? $profile->timezone: config('timezone','UTC');
        $profile->socket_id = $id;
        $profile->online = 0;
        $profile->last_active = Carbon::now($timezone)->timestamp;
        $profile->timestamps = false;
        $profile->update();
        return response()->json(true);
    }
    public function getSocket($user_id)
    {
        $user = User::findOrFail($user_id);
        return $user->profile->socket_id;
    }
    public function updateLastSeen($id){
        $user = User::findOrFail($id);
        $profile = $user->profile;
        $timezone = $profile->timezone? $profile->timezone: config('timezone','UTC');
        $profile->last_active = Carbon::now($timezone)->timestamp;
        $profile->prtimestamps = false;
        $profile->update();
        return response()->json(['success' => 1, 'error' => 0]);
    }
    public function destroy(Request $request, $id)
    {
        $user = User::find($id);
        $user->delete();
        return response()->json(['success' => 1, 'error' => 0]);
    }
    public function getPublicRoles(){
        $roles = Role::where('public',1)->get();
        return response()->json($roles);
    }
    private function attachMembership($profile, $ct = null){
        $content_type = $ct;
        if(!$ct){
            $content_type = ContentType::findOrFail($profile->content_type_id);
        }
        // attach membership:
        $membershipType = null;
        if($content_type->membership_type_id){
            $membershipType = MembershipType::find($content_type->membership_type_id);
        }else{
            $membershipType = MembershipType::where('is_default',1)->firstOrFail();
        }
        $date = Carbon::now(env('timezone','UTC'));
        $dateB = Carbon::now(env('timezone','UTC'));
        $duration = $membershipType->validity_duration;
        $end_date = null;
        if($membershipType->validity_unit === 'y'){
            $end_date = $dateB->addYears($duration)->toDateTimeString();
        }else{
            $end_date = $dateB->addMonths($duration)->toDateTimeString();
        }
        $membership = new Membership();
        $membership->membership_type_id = $membershipType->id;
        $membership->active = true;
        $membership->start_date = $date->toDateTimeString();
        $membership->end_date = $end_date;
        $profile->membership_id = $membership->id;
        return $profile;
    }
    private function disableProfiles($user_id){
        $user = User::with(['profiles' => function($query){
            $query->where('primary','=',1);
        }])->findOrFail($user_id);
        if($user->profiles->count()){
            foreach ($user->profiles as $profile) {
                $profile->timestamps = false;
                $profile->primary = 0;
                $profile->status = 3;
                $profile->update();
            }
        }
    }
}

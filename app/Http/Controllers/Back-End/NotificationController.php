<?php

namespace App\Http\Controllers;

use App\Http\Resources\Notification;
use App\Http\Resources\NotificationCollection;
use App\Interfaces\IResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index()
    {
        $profile = Auth::user()->profile;
        return Notification::collection($profile->notifications()->paginate(10));
    }

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function unread()
    {
        $profile = Auth::user()->profile;
        return Notification::collection($profile->unreadNotifications()->paginate(10));
    }
    public function getSavedCode(Request $request, $update_id, $type){
        $profile = Auth::user()->profile;
        $note = $profile->notifications()->where([
            ['type','LIKE','%'. $type . '%'],
            ['data','LIKE','%"id":' . $update_id . '%']
        ])->firstOrFail();
        return new Notification($note);

    }
    public function count()
    {
        $profile = Auth::user()->profile;
        $count  = $profile->unreadNotifications->count();
        return response()->json($count);
    }

    /**
     * Marks all notifications as read.
     *
     * @return \Illuminate\Http\Response
     */
    public function readAll()
    {
        $profile = Auth::user()->profile;
        $profile->unreadNotifications->markAsRead();
        $res = new IResponse();
        $res->details = "All notification marked as read";
        return response()->json($res);
    }

    public function read(Request $request)
    {
        $request->validate([
            'ids' => 'required|array|min:1'
        ]);
        $ids = $request->ids;
        $profile = Auth::user()->profile;
        $unread = $profile->unreadNotifications->filter(function ($item)use($ids){
            return in_array($item->id,$ids);
        });
        foreach ($unread as $item){
            $item->markAsRead();
        }
        $res = new IResponse();
        $res->details = "All notification marked as read";
        return response()->json($res);
    }
}

<?php

namespace App\Http\Controllers;


use App\Role;
use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;
use App\Http\Resources\Role as RoleResource;
class RoleController extends VoyagerBaseController
{
    public function all(Request $request, $min = false)
    {
        $can = Voyager::can('browse_roles');
        if(!$can){
            return response()->json("You can't browse roles",403);
        }
        if($min){
            $roles = Role::all();
        }else{
            $roles = Role::with('permissions')->get();
        }
        return RoleResource::collection($roles);
    }
}
<?php

namespace App\Http\Controllers;

use App\Office;
use Illuminate\Http\Request;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Events\BreadImagesDeleted;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;

class AdminOrganizationController extends VoyagerBaseController
{
   public function destroy(Request $request, $id)
   {
       $slug = $this->getSlug($request);

       $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

       // Check permission
       $this->authorize('delete', app($dataType->model_name));

       // Init array of IDs
       $ids = [];
       if (empty($id)) {
           // Bulk delete, get IDs from POST
           $ids = explode(',', $request->ids);
       } else {
           // Single item delete, get ID from URL
           $ids[] = $id;
       }
       foreach ($ids as $id) {
           $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
           $this->cleanup($dataType, $data);
       }
       foreach ($ids as $org_id){
           Office::whereOrganizationId($org_id)->delete();
       }
       $displayName = count($ids) > 1 ? $dataType->display_name_plural : $dataType->display_name_singular;
       $res = $data->destroy($ids);
       $data = $res
           ? [
               'message'    => __('voyager::generic.successfully_deleted')." {$displayName}",
               'alert-type' => 'success',
           ]
           : [
               'message'    => __('voyager::generic.error_deleting')." {$displayName}",
               'alert-type' => 'error',
           ];

       if ($res) {
           event(new BreadDataDeleted($dataType, $data));
       }

       return redirect()->route("voyager.{$dataType->slug}.index")->with($data);
   }
}

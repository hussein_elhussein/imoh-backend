<?php

namespace App\Http\Controllers;

use App\Content;
use App\ContentUpdate;
use App\Profile;
use App\Rating;
use App\Action;
use App\Service;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use  App\Http\Resources\Rating as RatingResource;
class RatingController extends Controller
{
    public function index($rateable)
    {

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$rateable)
    {
        $request->validate([
            'rating' => 'required|numeric',
            'comment' => 'required|string',
            'rateable_id' => 'required|numeric',
        ]);
        $model = null;
        $profile = Auth::user()->profile;
        switch ($rateable){
            case "content":
                $model = Content::findOrFail($request->rateable_id);
                if($model->author_id === $profile->id){
                    return response()->json("You can't rate your service",422);
                }
                break;
            case "profile":
                $model = Profile::findOrFail($request->rateable_id);
                if($model->id === $profile->id){
                    return response()->json("You can't rate your profile",422);
                }
                break;
            case "content_update":
                $model = ContentUpdate::with('content.author')->findOrFail($request->rateable_id);
                if($model->author_id === $profile->id){
                    return response()->json("You can't rate your own response",422);
                }elseif($model->service->author->id !== $profile->id){
                    return response()->json("You can't rate the response",403);
                }
                break;
            default:
                return response()->json("rateable_type is not supported",422);
                break;
        }
        $rated = $model->ratings()->where('profile_id',$profile->id)->first();
        if($rated){
            return response()->json("You've already rated",422);
        }
        $rating = new Rating();
        $rating->rating = $request->rating;
        $rating->comment = $request->comment;
        $rating->profile_id = $profile->id;
        $model->ratings()->save($rating);
        return response()->json('ok');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return mixed
     */
    public function show(Request $request, $rateable, $id)
    {
        $id = (int) $id;
        if(!$id || $id < 1){
            return response()->json("invalid id",422);
        }
        $model = null;
        switch ($rateable){
            case "content":
                $model = 'App\Content';
                break;
            case "profile":
                $model = 'App\Profile';
                break;
            case "content_update":
                $model = 'App\ContentUpdate';
                break;
            default:
                return response()->json("Rateable model is not supported",422);
                break;
        }
        $rating = Rating::where(['rateable_id' => $id],['rateable_type' => $model])
                        ->with('author')
                        ->paginate(10);
        return RatingResource::collection($rating);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($rateable,$id)
    {
        //
    }

    public function update($rateable,$id)
    {

    }
}

<?php

namespace App\Http\Controllers;

use App\ContentType;
use App\Http\Resources\Profile;
use App\Interfaces\IResponse;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\Http\Controllers\AccessTokenController;
use Psr\Http\Message\ServerRequestInterface;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use League\OAuth2\Server\Exception\OAuthServerException;
class AuthController extends AccessTokenController
{
    public function issueToken(ServerRequestInterface $request)
    {
        try {
            //get username (default is :email)
            $username = $request->getParsedBody()['username'];
            //get user
            $user = User::where('email', '=', $username)->firstOrFail();

            //issuetoken
            $tokenResponse = parent::issueToken($request);
            //convert response to json string
            $content = $tokenResponse->getContent();

            //convert json to array
            $data = json_decode($content, true);
            if(isset($data["error"]))
                throw new OAuthServerException('The user credentials were incorrect.', 6, 'invalid_credentials', 401);
            $user->load('roles','role');
            $roles = $user->roles->toArray();
            array_push($roles,$user->role->toArray());
            $profile = $user->profile;

            //load relations:
            $relations = [
                'ratings',
                'values',
                'members.primaryProfile.ratings',
                'members.primaryProfile.values'
            ];
            $profile->load($relations);
            $profile->setAttribute('roles',$roles);
            $profile->setAttribute('access_token',$data['access_token']);
            $profileResource = new Profile($profile);
            return response()->json($profileResource);
        }
        catch (ModelNotFoundException $e) { // email notfound
            return response()->json('incorrect email or password',401);
        }
        catch (OAuthServerException $e) { //password not correct..token not granted
            return response()->json('incorrect email or password',401);
            //return error message
        }
        catch (Exception $e) {
            dd($e);
            return response()->json('something went wrong',500);
            ////return error message
        }
    }

    //todo: implement this function:
    public function register(Request $request){
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|unique:users',
            'password' => 'required|string',
            'location.coords' => 'required|array',
            'location.coords.latitude' => 'required',
            'location.coords.longitude' => 'required',
            'location.country' => 'required|array',
            'location.country.name' => 'required|string',
            'location.sub_administrative_area' => 'required|string',
            'location.administrative_area' => 'required|string'
        ]);
    }
    public function logout(){
      $res = [
        'success' => 0,
        'error' => 1,
      ];
      if (Auth::check()) {
          $user = Auth::user();
          $user->OauthAccessToken()->delete();
          $profile = $user->profile;
          $profile->timestamps = false;
          $profile->online = 0;
          $profile->last_active = date('Y-m-d H:i:s');
          $profile->update();
          $res['success'] = 1;
          $res['error'] = 0;
      }
      return response()->json($res);
    }
    public function emailExists($mail){
      $user = DB::table('users')->where('email', $mail)->first();
      if ($user){
        return true;
      }else{
        return false;
      }
    }
    public function usernameExists($username){
      $user = DB::table('users')->where('username', $username)->first();
      if ($user){
        return true;
      }else{
        return false;
      }
    }
    public function generateProfileID() {
      return mt_rand();
    }
    public function guard()
    {
      return Auth::guard();
    }
    public function checkAuth(Request $request){
      //just return success since it was called from route protected by middleware
      return response()->json(true);
    }

}

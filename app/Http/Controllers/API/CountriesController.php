<?php

namespace App\Http\Controllers;

use App\Country;
use App\Translation;
use function foo\func;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CountriesController extends Controller
{

    public function index()
    {
        $countries = Country::translation('name','ar')->get();
        return response()->json($countries);
    }
}

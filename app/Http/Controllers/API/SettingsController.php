<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function index(Request $request){
        $content_types = setting('content-types');
        $site = setting('site');
        $settings = array_merge($content_types,$site);
        foreach ($settings as $key => $setting){
            $intVal = (int) $setting;
            if($intVal){
                $settings[$key] = $intVal;
            }
        }
        return response()->json($settings);
    }
}

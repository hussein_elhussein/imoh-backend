<?php

namespace App\Http\Controllers;

use App\Conversation;
use App\Events\ConversationCreated;
use App\Events\MessageCreated;
use App\Message;
use App\Http\Resources\Message as MessageResource;
use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Conversation as ConversationResource;
use Illuminate\Support\Facades\Log;
use App\Interfaces\IResponse;

class ConversationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index($pag = null)
    {
        $profile = Auth::user()->profile;
        if($pag){
            $conversations = $profile->conversations()->paginate(10);
        }else{
            $conversations = $profile->conversations()->get();
        }
        return ConversationResource::collection($conversations);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
           'receivers' => 'required|array|min:1',
           'receivers.*.id' => 'required|integer',
        ]);
        $socket_id = null;
        $room = true;
        //get unique socket id for the conversation:
        while ($room){
          $socket_id = generateId(50);
          $room = Conversation::where('socket_id', $socket_id)->first();
        }
        $author = Auth::user()->profile;
        $new_conv = new Conversation();
        $new_conv->socket_id = $socket_id;
        $new_conv->opener_id = $author->id;
        $new_conv->save();
        $receivers_ids = [];
        foreach ($request->receivers as $receiver){
            $receivers_ids[] = $receiver['id'];
        }
        $receivers_ids[] = $author->id;
        $receivers = Profile::findOrFail($receivers_ids);
        $new_conv->receivers()->attach($receivers);
        $new_conv->viewers()->attach([$author->id]);
        event(new ConversationCreated($new_conv));
        return response()->json($new_conv);
    }

    public function entered($id){
        $conversation = Conversation::findOrFail($id);
        $profile = Auth::user()->profile;
        $conversation->viewers()->syncWithoutDetaching([$profile->id]);
        return response()->json(true);
        // todo: set all messages in a conversation as read
    }
    public function left($id){
        $conversation = Conversation::findOrFail($id);
        $profile = Auth::user()->profile;
        $conversation->viewers()->detach([$profile->id]);
        return response()->json(true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return mixed
     */
    public function show($id)
    {
        $profile = Auth::user()->profile;
        $conv = Conversation::with('messages','receiversMin','opener','viewersMin')->findOrFail($id);
        $canView = false;
        foreach ($conv->receiversMin as $receiver) {
            if($receiver->id === $profile->id){
                $canView = true;
            }
        }
        if(!$canView){
            $canView = $conv->opener->id === $profile->id;
        }
        if($canView){
            return MessageResource::collection($conv->messages);
        }
        return response()->json("You can't view this conversation", 403);
    }
    public function send(Request $request)
    {
        $request->validate(
            [
                'text' => 'string',
                'conversation_id' => 'required|exists:conversations,id',
                'conversation_socket_id' => 'required|string',
                'attachment' => 'array',
                'attachment.blob_ext' => 'required_with:attachment|string',
                'attachment.path' => 'required_with:attachment|string'
            ]
        );
        //save the message:
        $author = Auth::user()->profile;
        $message = new Message();
        $message->sender_id = $author->id;
        $message->conversation_id = $request->conversation_id;
        if($request->text && strlen($request->text)){
            $message->content = $request->text;
        }
        if(isset($request->attachment)){
            $message->attachment_path = $request->attachment['path'];
            if(isset($request->attachment['duration_humanized'])){
            	$message->duration_humanized = $request->attachment['duration_humanized'];
            }
        }
        $message->save();
        event(new MessageCreated($message,$request->conversation_socket_id,$author));
        return response()->json(['result' => 'done', 'id' => $message->id]);
    }

    /**
     * Sets a conversation messages as read.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function setRead(Request $request)
    {
        $request->validate([
            'ids' => 'required|array|min:1'
        ]);
        $profile = Auth::user()->profile;
        $messages = Message::findOrFail($request->ids);
        foreach ($messages as $message) {
            $message->readers()->attach($profile->id);
        }
        $res = new IResponse();
        $res->details = 'set ' . $messages->count() . ' messages as read';
        return response()->json($res);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $conv = Conversation::withCount(['receivers','deleters'])->findOrFail($id);
        $profile = Auth::user()->profile;
        $conv->deleters()->attach($profile->id);
        $deleters = $conv->deleters_count + 1;
        if($deleters === $conv->receivers_count){
        	Conversation::destroy([$conv->id]);
        }else{
            $conv->deleters()->attach($profile->id);
        }
        $res = new IResponse();
        $res->details = "Successfully deleted!";
        return response()->json($res);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroyMany(Request $request)
    {
        $request->validate([
           'ids' => 'required|array|min:1'
        ]);
        Conversation::destroy($request->ids);
        return response()->json(true);
    }
}

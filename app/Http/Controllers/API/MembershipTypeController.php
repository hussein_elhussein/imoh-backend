<?php

namespace App\Http\Controllers;

use App\MembershipType;
use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;

class MembershipTypeController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $can = Voyager::can('browse_content_types');
        if($request->ajax() && !$can){
            return response()->json("You can't browse content types",403);
        }else if(!$can){
            return redirect()->route('voyager.login');
        }
        $types = MembershipType::all();
        return response()->json($types);

    }
}

<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Http\Resources\Comment as CommentResource;
use App\Interfaces\IResponse;
use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{

    public function index()
    {
        $comments = Comment::all();
        return response()->json($comments);
    }
    public function indexProfile($id)
    {
        $profile = Profile::findOrFail($id);
        $relations = [
            'author',
            'likes',
            'dislikes'
        ];
        $comments = $profile->comments()->with($relations)->paginate(10);
        return CommentResource::collection($comments);
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {

        //validate:
        $user = Auth::user();
        $user = $user->profile;
        $request->validate([
            'comment' => 'required|string',
            'commentable_id' => 'required|numeric',
            'commentable_type' => 'required|string',
        ]);
        $res = new IResponse(false,true,"Comment saved");
        $target = null;
        switch ($request->commentable_type){
            case "comment":
                $target =  Comment::findOrFail($request->commentable_id);
                break;
            case "profile":
                $target =  Profile::findOrFail($request->commentable_id);;
                break;
            default:
                $res->error = true;
                $res->success = false;
                $res->details = 'unknown type';
                return response()->json($res,403);
                break;
        }
        $comment = new Comment();
        $comment->fill([
            'profile_id' => $user->id,
            'commentable_id' => $target->id,
            'commentable_type' => get_class($target),
            'content' => $request->comment
        ]);

        $comment->save();
        $res->id = $comment->id;
        return response()->json($res);

    }
    public function show($id)
    {
        //
    }
    public function destroy($id)
    {
        //
    }
}

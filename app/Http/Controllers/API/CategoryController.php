<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Resources\CategoryCollection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Facades\Voyager;

class CategoryController extends Controller
{
    public function index()
    {
        $can = Voyager::can('read_categories') && Voyager::can('browse_categories');
        if($can){
            $categories = Category::with('children')->get();
            return response()->json($categories);
        }
        return redirect()->route('voyager.login');

    }
    public function indexMin()
    {
        $can = Voyager::can('read_categories') && Voyager::can('browse_categories');
        if($can){
            $categories = Category::all();
            return response()->json($categories);
        }
        return redirect()->route('voyager.login');

    }

    public function store(Request $request)
    {
        //
    }
    public function show($id)
    {
        //
    }
    public function update(Request $request, $id)
    {
        //
    }
    public function destroy($id)
    {
        //
    }
    public function getChildren(Request $request, $id = null){
        if($request->has('parents')){
            $request->validate([
                'parents' => 'required|array'
            ]);
            // get multiple parents children:
            $categories = Category::with('children')->findMany($request->parents);
            $children = [];
            foreach ($categories as $category){
               $children = array_merge($children,$category->children->toArray());
            }
            return response()->json($children);
        }else{
            $category = Category::with('children')->findOrFail($id);
            return response()->json($category->children);
        }
    }
}

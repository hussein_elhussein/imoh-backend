<?php

namespace App\Http\Controllers;

use App\Image;
use Illuminate\Http\Request;


class ImageController extends Controller
{
    use \App\Traits\UploaderTrait;
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }
    public function index()
    {
        $images = Image::all();
        return response()->json($images);
    }
    public function store(Request $request)
    {
        //dd($request->all());
        $permitted = CheckPermission('can_create');
        if(!$permitted) {
            $errors = [
                'success' => 0,
                'errors' => 1,
                'details' => "You don't have permission to create!",
            ];
            return response()->json($errors);
        }
        $image = new Image;
        $image->title = $request->image['title'];
        $image->description = $request->image['description'];
        $image->url = $request->image['url'];
        $image->save();
        return response()->json([
            'success' => 1,
            'errors' => 0,
            'details' => "Image was created!",
        ]);
    }
    public function show($id)
    {
        $image = Image::find($id);
        return response()->json($image);
    }
    public function update(Request $request, $id)
    {
        $permitted = CheckPermission('can_edit');
        if(!$permitted) {
            $errors = [
                'success' => 0,
                'errors' => 1,
                'details' => "You don't have permission to edit!",
            ];
            return response()->json($errors);
        }
        $image = Image::find($id);
        $image->title = $request->image['title'];
        $image->description = $request->image['description'];

        //delete the old image file:
        if ($image && $image->url && $image->url !== '')
            $this->removeFile($image->url);

        $image->url = $request->image['url'];
        $image->save();
        return response()->json([
            'success' => 1,
            'errors' => 0,
            'details' => "Image was updated!",
        ]);
    }
    public function destroy($id)
    {
        $permitted = CheckPermission('can_delete');
        if(!$permitted) {
            return response()->json([
                'success' => 0,
                'errors' => 1,
                'details' => "You don't have permission to delete!",
            ]);
        }

        $image = Image::find($id);
        //delete the image file:
        if ($image && $image->url && $image->url !== '')
            $this->removeFile($image->url);
        $image->delete();
        return response()->json([
            'success' => 1,
            'errors' => 0,
            'details' => "Image was deleted!",
        ]);
    }
}

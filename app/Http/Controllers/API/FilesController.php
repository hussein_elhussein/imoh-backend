<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Http\File;

class FilesController extends Controller
{
    use \App\Traits\UploaderTrait;
    public function upload(Request $request){
        $target_field = null;
        if($request->hasFile('image')){
            $target_field = 'image';
        }elseif ($request->hasFile('file_field')){
            $target_field = 'file_field';
        }elseif ($request->hasFile('file')){
            $target_field = 'file';
        }
        $visibility = 'private';
        if($request->visibility && $request->visibility === 'public'){
            $visibility = "public";
        }
        switch ($target_field){
            case 'image':
            case 'file_field':
            $res = $this->uploadFile($target_field,$visibility,$request);
            return response()->json($res);
            break;
            case 'file':
                $res = $this->uploadFile('file',$visibility, $request);
                return response()->json($res);
                break;
            default:
                return response()->json("Request doesn't contain a file", 422);
        }
    }
    public function view($name){
      $exist = Storage::disk('local')->exists($name);
      if(!$exist){
        abort(404);
      }
      if(!Auth::check()){
        abort(401,'Forbidden');
      }
      $ext = array_last(explode('.', $name));
      $images_ext = [
        'jpg' => 'jpg',
        'png' => 'png',
        'bmp' => 'bmp',
        'tiff' => 'tiff',
        'gif' => "gif"
      ];

      //check if file is image and return preview:
      if(isset($images_ext[$ext])){
        return response()->file(storage_path('app/' . $name));
      }
      return response()->download(storage_path('app/' . $name));
    }
    public function remove($name){
        $res = $this->removeFile($name);
        if ($res)
            return response()->json('File deleted');
        return response()->json('File not found');
    }
}

<?php

namespace App\Http\Controllers;

use App\Action;
use App\Interfaces\IResponse;
use App\Notifications\ActionMade;
use App\Organization;
use App\Service;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class ActionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'actionable_id' => 'required|numeric',
            'actionable_type' => 'required|string',
            'service_id' => 'required|numeric',
        ]);
        $res = new IResponse();
        $target = null;
        switch ($request->actionable_type){
            case "user":
                $target = User::findOrFail($request->actionable_id);
                break;
            case "organization":
                $target =  Organization::findOrFail($request->actionable_id);
                break;
            default:
                $res->details = 'unknown type';
                $res->error = true;
                return response($res);
                break;
        }

        $service = Service::findOrFail($request->service_id);
        $auth = Auth::user();
        $action = new Action();
        $action->author_id = $auth->id;
        $action->actionable_id = $target->id;
        $action->actionable_type = get_class($target);
        $action->service_id = $request->service_id;
        if(isset($request->comment)){
            $action->comment = $request->comment;
        }
        $action->save();
        $service->author->notify(new ActionMade($action));
        $res->details = "User will be notified, Thank you";
        $res->id = $action->id;
        return response()->json($res);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Organization;
use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class SocialController extends Controller
{
    public function like(Request $request)
    {
        $request->validate([
            'like' => 'required|bool',
            'likeable_id' => 'required|numeric',
            'likeable_type' => 'required|string',
        ]);
        $err = [
            'errors' => 1,
            'success' => 0,
            'details' => ''
        ];
        $res = [
            'errors' => 0,
            'success' => 1,
            'details' => ''
        ];
        $target = null;
        $liker = Auth::user();
        switch ($request->likeable_type){
            case "user":
                $target = User::findOrFail($request->likeable_id);
                break;
            case "post":
                $target =  Post::findOrFail($request->likeable_id);
                break;
            case "comment":
                $target =  Comment::findOrFail($request->likeable_id);
                break;
            case "organization":
                $target =  Organization::findOrFail($request->likeable_id);
                break;
            default:
                $err['details'] = 'unknown type';
                return response($err);
                break;
        }
        if(!$liker->hasPermission('add_likes')){
            $err['details'] = "Sorry, you can't like or dislike";
            return response()->json($err,401);
        }
        if($request->like){
            $liker->like($target);
        }else{
            $liker->dislike($target);
        }
        $res['details'] = 'done';
        return response()->json($res);

    }
    public function follow(Request $request)
    {
        $request->validate([
            'follow' => 'required|bool',
            'followable_id' => 'required|numeric',
            'followable_type' => 'required|string',
        ]);
        $err = [
            'errors' => 1,
            'success' => 0,
            'details' => ''
        ];
        $res = [
            'errors' => 0,
            'success' => 1,
            'details' => ''
        ];
        $target = null;
        $follower = Auth::user();
        switch ($request->followable_type){
            case "user":
                $target = User::findOrFail($request->followable_id);
                break;
            case "organization":
                $target =  Organization::findOrFail($request->followable_id);
                break;
            default:
                $err['details'] = 'unknown type';
                return response($err);
                break;
        }
        $follower->toggleFollow($target);
        //dd($follower->followings()->pluck('id')->toArray());
        $res['details'] = 'done';
        return response()->json($res);

    }
}

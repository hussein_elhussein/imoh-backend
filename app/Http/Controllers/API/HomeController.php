<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $homePage = Page::find(1);
        return response()->json($homePage);
    }
    public function test(){
      return response()->json("i received your request");
    }
    public function store(Request $request)
    {
    }
    public function show($id)
    {
        //
    }
    public function update(Request $request, $id)
    {
    }
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }
    public function index()
    {
        //check permissions
        $permitted = CheckPermission('special');
        if(!$permitted) {
            $errors = [
                'success' => 0,
                'errors' => 1,
                'details' => "You don't have permission to view or edit accounts!",
            ];
            return response()->json($errors);
        }
        $users = User::all();
        return response()->json($users);

    }
    public function store(Request $request)
    {
        //check permissions
        $permitted = CheckPermission('special');
        if(!$permitted) {
            $errors = [
                'success' => 0,
                'errors' => 1,
                'details' => "You don't have permission to create!",
            ];
            return response()->json($errors);
        }
        $user = new User;
        $user->name = $request->account['name'];
        $user->email = $request->account['email'];
        $user->password = Hash::make($request->account['password']);
//        $user->role_id = $request->account['role']['id'];
        //todo: assign default role
        if ($request->account['owner']){
            $user->owner = 1;
        }else{
            $user->owner = 0;
        }
        $user->save();
        $result = [
            'success' => 1,
            'errors' => 0,
            'details' => "Account created successfully!",
        ];

        return response()->json($result);

    }
    public function show($id)
    {
        //check permissions
        $permitted = CheckPermission('special');
        if(!$permitted) {
            $errors = [
                'success' => 0,
                'errors' => 1,
                'details' => "You don't have permission to view or edit accounts!",
            ];
            return response()->json($errors);
        }
        $user = User::find($id);
        return response()->json($user);
    }
    public function update(Request $request, $id)
    {
        //check permissions
        $permitted = CheckPermission('special');
        if(!$permitted) {
            $errors = [
                'success' => 0,
                'errors' => 1,
                'details' => "You don't have permission to view or edit accounts!",
            ];
            return response()->json($errors);
        }

        $user = User::find($id);
        $user->name = $request->account['name'];
        if (key_exists('email',$request->account)){
            $user->email = $request->account['email'];
        }
        if (key_exists('password',$request->account)){
            $user->password = Hash::make($request->account['password']);
        }
        //todo: assign default role:
//        $user->role_id = $request->account['role']['id'];
        $user->update();
        $result = [
            'success' => 1,
            'errors' => 0,
            'details' => "Account created successfully!",
        ];
        return response()->json($result);
    }
    public function destroy($id)
    {
        //check permissions
        $permitted = CheckPermission('special');
        if(!$permitted) {
            $errors = [
                'success' => 0,
                'errors' => 1,
                'details' => "You don't have permission to view or edit accounts!",
            ];
            return response()->json($errors);
        }

        $user = User::find($id);
        if(!is_null($user->owner) && $user->owner == 1) {
            $result = [
                'success' => 0,
                'errors' => 1,
                'details' => "This is the owner account, you can't delete it!",
            ];
            return response()->json($result);
        }
        $user->delete();
        $result = [
            'success' => 1,
            'errors' => 0,
            'details' => "Account deleted successfully!",
        ];
        return response()->json($result);
    }
}

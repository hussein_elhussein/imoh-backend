<?php

namespace App\Http\Controllers;

use App\Category;
use App\Interfaces\IResponse;
use App\Location;
use App\Membership;
use App\MembershipType;
use App\Office;
use App\Organization;
use App\Profile;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Profile as ProfileResource;

class OrganizationController extends Controller
{
    use \App\Traits\PermissionHelper;
    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index()
    {
        $org_type_id = setting('content-types.organization_type_id',2);
        $orgs = Profile::where('content_type_id', $org_type_id);
        return ProfileResource::collection($orgs->paginate(10));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function myOrganizations()
    {
        $can_browse = $this->CheckPermission('browse_organizations');
        $can_read = $this->CheckPermission('read_organizations');
        if(!$can_browse || !$can_read){
            $err = [
                'success' => 0,
                'error' => 1,
                'details' => "You don't have permission to browse organizations"
            ];

            return response()->json($err);
        }
        $auth = Auth::id();
        $orgs = Organization::with('author','members','offices')
            ->where('author_id',$auth)
            ->paginate(10);
        return response()->json($orgs);

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate:
        $can_add = $this->CheckPermission('add_organizations');
        $user =  Auth::user();
        if(!$can_add){
            $err = [
                'errors' => 1,
                'success' => 0,
                'details' => "You don't have permission to add organization"
            ];
            return response()->json($err);
        }

        if($user->organizations()->first() && !$user->owner){
            $err = [
                'errors' => 1,
                'success' => 0,
                'details' => "You can't add another organization"
            ];
            return response()->json($err);
        }
        $request->validate([
            'name' => 'required|string',
            'categories_ids' => 'required|array|min:1',
            'head_office' => 'required|array',
            'head_office.email' => 'required|email',
            'head_office.is_head_office' => 'required|boolean',
            'head_office.location_details' => 'string',
            'head_office.phone' => 'required|digits_between:7,11',
            'head_office.second_email' => 'email',
            'head_office.second_phone' => 'digits_between:7,11',
            'head_office.location' => 'required|array|min:3',
            'head_office.location.country' => 'required|array|min:1',
            'head_office.location.country.id' => 'required|numeric|exists:countries,id',
            'head_office.location.administrative_area' => 'required|string',
            'head_office.location.sub_administrative_area' => 'required|string',
            'head_office.location.coords' => 'required|array|min:2',
            'head_office.location.coords.latitude' => 'required|numeric',
            'head_office.location.coords.longitude' => 'required|numeric',
        ]);
        $membership_type = MembershipType::where([['is_default',1],['is_free',1]])->first();
        if(!$membership_type){
            return response()->json("free membership is not available",422);
        }

        //validation passes, save:

        // membership
        $membership = new Membership();
        $membership->membership_type_id = $membership_type->id;
        $membership->validity_duration = 12;
        $membership->renewal_date = Carbon::now()->addYear()->toDateTimeString();
        $membership->save();

        // organization:
        $org_id = null;
        do {
            $org_id = mt_rand();
            $exist = Organization::where('organization_id',$org_id)->first();
        } while ($exist);
        $auth_id = Auth::id();
        $org = new Organization();
        $org->fill($request->toArray());
        $org->membership_id = $membership->id;
        $org->organization_id = $org_id;
        $org->author_id = $auth_id;
        $org->save();

        // find location for the office, otherwise create it:
        $req_loc = $request->head_office['location'];
        $location = Location::where(
            'administrative_area',
            $req_loc['administrative_area'])
            ->orWhere(
                'sub_administrative_area',
                $req_loc['sub_administrative_area'])->first();
        if(!$location){
            $location = new Location();
            $location->fill($req_loc);
            $location->country_id = $req_loc['country']['id'];
            $location->save();
        }

        // office:
        $req_office = $request->head_office;
        $office = new Office();
        $office->fill($req_office);
        $office->organization_id = $org->id;
        $office->phone_country_id = $req_loc['country']['id'];
        if(isset($req_office['second_phone'])){
            $office->second_phone_country_id = $req_loc['country']['id'];
        }
        $office->location_id = $location->id;
        $office->author_id = $auth_id;
        $office->loc_lat = $req_loc['coords']['latitude'];
        $office->loc_lng = $req_loc['coords']['longitude'];
        $office->save();
        // attach categories if they exists:
        foreach ($request->categories_ids as $cat_id) {
            $cat = Category::find($cat_id);
            if($cat){
                $org->categories()->attach($cat_id);
            }
        }
        $res = new IResponse();
        $res->details = "Organization saved successfully";
        $res->id = $org->id;
        return response()->json($res);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return mixed
     */
    public function show($id)
    {
        $profile = Profile::with(
            'ratings',
            'values'
        )->findOrFail($id);
        return new ProfileResource($profile);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $org = Organization::findOrFail($id);
        //validate:
        $can_edit = $this->CheckPermission('edit_organizations');
        if(!$can_edit){
            $err = [
                'errors' => 1,
                'success' => 0,
                'details' => "You don't have permission to edit organization"
            ];
            return response()->json($err);
        }

        $validatedData = $request->validate([
            'name' => 'required|string',
            'categories_ids' => 'required|array|min:1'
        ]);

        $org->name = $request->name;
        if(isset($request->logo) && strlen($request->logo) > 1){
            $org->logo = $request->logo;
        }
        if(isset($request->about) && strlen($request->about) > 1){
            $org->about = $request->about;
        }
        $org->status = "PENDING";

        $org->update();
        $org->categories()->detach();
        foreach ($request->categories_ids as $categories_id) {
            $org->categories()->attach($categories_id);
        }
        $res = [
            'errors' => 0,
            'success' => 1,
            'details' => 'Organization updated successfully',
        ];
        return response()->json($res);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $org = Organization::findOrFail($id);
        $org->delete();
        $res = [
            'errors' => 0,
            'success' => 1,
            'details' => 'Organization deleted successfully',
        ];
        return response()->json($res);
    }
}

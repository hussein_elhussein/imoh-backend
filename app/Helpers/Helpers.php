<?php
if(!function_exists("generateId")){
    function generateId($length, $numeric = false){
        $php_version = (int) explode('.', phpversion())[0];
        $id = null;
        if($php_version >= 7){
            $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            if($numeric){
                $keyspace = '0123456789';
            }
            $pieces = [];
            $max = mb_strlen($keyspace, '8bit') - 1;
            for ($i = 0; $i < $length; ++$i) {
                $pieces []= $keyspace[random_int(0, $max)];
            }
            $id = implode('', $pieces);
        }else{
            $id =  bin2hex(openssl_random_pseudo_bytes($length));
        }
        return $id;
    }
}
if(!function_exists('generateUniqueID')){
    function generateUniqueID($table, $column,  $length, $numeric = false){
        $id = null;
        do{
            $id = generateId($length,$numeric);
            $found = \Illuminate\Support\Facades\DB::table($table)
            ->where($column, $id)
            ->first();
        }while($found);
        return $id;
    }
}
if (!function_exists("consoleOutput")) {

    /**
     * Write on Console
     *
     * @param string $type
     * @param  $messages
     * @return void
     */
    function consoleOutput(string $type, ...$messages)
    {
        $arr = [
            "error" => "error",
            "warning" => "comment",
            "info" => "info",
            "purple" => "question"
        ];
        $message = null;
        foreach ($messages as $m) {
            $message .=  ' ' . $m;
        }
        if (in_array($type, ['error', 'warning', 'info', "purple"])) {
            \Illuminate\Support\Facades\Log::info('messages:', [$message]);
            $output = new Symfony\Component\Console\Output\ConsoleOutput();
            $output->writeln("<{$arr[$type]}>{$message}</{$arr[$type]}>");
        }else{
            \Illuminate\Support\Facades\Log::info('unknown log type:', [$type]);
        }
    }
}
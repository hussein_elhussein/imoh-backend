<?php

namespace App\Helpers;


use App\ContentType;
use App\Permission;
use App\Role;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class Permissions
{

    public function __construct()
    {
    }

    /**
     * Generates permissions for a content type.
     *
     * @param Model $content_type
     */
    public static function generate($content_type){
        $name = strtolower($content_type->type);
        $name = str_replace(' ', '_',$name);
        $permissions = [
            'browse_' . $name => 'Browse ' . $name,
            'read_' .   $name => 'Read ' . $name,
            'edit_' . $name => 'Edit ' . $name,
            'add_' . $name => 'Add ' . $name,
            'delete_' . $name => 'Delete ' . $name,
        ];
        $generated = [];
        foreach ($permissions as $key => $permission) {
            $per = new Permission();
            $per->key = $key;
            $per->table_name = $name . '_ct';
            $per->save();
            $generated[] = $per->id;
        }
        // Attach permissions to the role:
        if(isset($content_type->role_id)){
          $role = Role::findOrFail($content_type->role_id);
          $role->permissions()->attach($generated);
          $stop = null;
        }
    }

    /**
     *  Deletes the generated permissions for a content type.
     * @param Model $content_type
     */
    public static function delete($content_type){
        $name = strtolower($content_type->type);
        $table_name = str_replace(' ', '_', $name) . '_ct';
        $permissions = Permission::whereTableName($table_name)->get()->pluck('id')->toArray();
        Permission::destroy($permissions);
    }

    /**
     * Checks whether a user can access a content of certain type
     * @param $ability
     * @param $type_id
     * @return bool
     */
    public static function can($ability, $type_id){
        $auth = Auth::user();
        if(!$auth){
            return false;
        }
        $cType = ContentType::findOrFail($type_id);
        $type = $cType->type;
        $type = str_replace(' ', '_', $type);
        $type = strtolower($type);
        $key = $ability . '_' . $type;
        $p_type_id = $auth->profile->content_type_id;
        $profile_type = ContentType::with(['role.permissions' => function($query)use ($type,$key){
            $query->where([
                ['key','=',$key],
                ['table_name','=', $type . '_ct']
            ]);
        }])->findOrFail($p_type_id);
        if($profile_type->role){
            return $profile_type->role->permissions->count() > 0;
        }
        return false;
    }
}
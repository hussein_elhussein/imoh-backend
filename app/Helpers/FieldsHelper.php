<?php
if(!function_exists("generateId")){

    /**
     * @param \App\Value $value
     * @return array
     */
    function getValue($value){
        $fieldHelper = new \App\Helpers\FieldValue($value);
        return $fieldHelper->getValue();
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Hussein
 * Date: 8/12/2018
 * Time: 9:24 PM
 */

namespace App\Helpers;


use App\Value;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class FieldValue
{
    private $value;


    /**
     * FieldValue constructor.
     * @param Value $value
     */
    public function __construct($_value)
    {
        $this->value = $_value;
    }

    /**
     * @return array;
     */
    public function getValue()
    {
        $field = $this->value->field;
        $res = null;
        switch ($field->type){
            case "checkbox":
                $res = $this->checkbox();
                break;
            case 'start_date':
            case 'end_date':
            case 'timestamp':
            case 'date':
            $res = $this->dateValue();
                break;
            case "select_dropdown":
                $res = $this->selectValue();
                break;
            default:
                $res  = $this->defaultValue();
                break;
        }
        return $res;
    }
    private function selectValue(){
        $field = $this->value->field;
        $str_val = (string)$this->value->value;
        $hasVal = strlen($str_val);
        $hasOptions = strlen($field->options);
        $res = $this->defaultValue();
        if($hasVal && $hasOptions){
            $optionsObj = json_decode(json_decode($field->options));
            $valid = isset($optionsObj->default) && isset($optionsObj->options);
            if($valid){
                $options = get_object_vars($optionsObj->options);
                foreach ($options as $key => $option) {
                    if($key === $str_val){
                        $res['label'] = $field->label;
                        $res['value'] = $option;
                    }
                }
            }
        }
        return $res;
    }
    private function checkbox(){
        $def_val = $this->defaultValue();
        $val = $this->value->value;
        if(!$this->valueExist()){
            return $def_val;
        }
        $int = (int)$val;
        if(is_int($int)){
            $def_val['value'] = $int > 0;
            return $def_val;
        }
        $str_val = (string)$val;
        if($str_val){
            $tr = $str_val === "true" || $str_val === "TRUE";
            $def_val['value'] = $tr;
            return $def_val;
        }
        return $def_val;
    }
    private function dateValue(){
        $defVal = $this->defaultValue();
        if(!$this->valueExist()){
            return $this->defaultValue();
        }
        $profile = Auth::user()->profile;
        $timezone = null;
        if($profile->timezone){
            $timezone = $profile->timezone;
        }else{
            $timezone = config('timezone','UTC');
        }
        $date_obj = Carbon::createFromTimestamp($this->value->value, $timezone);
        if($date_obj){
            $defVal['value'] = $date_obj->format('Y-m-d h:i:s a');
            return $defVal;
        }
        return $defVal;
    }

    private function defaultValue(){
        $res = [
            'label' => $this->value->field->label,
            'value' => $this->value->value
        ];
        return $res;
    }

    /**
     * @return bool
     */
    private function valueExist(){
        $str_val = (string)$this->value->value;
        return strlen($str_val) !== 0;
    }
}
<?php
use Illuminate\Http\Request;
use App\Http\Requests;
function SaveImage(Request $request, $fieldName)
    {
        if ($request->file($fieldName)->isValid()) {
            $image = $request->file($fieldName);
          $ext = $image->getClientOriginalExtension();
          $imgName = rand(0,100000) . '.' . $ext;
          $image->move('app/uploads/items', $imgName);
          return $imgName;
        }else{
            return false;
        }
    }


function DeleteImage($imageName){
    File::delete('app/uploads/items/' . $imageName);
}


<?php

namespace App\Notifications;

use App\Confirmation;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\BroadcastMessage;
use \App\Http\Resources\Confirmation as ConfirmationResource;
use Illuminate\Support\Facades\Log;

class ConfirmationCreated extends Notification
{
    use Queueable;
    public $data;
    /**
     * Create a new notification instance.
     *
     * @param Confirmation $confirmation
     * @return void
     */
    public function __construct($confirmation)
    {
        $conf_resource = new ConfirmationResource($confirmation);
        $this->data = [
            'data' => [
                'id' => $confirmation->id,
                'text' => $confirmation->code,
                'visible' => false,
                'original' => $conf_resource,
            ]
        ];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        if($notifiable->online){
            return ['database','broadcast'];
        }else{
            return ['database'];
        }
    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        $data = $this->data;
        unset($data['data']['original']);
        return $data;
    }

    /**
     * Get the broadcastable representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        $data = $this->data;
        $data['notifiable'] = $notifiable->socket_id;
        return new BroadcastMessage($data);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return $this->data;
    }
}

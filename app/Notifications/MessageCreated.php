<?php

namespace App\Notifications;

use App\Message;
use App\Profile;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use VSignal\Channels\PushChannel;
use VSignal\Interfaces\Push;

class MessageCreated extends Notification implements ShouldQueue
{
    use Queueable;
    public $data;

    /**
     * Create a new notification instance.
     *
     * @param Message $message: The message instance.
     * @param Profile $author: The author instance.
     * @return void
     */
    public function __construct($message, $author)
    {
        $this->data = [
            'author' => [
                'name' => setting('site.title','IMOH'),
                'avatar' => 'logo.png',
                'socket_id' => null,
            ],
            'data' => [
                'id' => $message->id,
                'text' => "New Message from " . $author->name,
                'visible' => true,
            ]
        ];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        if($notifiable->online){
            return ['database','broadcast'];
        }else if(isset($notifiable->device_token)){
            return ['database',PushChannel::class];
        }else{
            return ['database'];
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return $this->data;
    }

    /**
     * Get the broadcastable representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        $data = $this->data;
        $data['notifiable'] = $notifiable->socket_id;
        return new BroadcastMessage($data);
    }

    /**
     * Get the pushable presentation of the notification.
     *
     * @param  mixed  $notifiable
     * @return Push
     */
    public function toPush($notifiable)
    {
        $title = "New message";
        $body = $this->data['data']['text'];
        $device_id = $notifiable->device_token;
        $locale = $notifiable->locale === 2? "ar": "en";
        $push = new Push([$device_id]);
        $push->setMessage($body,$title);
        if($locale !== 'en'){
            $title = "رسالة جديدة";
            $push->addLocale($body, $title, $locale);
        }
        return $push;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return $this->data;
    }
}

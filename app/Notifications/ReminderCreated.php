<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use VSignal\Channels\PushChannel;
use VSignal\Interfaces\Push;

class ReminderCreated extends Notification implements ShouldQueue
{
    use Queueable;
    public $data;

    /**
     * Create a new notification instance.
     *
     * @param Model $reminder: The reminder instance.
     * @return void
     */
    public function __construct(Model $reminder)
    {
        $this->data = [
            'author' => [
                'name' => setting('site.title'),
                'avatar' => 'logo.png',
                'socket_id' => null,
            ],
            'data' => [
                'id' => $reminder->id,
                'text' => "New reminder has been created",
                'visible' => true,
            ]
        ];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        if($notifiable->online){
            return ['database','broadcast'];
        }else if(isset($notifiable->device_token)){
            return ['database',PushChannel::class];
        }else{
            return ['database'];
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return $this->data;
    }

    /**
     * Get the broadcastable representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        $data = $this->data;
        $data['notifiable'] = $notifiable->socket_id;
        return new BroadcastMessage($data);
    }

    /**
     * Get the pushable presentation of the notification.
     *
     * @param  mixed  $notifiable
     * @return Push
     */
    public function toPush($notifiable)
    {
        $title = "Reminder";
        $body = $this->data['data']['text'];
        $device_id = $notifiable->device_token;
        $locale = $notifiable->locale === 2? "ar": "en";
        $push = new Push([$device_id]);
        $push->setMessage($body,$title);
        if($locale !== 'en'){
            $title = "تذكير";
            $push->addLocale($body, $title, $locale);
        }
        return $push;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return $this->data;
    }
}

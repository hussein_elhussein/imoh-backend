<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use VSignal\Channels\PushChannel;
use VSignal\Interfaces\Push;

class ReminderNote extends Notification implements ShouldQueue
{
    use Queueable;
    public $data;
    public $schedule;

    /**
     * Create a new notification instance.
     *
     * @param Model $reminder: The reminder instance.
     * @param string $schedule: Date of the reminder.
     * @param string $text: The reminder text.
     * @return void
     */
    public function __construct(Model $reminder, $schedule, $text)
    {
        $this->data = [
            'author' => [
                'name' => setting('site.title'),
                'avatar' => 'logo.png',
                'socket_id' => null,
            ],
            'data' => [
                'id' => $reminder->id,
                'text' => $text,
                'visible' => true,
            ]
        ];
        $this->schedule = $schedule;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [PushChannel::class];
    }

    /**
     * Get the pushable presentation of the notification.
     *
     * @param  mixed  $notifiable
     * @return Push
     */
    public function toPush($notifiable)
    {
        $title = "Reminder";
        $body = $this->data['data']['text'];
        $device_id = $notifiable->device_token;
        $locale = $notifiable->locale === 2? "ar": "en";
        $schedule = $this->schedule;
        $push = new Push([$device_id],$schedule);
        $push->setMessage($body, $title);
        if($locale !== 'en'){
            $title = "تذكير";
            $push->addLocale($body,$title,$locale);
        }
        return $push;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return $this->data;
    }
}

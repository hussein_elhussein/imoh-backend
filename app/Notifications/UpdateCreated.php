<?php

namespace App\Notifications;

use App\Http\Resources\Profile as ProfileResource;
use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use VSignal\Channels\PushChannel;
use VSignal\Interfaces\Push;

class UpdateCreated extends Notification implements ShouldQueue
{
    use Queueable;
    public $data;
    /**
     * Create a new notification instance.
     *
     * @param Model $update: the update of the content
     * @return void
     */
    public function __construct(Model $update)
    {
        $text = "New service update";
        $this->data = [
            'author' => new ProfileResource($update->author,null,true),
            'data' => [
                'id' => $update->id,
                'text' => $text,
                'visible' => true,
            ]
        ];

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        if($notifiable->online){
            return ['database','broadcast'];
        }else if(isset($notifiable->device_token)){
            return ['database',PushChannel::class];
        }else{
            return ['database'];
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return $this->data;
    }

    /**
     * Get the broadcastable representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        $data = $this->data;
        $data['notifiable'] = $notifiable->socket_id;
        return new BroadcastMessage($data);
    }

    /**
     * Get the pushable presentation of the notification.
     *
     * @param  mixed  $notifiable
     * @return Push
     */
    public function toPush($notifiable)
    {
        $title = "New service update";
        $body = $this->data['author']['name'] . " updated your service";
        $device_id = $notifiable->device_token;
        $locale = $notifiable->locale === 2? 'ar': "en";
        $push = new Push([$device_id]);
        $push->setMessage($body, $title);
        if($locale !== 'en'){
            $title = "تم تحديث الخدمة";
            $push->addLocale($body,$title,$locale);
        }
        return $push;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return $this->data;
    }
}

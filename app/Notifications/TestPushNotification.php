<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use VSignal\Channels\PushChannel;
use VSignal\Interfaces\Push;

class TestPushNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [PushChannel::class];
    }


    public function toPush($notifiable){
        $title = "Test";
        $body = "This is a test notification";
        $device_id = $notifiable->device_token;
        $locale = "en";
        $push = new Push($title,$body,$locale,$device_id, "2018-7-19 12:00:00 GMT+0300");
        return $push;
    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

<?php

namespace App\Events;

use App\Reminder;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class ReminderCanceled
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $reminder;
    /**
     * Create a new event instance.
     *
     * @param Reminder $reminder
     * @return void
     */
    public function __construct($reminder)
    {
        $this->reminder = $reminder;
    }
}

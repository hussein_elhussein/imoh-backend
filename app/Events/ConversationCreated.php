<?php

namespace App\Events;

use App\Conversation;
use App\Http\Resources\Profile;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class ConversationCreated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $id;
    public $socket_id;
    public $opener;
    public $receivers;

    /**
     * Create a new event instance.
     * @param Conversation $conversation
     * @return void
     */
    public function __construct($conversation)
    {
        $this->id = $conversation->id;
        $this->socket_id = $conversation->socket_id;
        $this->opener = new Profile($conversation->opener,null,true);
        $this->receivers = [];
        foreach ($conversation->receivers as $receiver) {
            $this->receivers[] = new Profile($receiver,null,true);
        }
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return ['chat-channel'];
    }
}

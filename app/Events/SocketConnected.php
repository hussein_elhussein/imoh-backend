<?php

namespace App\Events;

use App\Profile;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SocketConnected implements ShouldBroadcast, ShouldQueue
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $conversations;
    /**
     * Create a new event instance.
     *
     * @param Profile $profile;
     * @return void
     */
    public function __construct($profile)
    {
        $this->conversations = [];
        $this->joinConversations($profile);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return ['chat-channel'];
    }

    /**
     *  Rejoins the user to all previous conversations
     * @param Profile $profile
     */
    private function joinConversations($profile){
        foreach ($profile->conversations as $conversation) {
            $this->conversations[] = [
                'client_id' => $profile->socket_id,
                'conversation_id' => $conversation->socket_id
            ];
        }
    }
}

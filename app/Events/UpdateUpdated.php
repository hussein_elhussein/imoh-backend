<?php

namespace App\Events;

use App\ContentUpdate;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class UpdateUpdated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $contentUpdate;

    /**
     * Create a new event instance.
     *
     * @param ContentUpdate $contentUpdate
     * @return void
     */
    public function __construct($contentUpdate)
    {
        $this->contentUpdate    = $contentUpdate;
    }
}

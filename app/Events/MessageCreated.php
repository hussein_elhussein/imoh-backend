<?php

namespace App\Events;

use App\Conversation;
use App\Message;
use App\Profile;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use App\Notifications\MessageCreated as MessageNotification;
class MessageCreated implements ShouldQueue
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Message
     */
    public $message;
    /**
     * @var string
     */
    public $socket_id;
    /**
     * @var Profile
     */
    public $author;
    /**
     * Create a new event instance.
     *
     * @param Message $message
     * @param string $socket_id
     * @param Profile $author
     * @return void
     */
    public function __construct($message, $socket_id, $author)
    {
        $this->message = $message;
        $this->socket_id = $socket_id;
        $this->author = $author;
        $this->notifyReceivers();
    }
    private function notifyReceivers(){
        $conversation = Conversation::with('receivers','viewersMin','deletersMin')
            ->find($this->message->conversation_id);

        // get notifiables
        $toNotify = collect([]);
        if($conversation){
            foreach ($conversation->receivers as $receiver) {
                $found = $conversation->viewersMin->filter(function($viewer) use($receiver){
                    return $viewer->id === $receiver->id;
                });
                if(!$found->count()){
                    $toNotify->push($receiver);
                }
            }
            if($toNotify->count()){
                Notification::send($toNotify, new MessageNotification($this->message,$this->author));
            }
        }
    }
}

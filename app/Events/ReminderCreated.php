<?php

namespace App\Events;

use App\ContentUpdate;
use App\Profile;
use App\Reminder;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Support\Collection;

class ReminderCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $reminder;
    public $profiles;
    public $contentUpdate;
    /**
     * Create a new event instance.
     * @param Reminder $reminder
     * @param Collection $profiles
     * @param ContentUpdate $contentUpdate
     * @return void
     */
    public function __construct($reminder, $profiles, $contentUpdate)
    {
        $this->reminder         = $reminder;
        $this->profiles         = $profiles;
        $this->contentUpdate    = $contentUpdate;
    }
}

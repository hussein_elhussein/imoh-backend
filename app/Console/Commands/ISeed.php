<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ISeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:iseed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'deletes all records from database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $db = env('DB_DATABASE',NULL);
        if($db){
            $tables = DB::select('SHOW TABLES');
            $db_prop = "Tables_in_" . $db;
            foreach($tables as $table)
            {
                $this->info('Generating seed file for: ' . $table->{$db_prop});
                \Iseed::generateSeed($table->{$db_prop});
            }
            $this->info('done');
        }else{
            $this->error('no database is defined');
        }
    }
}

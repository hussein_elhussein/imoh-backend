<?php

namespace App\Observers;

use App\Content;
use App\ContentType;
use App\Services\ProfileService;

class ContentObserver
{
    private $service;
    public function __construct(ProfileService $service)
    {
        $this->service = $service;
    }

    /**
     * Handle to the content "retrieved" event.
     *
     * @param  \App\Content  $content
     * @return void
     */
    public function retrieved(Content $content)
    {

    }

    /**
     * Handle to the content "created" event.
     *
     * @param  \App\Content  $content
     * @return void
     */
    public function created(Content $content)
    {
        //creates and attach a profile to an office when it's created:
        $is_office = false;
        if(request()->has('ids')){
            $office_type_id = (int) setting('content-types.office_type_id',6);
            $ids = request()->get('ids');
            foreach ($ids as $id){
                if($id === $office_type_id){
                    $is_office = true;
                }
            }
        }
        if($is_office){
            $profile_type_id = (int) setting('content-types.profile_type_id',1);
            $profile_type = ContentType::find($profile_type_id);
            $data = request()->get($profile_type->type);
            $profile = $this->service->attachMember($data,$profile_type);
            $content->timestamps = false;
            $content->profile_id = $profile->id;
            $content->update();
        }
    }

    /**
     * Handle the content "updated" event.
     *
     * @param  \App\Content  $content
     * @return void
     */
    public function updated(Content $content)
    {
        //
    }

    /**
     * Handle the content "deleted" event.
     *
     * @param  \App\Content  $content
     * @return void
     */
    public function deleted(Content $content)
    {
        //
    }
}

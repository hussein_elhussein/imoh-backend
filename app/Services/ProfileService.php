<?php

namespace App\Services;


use App\ContentType;
use App\Models\Traits\LocationHelper;
use App\Models\Traits\ValueHelper;
use App\Profile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileService
{
    use ValueHelper;
    use LocationHelper;
    public function saveUser($data){
        $user = new User();
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = bcrypt($data['password']);
        $user->save();
        return $user;
    }
    public function saveProfile($data, $values,$content_type, $primary = null){
        //save the profile:
        $profile = new Profile();
        $profile_id = generateUniqueID('profiles','profile_id',20,true);
        $qr_code = generateUniqueID('profiles','qr_code',50);
        $profile->fill($data);
        $profile->content_type_id = $content_type->id;
        $profile->profile_id = $profile_id;
        $profile->qr_code = $qr_code;
        $profile->loc_lat = $data['location']['coords']['latitude'];
        $profile->loc_lng = $data['location']['coords']['longitude'];
        if($primary){
            $profile->primary = 1;

        }
        $profile->save();
        // Attach fields values to profile:
        if($values){
            $this->attachValues($profile,$values);
        }
        return $profile;
    }
    public function register($data, $cType = null){
        $user = $this->saveUser($data);
        $values = $data['values'];
        if(!$cType){
            $profile_type_id = setting('content-types.profile_type_id',1);
            $cType = ContentType::with('sections.fields')->findOrFail($profile_type_id);
        }
        $profile = $this->saveProfile($data,$values,$cType);
        $profile->users()->attach($user->id);
        return $profile;
    }
    public function attachMember($data,$profile_type){
        $auth = Auth::user();
        $profile = $auth->profile;
        $member = $this->register($data,$profile_type);
        $mem_user = $member->users->first();
        $profile->users()->attach([$mem_user->id => ['is_member' => 1]]);
        return $member;
    }
    public function validateFields($request, $update = false, $register = false, $profileCT = null){
        $prefix = "";
        if($profileCT){
            $prefix = $profileCT->type  . '.';
        }
        $update_roles = [
            $prefix . 'content_type_id'     => 'required|integer',
            $prefix . 'values'              => 'required|array|min:1',
            $prefix . 'values.*.field_id'   => 'required|integer',
            $prefix . 'values.*.value'      => 'required',
            $prefix . 'name'                => 'required|string',
            $prefix . 'avatar'              => 'string',

        ];
        $storeRules = [
            $prefix . 'location.coords'                     => 'required|array',
            $prefix . 'location.coords.latitude'            => 'required',
            $prefix . 'location.coords.longitude'           => 'required',
            $prefix . 'location.country'                    => 'required|array',
            $prefix . 'location.country.name'               => 'required|string',
            $prefix . 'location.sub_administrative_area'    => 'required|string',
            $prefix . 'location.administrative_area'        => 'required|string'
        ];
        if($profileCT){
            $storeRules = $storeRules + [
                $profileCT->type => 'required|array'
            ];
        }
        if($update){
            $request->validate($update_roles);
        }else{
            if($register){
                $storeRules = $storeRules + [
                    $prefix . 'name'        => 'required|string',
                    $prefix . 'email'       => 'required|email|unique:users,email',
                    $prefix . 'password'    => 'required|string',
                ];
            }
            $rules = array_merge($update_roles,$storeRules);
            $request->validate($rules);
        }
    }
}
#!/usr/bin/env bash

composer dump-autoload &&
mysql -u root -e "drop database imoh_db" &&
mysql -u root -e "create database imoh_db" &&
php artisan migrate --seed